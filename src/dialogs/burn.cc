/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "dialogs/burn.h"
#include "widgets/combo-speed.h"
#include "baconmm/cd-selection.h"
#include "ucompose.h"

#include <cd-drive.h>

#include <gtkmm/stock.h>
#include <gtkmm/image.h>
#include <gtkmm/combobox.h>
#include <gtkmm/label.h>
#include <gtkmm/checkbutton.h>

#include <libglademm/xml.h>

namespace Coaster
{

namespace Dialogs
{

Burn::Burn(BaseObjectType* cobject,
           const Glib::RefPtr<Gnome::Glade::Xml>& refXml)
: Gtk::Dialog(cobject),
  m_pImage(0), m_pLabel_Drive(0), m_pLabel_Speed(0),
  m_pHBox_Drive(0), m_pHBox_Speed(0), m_pChkBtn_Dummy(0), m_pChkBtn_Eject(0),
  m_pOK(0), m_pCancel(0), m_pComboDrives(0), m_pComboSpeed(0)
{
  using namespace Gtk;

  refXml->get_widget("burn_image", m_pImage);
  refXml->get_widget("burn_label_drive", m_pLabel_Drive);
  refXml->get_widget("burn_label_speed", m_pLabel_Speed);
  refXml->get_widget("burn_hbox_drive", m_pHBox_Drive);
  refXml->get_widget("burn_hbox_speed", m_pHBox_Speed);
  refXml->get_widget("burn_dummy", m_pChkBtn_Dummy);
  refXml->get_widget("burn_eject", m_pChkBtn_Eject);
  refXml->get_widget("burn_ok", m_pOK);
  refXml->get_widget("burn_cancel", m_pCancel);
  
//  m_pOK->set_sensitive(false);

  m_pImage->set(Gtk::StockID("coaster-burn"), Gtk::ICON_SIZE_DIALOG);

  m_pComboDrives = new Bacon::CdSelection();
  m_pComboDrives->property_file_image() = false;
  m_pComboDrives->property_show_recorders_only() = true;
  m_pComboDrives->signal_device_changed().connect(sigc::mem_fun(*this, &Burn::on_combo_changed));
  m_pComboDrives->show_all_children();
  m_pHBox_Drive->pack_start(*Gtk::manage(m_pComboDrives));
  m_pHBox_Drive->show_all();
  m_pLabel_Drive->set_mnemonic_widget(*m_pComboDrives);

  m_pComboSpeed = new Widgets::ComboSpeed();
  m_pComboSpeed->populate_speed(m_pComboDrives->get_cdrom()->max_speed_write);
  m_pHBox_Speed->pack_start(*Gtk::manage(m_pComboSpeed));
  m_pHBox_Speed->show_all();
  m_pLabel_Speed->set_mnemonic_widget(*m_pComboSpeed);
  
  set_default_response(Gtk::RESPONSE_OK);
}

Burn::~Burn()
{}

CDDrive* Burn::get_cd_drive() const
{
  return const_cast<CDDrive*>(m_pComboDrives->get_cdrom());
}

bool Burn::get_dummy() const
{
  return m_pChkBtn_Dummy->get_active();
}

int Burn::get_speed() const
{
  return m_pComboSpeed->get_selected_real_speed();
}

bool Burn::get_eject() const
{
  return m_pChkBtn_Eject->get_active();
}

void Burn::set_action_widgets_sensitive(bool sensitive)
{
  m_pComboDrives->set_sensitive(sensitive);
  m_pComboSpeed->set_sensitive(sensitive);
  get_action_area()->set_sensitive(sensitive);
  m_pCancel->set_sensitive(true);
}

void Burn::on_combo_changed(const Glib::ustring& device)
{
  m_pComboSpeed->populate_speed(m_pComboDrives->get_cdrom()->max_speed_write);
}

} // namespace Dialogs

} // namespace Coaster
