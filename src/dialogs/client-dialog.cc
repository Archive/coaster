/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "dialogs/client-dialog.h"
#include "client.h"

#include <libglademm/xml.h>

namespace Coaster
{

namespace Dialogs
{

ClientDialog::ClientDialog(BaseObjectType* cobject,
                           const Glib::RefPtr<Gnome::Glade::Xml>& refXml,
                           bool instant)
: Gtk::Dialog(cobject), m_instant_(instant), m_refGlade(refXml)
{
  m_pConfClient = new Client();
}

ClientDialog::~ClientDialog()
{
  if(m_pConfClient)
  {
    delete m_pConfClient;
    m_pConfClient = 0;
  }
}

void ClientDialog::connect_widget(const Glib::ustring& key,
                                  const Glib::ustring& glade_widget_name)
{
  Gtk::Widget* pWidget = 0;
  m_refGlade->get_widget(glade_widget_name, pWidget);
  if (pWidget && m_pConfClient)
  {
    if(m_instant_)
      m_pConfClient->add_instant(key, *pWidget);
    else
      m_pConfClient->add(key, *pWidget);
  }
}

void ClientDialog::load()
{
  if (m_pConfClient)
    m_pConfClient->load();
}

void ClientDialog::save()
{
  if (m_pConfClient)
    m_pConfClient->save();
}

void ClientDialog::on_show()
{
  load();
  Gtk::Dialog::on_show();
}

void ClientDialog::on_hide()
{
  if(!m_instant_)
    save();

  Gtk::Dialog::on_hide();
}

void ClientDialog::on_response(int response_id)
{
  switch(response_id)
  {
    case Gtk::RESPONSE_CANCEL:
    {
      Gtk::Dialog::on_response(response_id);
      break;
    }
    default:
    {
      hide();
      Gtk::Dialog::on_response(response_id);
      break;
    }
  }
}

} // namespace Dialogs

} // namespace Coaster
