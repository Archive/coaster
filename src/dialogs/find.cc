/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "dialogs/find.h"

#include <gtkmm/entry.h>
#include <libglademm/xml.h>

namespace Coaster
{

namespace Dialogs
{

Find::Find(BaseObjectType* cobject,
           const Glib::RefPtr<Gnome::Glade::Xml>& refXml)
: ClientDialog(cobject, refXml, false), m_pEntry(0)
{
  refXml->get_widget("search_entry", m_pEntry);
  
  connect_widget("search/last_search", "search_entry");
  connect_widget("search/match_case", "search_match_case_check");
  load();
}

Find::~Find()
{}

Glib::ustring Find::get_search_string() const
{
  return m_pEntry->get_text();
}

} // namespace Dialogs

} // namespace Coaster
