/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "dialogs/erase.h"
#include "cstr-debug.h"
#include "baconmm/cd-selection.h"
#include "ucompose.h"

#include <gtkmm/checkbutton.h>
#include <gtkmm/image.h>
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/label.h>

#include <libglademm/xml.h>

namespace Coaster
{

namespace Dialogs
{

Erase::Erase(BaseObjectType* cobject,
             const Glib::RefPtr<Gnome::Glade::Xml>& refXml)
: Gtk::Dialog(cobject),
  m_pImage(0), m_pLabel_Drive(0), m_pHBox_Drive(0), m_pFast(0), m_pOK(0), 
  m_pComboDrives(0)
{
  refXml->get_widget("erase_image", m_pImage);
  refXml->get_widget("erase_fast", m_pFast);
  refXml->get_widget("erase_hbox_drive", m_pHBox_Drive);
  refXml->get_widget("erase_label_drive", m_pLabel_Drive);
  refXml->get_widget("erase_ok", m_pOK);

  m_pOK->set_sensitive(false);
  m_pOK->signal_clicked().connect(sigc::mem_fun(*this, &Erase::on_ok_clicked));
  
  m_pImage->set(Gtk::StockID("coaster-erase"), Gtk::ICON_SIZE_DIALOG);

  m_pComboDrives = new Bacon::CdSelection();
  m_pLabel_Drive->set_mnemonic_widget(*m_pComboDrives);
  /*if(m_pComboDrives->is_selected_erasable())
    m_pOK->set_sensitive(true);*/
  /*m_pComboDrives->signal_changed().connect(sigc::mem_fun(*this, &Erase::on_combo_changed));*/

  m_pHBox_Drive->pack_start(*Gtk::manage(m_pComboDrives));
  m_pHBox_Drive->show_all();

  set_default_response(Gtk::RESPONSE_OK);
}

Erase::~Erase()
{
  debug("Erase: destroyed");
}

CDDrive* Erase::get_cd_drive() const
{
  return const_cast<CDDrive*>(m_pComboDrives->get_cdrom());
}

bool Erase::get_fast() const
{
  return m_pFast->get_active();
}

void Erase::set_action_widgets_sensitive(bool sensitive)
{
  m_pComboDrives->set_sensitive(sensitive);
  m_pFast->set_sensitive(sensitive);
  get_action_area()->set_sensitive(sensitive);
}

void Erase::on_combo_changed()
{
  /*if(m_pComboDrives->is_selected_erasable())
    m_pOK->set_sensitive();
  else
    m_pOK->set_sensitive(false);*/
}

void Erase::on_ok_clicked()
{
  set_action_widgets_sensitive(false);

  //IO::erasedisc(get_drive_info(), get_fast(), sigc::mem_fun(*this, &Erase::pulse));

  response(Gtk::RESPONSE_OK);
}

} // namespace Dialogs

} // namespace Coaster
