/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_PREFERENCES_H_
#define _COASTER_PREFERENCES_H_

#include <bakery/Configuration/Dialog_Preferences.h>

namespace Gtk
{
class Button;
class CheckButton;
class Entry;
class HBox;
class Label;
class Notebook;
class Table;
} // namespace Gtk

namespace Gnome
{
namespace Glade
{
class Xml;
} // namespace Glade
} // namespace Gnome

namespace Bacon
{
class CdSelection;
} // namespace Baconmm

namespace Coaster
{

namespace Widgets
{
class PrefsList;
} // namespace Widgets

namespace Dialogs
{

class Preferences : public Bakery::Dialog_Preferences
{
public:
  Preferences(Gtk::Window& parent);
  virtual ~Preferences();
protected:
  void on_category_changed(int id);
  void on_sound_enable_changed();
  void on_checkbutton_changed(Gtk::CheckButton* check,
                              Gtk::Widget* entry,
                              Gtk::Widget* button);
  void on_file_button_clicked(Gtk::Entry* entry,
                              const Glib::ustring& key);

private:
  Widgets::PrefsList* m_pCategories;
  Gtk::Notebook* m_pNotebook;

  // General tab widgets
  Gtk::Label* m_pReader_Label;
  Gtk::Label* m_pWriter_Label;

  Gtk::Table* m_pDev_Table;

  Bacon::CdSelection* m_pReader;
  Bacon::CdSelection* m_pWriter;

  // Notifications tab widgets
  Gtk::CheckButton* m_pSound_Check;

  Gtk::CheckButton* m_pBurnFin_Check;
  Gtk::Entry*       m_pBurnFin_Entry;
  Gtk::Button*      m_pBurnFin_Button;

  Gtk::CheckButton* m_pBurnErr_Check;
  Gtk::Entry*       m_pBurnErr_Entry;
  Gtk::Button*      m_pBurnErr_Button;
};

} // namespace Dialogs

} // namespace Coaster

#endif // _COASTER__H_
