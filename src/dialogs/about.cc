/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "dialogs/about.h"

#include "cstr-intl.h"
#include "ucompose.h"
#include "widgets/stock-label-button.h"

#include <gtkmm/button.h>
#include <gtkmm/image.h>
#include <gtkmm/label.h>
#include <gtkmm/notebook.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/stock.h>
#include <gtkmm/viewport.h>

#include <libgnome/gnome-url.h>

namespace
{

Gtk::Label* create_label()
{
  using namespace Gtk;
  
  Label* label = new Label("");
  label->set_selectable(true);
  label->set_alignment(0.0, 0.0);
  label->set_padding(8, 8);
  label->set_justify(JUSTIFY_LEFT);

  return label;
}

} // anonymous namespace
namespace Coaster
{

namespace Dialogs
{

About::About(const Glib::ustring& name,
             const Glib::ustring& version,
             const Glib::ustring& copyright,
             const Glib::ustring& comments,
             const Glib::ustring& website,
             const std::vector<Glib::ustring>& authors,
             const std::vector<Glib::ustring>& documenters,
             const Glib::ustring& translators,
             const Glib::RefPtr<Gdk::Pixbuf>& logo)
: m_pClose(0), m_pCredits(0), m_pLogo_Image(0), 
  m_pName_Label(0), m_pComments_Label(0), m_pCopyright_Label(0),
  m_pWebsite_Button(0), m_Hand_Cursor(Gdk::HAND2), m_Regular_Cursor(Gdk::XTERM),
  m_Website(website), m_vecAuthors(authors), m_vecDocumenters(documenters),
  m_Translators(translators), m_bCreditsShown(false), m_pCredits_Dialog(0)
{
  using namespace Gtk;
  set_has_separator(false);
  set_title(String::ucompose(_("About %1"), name));

  Widget::push_composite_child();
  
  VBox* vbox = new VBox(false, 8);
  vbox->set_border_width(8);

  get_vbox()->pack_start(*manage(vbox), true, true, 0);

  Image* m_pLogo_Image = new Image(logo);
  vbox->pack_start(*manage(m_pLogo_Image), false, false, 0);

  m_pName_Label = new Label();
  m_pName_Label->set_selectable(true);
  m_pName_Label->set_justify(JUSTIFY_CENTER);
  m_pName_Label->set_markup(String::ucompose("<span size='xx-large' weight='bold'>%1 %2</span>",
                                          name, version));
  vbox->pack_start(*manage(m_pName_Label), false, false, 0);

  Label* m_pComments_Label = new Label();
  m_pComments_Label->set_selectable(true);
  m_pComments_Label->set_justify(JUSTIFY_CENTER);
  m_pComments_Label->set_line_wrap(true);
  m_pComments_Label->set_text(comments);
  vbox->pack_start(*manage(m_pComments_Label), false, false, 0);

  m_pCopyright_Label = new Label();
  m_pCopyright_Label->set_selectable(true);
  m_pCopyright_Label->set_justify(JUSTIFY_CENTER);
  m_pCopyright_Label->set_markup(String::ucompose("<span size='small'>%1</span>", copyright));
  vbox->pack_start(*manage(m_pCopyright_Label), false, false, 0);

  m_pWebsite_Button = new Button("");
  m_pWebsite_Button->unset_flags(RECEIVES_DEFAULT);
  m_pWebsite_Button->unset_flags(CAN_FOCUS);
  m_pWebsite_Button->set_relief(RELIEF_NONE);
  
  GdkColor link_color = { 0, 0, 0, 0xffff };
  Glib::ustring link_text = Glib::convert_return_gchar_ptr_to_ustring(g_markup_printf_escaped("<span foreground=\"#%04x%04x%04x\" underline=\"single\">%s</span>", link_color.red, link_color.green, link_color.blue, m_Website.c_str()));

  Label* link_label = static_cast<Label*>(m_pWebsite_Button->get_child());
  link_label->set_markup(link_text);
  m_pWebsite_Button->signal_clicked().connect(sigc::mem_fun(*this, &About::on_link_clicked));

  HBox* hbox = new HBox(true, 0);
  hbox->pack_start(*manage(m_pWebsite_Button), false, false, 0);
  vbox->pack_start(*manage(hbox), true, false, 0);

  vbox->show();
  m_pLogo_Image->show();
  m_pName_Label->show();
  hbox->show();

  // Add the OK button
  add_button(Stock::CLOSE, RESPONSE_CLOSE);
  set_default_response(RESPONSE_CLOSE);
  get_action_area()->children().front().get_widget()->grab_focus();

  // Add the credits button
  m_pCredits = new Widgets::SLButton(StockID("gnome-stock-about"), _("_Credits"));
  get_action_area()->pack_end(*manage(m_pCredits), false, true, 0);
  get_action_area()->set_child_secondary(*m_pCredits, true);
  m_pCredits->signal_clicked().connect(sigc::mem_fun(*this, &About::on_credits_clicked));

  set_resizable(false);

  Widget::pop_composite_child();

  signal_response().connect(sigc::hide(sigc::mem_fun(*this, &About::on_close_clicked)));

  show_all();
}

About::~About()
{}

void About::on_close_clicked()
{
  if(m_pCredits_Dialog)
  {
    delete m_pCredits_Dialog;
    m_pCredits_Dialog = 0;
  }

  hide();
}

void About::on_credits_clicked()
{
  if(m_pCredits_Dialog && m_bCreditsShown)
  {
    m_pCredits_Dialog->present();
    return;
  }

  create_credits_dialog();
  m_pCredits_Dialog->present();
}

void About::create_credits_dialog()
{
  using namespace Gtk;

  if(m_pCredits_Dialog)
  {
    delete m_pCredits_Dialog;
    m_pCredits_Dialog = 0;
  }

  m_pCredits_Dialog = new Dialog(_("Credits"), *this);
  m_pCredits_Dialog->set_default_size(360, 260);
  m_pCredits_Dialog->set_default_response(RESPONSE_CLOSE);
  m_pCredits_Dialog->set_border_width(8);
  m_pCredits_Dialog->get_vbox()->set_spacing(2);
  m_pCredits_Dialog->add_button(Stock::CLOSE, RESPONSE_CLOSE);

  Notebook* notebook = new Notebook();
  notebook->set_border_width(6);
  m_pCredits_Dialog->get_vbox()->pack_start(*manage(notebook), true, true, 0);

  if(!m_vecAuthors.empty())
  {
    Label* label = create_label();

    ScrolledWindow* sw = new ScrolledWindow();
    sw->set_policy(POLICY_AUTOMATIC, POLICY_AUTOMATIC);
    sw->add(*manage(label));
    static_cast<Viewport*>(sw->get_child())->set_shadow_type(SHADOW_NONE);
    notebook->append_page(*manage(sw), _("Written by"));
    
    Glib::ustring string;
    for(std::vector<Glib::ustring>::iterator i = m_vecAuthors.begin() ; i != m_vecAuthors.end() ; ++i)
    {
      string += Glib::Markup::escape_text(*i);
      if(i != m_vecAuthors.end())
        string += "\n";
    }

    label->set_markup(string);
  }

  if(!m_vecDocumenters.empty())
  {
    Label* label = create_label();

    ScrolledWindow* sw = new ScrolledWindow();
    sw->set_policy(POLICY_AUTOMATIC, POLICY_AUTOMATIC);
    sw->add(*manage(label));
    static_cast<Viewport*>(sw->get_child())->set_shadow_type(SHADOW_NONE);
    notebook->append_page(*manage(sw), _("Documented by"));
    
    Glib::ustring string;
    for(std::vector<Glib::ustring>::iterator i = m_vecDocumenters.begin() ; i != m_vecDocumenters.end() ; ++i)
    {
      string += Glib::Markup::escape_text(*i);
      if(i != m_vecDocumenters.end())
        string += "\n";
    }

    label->set_markup(string);
  }

  if(m_Translators != "")
  {
    Label* label = create_label();

    ScrolledWindow* sw = new ScrolledWindow();
    sw->set_policy(POLICY_AUTOMATIC, POLICY_AUTOMATIC);
    sw->add(*manage(label));
    static_cast<Viewport*>(sw->get_child())->set_shadow_type(SHADOW_NONE);
    notebook->append_page(*manage(sw), _("Documented by"));

    Glib::ustring string;
    string += Glib::Markup::escape_text(m_Translators);
    label->set_markup(string);
  }

  m_bCreditsShown = true;
  m_pCredits_Dialog->show_all();

  m_pCredits_Dialog->signal_response().connect(sigc::hide(sigc::mem_fun(*this, &About::on_credits_close)));
}

void About::on_credits_close()
{
  m_pCredits_Dialog->hide();
  m_bCreditsShown = false;
}

void About::on_link_clicked()
{
  gnome_url_show(m_Website.c_str(), 0);
}

} // namespace Dialogs

} // namespace Coaster
