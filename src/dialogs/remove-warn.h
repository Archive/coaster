/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_REMOVE_WARN_H_
#define _COASTER_REMOVE_WARN_H_

#include "dialogs/client-dialog.h"

namespace Gnome
{
namespace Glade
{
class Xml;
} // namespace Glade
} // namespace Gnome

namespace Coaster
{

namespace Dialogs
{

class RemoveWarn : public ClientDialog
{
public:
  RemoveWarn(BaseObjectType* cobject,
             const Glib::RefPtr<Gnome::Glade::Xml>& refXml);
  virtual ~RemoveWarn();
};

} // namespace Dialogs

} // namespace Coaster

#endif // _COASTER_REMOVE_WARN_H_
