/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_BURN_PROGRESS_H_
#define _COASTER_BURN_PROGRESS_H_

#include <gtkmm/dialog.h>

namespace Gtk
{
class ProgressBar;
class Label;
class Button;
} // namespace Gtk

namespace Gnome
{
namespace Glade
{
class Xml;
} // namespace Glade
} // namespace Gnome

namespace Coaster
{

namespace Dialogs
{

class BurnProgress : public Gtk::Dialog
{
public:
  BurnProgress(BaseObjectType* cobject,
               const Glib::RefPtr<Gnome::Glade::Xml>& refXml);
  virtual ~BurnProgress();

  void on_progress_changed(double fraction);
  void set_progress(double fraction);
  void set_text(const Glib::ustring& text);

  Glib::SignalProxy0<void> signal_cancel_clicked();
protected:
  
private:
  Gtk::ProgressBar* m_pProgressBar;
  Gtk::Label*       m_pLabel_Status;

  Gtk::Button* m_pCancel;
};

} // namespace Dialogs

} // namespace Coaster

#endif // _COASTER_BURN_PROGRESS_H_
