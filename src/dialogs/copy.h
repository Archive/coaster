/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_COPY_DIALOG_
#define _COASTER_COPY_DIALOG_

#include "sharedptr.h"

#include <gtkmm/dialog.h>
#include <cd-drive.h>

namespace Gtk
{
class HBox;
class Image;
class Button;
class Label;
class Expander;
class ProgressBar;
} // namespace Gtk

namespace Bacon
{
class CdSelection;
} // namespace Bacon

namespace Gnome
{
namespace Glade
{
class Xml;
} // namespace Glade
} // namespace Gnome

namespace Coaster
{

class ComboDrives;

namespace Dialogs
{

class Copy : public Gtk::Dialog
{
public:
  Copy(BaseObjectType* cobject,
       const Glib::RefPtr<Gnome::Glade::Xml>& refXml);
  virtual ~Copy();

  CDDrive* get_read_drive() const;
  CDDrive* get_write_drive() const;

  void set_action_widgets_sensitive(bool sensitive = true);

protected:
  void on_combo_changed(const Glib::ustring& device);
  void on_finished_copying();
  void on_ok_clicked();
  
private:
  Gtk::Image* m_pImage;
  
  Gtk::HBox* m_pHBox_Read;
  Gtk::HBox* m_pHBox_Write;

  Gtk::Button* m_pOK;
  Gtk::Button* m_pCancel;

  Gtk::Label* m_pLabel_Read;
  Gtk::Label* m_pLabel_Write;
  
  Bacon::CdSelection* m_pComboDrives_Read;
  Bacon::CdSelection* m_pComboDrives_Write;
};

} // namespace Dialogs
  
} // namespace Coaster

#endif // _COASTER_COPY_DIALOG_
