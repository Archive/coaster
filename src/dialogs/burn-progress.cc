/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "dialogs/burn-progress.h"
#include "ucompose.h"

#include <gtkmm/progressbar.h>
#include <gtkmm/label.h>
#include <gtkmm/button.h>

#include <libglademm/xml.h>

namespace Coaster
{

namespace Dialogs
{

BurnProgress::BurnProgress(BaseObjectType* cobject,
                           const Glib::RefPtr<Gnome::Glade::Xml>& refXml)
: Gtk::Dialog(cobject), m_pProgressBar(0), m_pLabel_Status(0), m_pCancel(0)
{
  using namespace Gtk;

  refXml->get_widget("burn_prog_progressbar", m_pProgressBar);
  refXml->get_widget("burn_prog_status_label", m_pLabel_Status);
  refXml->get_widget("burn_prog_cancel", m_pCancel);

  set_default_response(RESPONSE_CANCEL);
}

BurnProgress::~BurnProgress()
{}

void BurnProgress::on_progress_changed(double fraction)
{
  m_pProgressBar->set_fraction(fraction);
}

void BurnProgress::set_progress(double fraction)
{
  m_pProgressBar->set_fraction(fraction);
}

void BurnProgress::set_text(const Glib::ustring& text)
{
  m_pLabel_Status->set_markup(String::ucompose("<i>%1</i>", text));
}

Glib::SignalProxy0<void> BurnProgress::signal_cancel_clicked()
{
  return m_pCancel->signal_clicked();
}

} // namespace Dialogs

} // namespace Coaster
