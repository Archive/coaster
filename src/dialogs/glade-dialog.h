/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_GLADE_DIALOG_
#define _COASTER_GLADE_DIALOG_

#include <gtkmm/dialog.h>
#include <libglademm/xml.h>
#include "cstr-strings.h"

namespace Coaster
{

namespace Dialogs
{

template<class T>
class GladeDialog
{
public:
  static std::auto_ptr<T> create(Gtk::Window& parent,
                                 const Glib::ustring& node)
  {
    using namespace Gnome::Glade;

    const Glib::RefPtr<Xml> xml = Xml::create(glade_dialogs_filename, node);

    T* tdialog = 0;
    std::auto_ptr<T> dialog(xml->get_widget_derived(node, tdialog));

    dialog->set_transient_for(parent);

    return dialog;
  }
};

template<>
class GladeDialog<Gtk::Dialog>
{
public:
  static std::auto_ptr<Gtk::Dialog> create(Gtk::Window& parent,
                                           const Glib::ustring& node)
  {
    using namespace Gnome::Glade;

    const Glib::RefPtr<Xml> xml = Xml::create(glade_dialogs_filename, node);

    Gtk::Dialog* tdialog = 0;
    std::auto_ptr<Gtk::Dialog> dialog(xml->get_widget(node, tdialog));

    dialog->set_transient_for(parent);

    return dialog;
  }
};

} // namespace Dialogs

} // namespace Coaster

#endif // _COASTER_GLADE_DIALOG_
