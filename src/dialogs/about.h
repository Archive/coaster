/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_ABOUT_H_
#define _COASTER_ABOUT_H_

#include <gtkmm/dialog.h>

#include <gdkmm/cursor.h>
#include <vector>

namespace Gdk
{
class Pixbuf;
} // namespace Gdk

namespace Gtk
{
class Button;
class Image;
class Label;
} // namespace Gtk

namespace Coaster
{

namespace Widgets
{
class SLButton;
} // namespace Widgets

namespace Dialogs
{

class About : public Gtk::Dialog
{
public:
  About(const Glib::ustring& name,
        const Glib::ustring& version,
        const Glib::ustring& copyright,
        const Glib::ustring& comments,
        const Glib::ustring& website,
        const std::vector<Glib::ustring>& authors,
        const std::vector<Glib::ustring>& documenters,
        const Glib::ustring& translators,
        const Glib::RefPtr<Gdk::Pixbuf>& logo);
  virtual ~About();

protected:
  void on_close_clicked();
  void on_credits_clicked();
  void on_credits_close();
  void on_link_clicked();

  void create_credits_dialog();
  
private:
  Gtk::Button*       m_pClose;
  Widgets::SLButton* m_pCredits;

  Gtk::Image*  m_pLogo_Image;
  Gtk::Label*  m_pName_Label;
  Gtk::Label*  m_pComments_Label;
  Gtk::Label*  m_pCopyright_Label;
  Gtk::Button* m_pWebsite_Button;
  
  Gdk::Cursor m_Hand_Cursor;
  Gdk::Cursor m_Regular_Cursor;

  Glib::ustring m_Website;
  std::vector<Glib::ustring> m_vecAuthors;
  std::vector<Glib::ustring> m_vecDocumenters;
  Glib::ustring m_Translators;

  bool m_bCreditsShown;
  Gtk::Dialog* m_pCredits_Dialog;
};

} // namespace Dialogs

} // namespace Coaster

#endif // _COASTER_ABOUT_H_
