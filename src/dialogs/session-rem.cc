/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "session-rem.h"
#include "widgets/discard-button.h"
#include "cstr-intl.h"

#include <iostream>
#include <gtkmm/stock.h>

namespace Coaster
{

DiagSessRemove::DiagSessRemove(bool prev,
                               bool next)
  : Gtk::MessageDialog(_("Would you like to move the files in this session to another session?"), false /* no markup */, Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_NONE)
{
  using namespace Gtk;
  
  set_title(_("Move files?"));

  DiscardButton* pDiscard = new DiscardButton(_("_Discard"));

  add_action_widget(*manage(pDiscard), BUTTON_Discard);
  add_button(Stock::CANCEL, BUTTON_Cancel);
  if(prev)
  {
    SLButton* pPrevSess     = new SLButton(Stock::GO_BACK, _("_Previous"));
    add_action_widget(*manage(pPrevSess), BUTTON_Prev);
  }
  if(next)
  {
    SLButton* pNextSess     = new SLButton(Stock::GO_FORWARD, _("_Next"));
    add_action_widget(*manage(pNextSess), BUTTON_Next);
  }

  set_default_response(BUTTON_Cancel);
}

DiagSessRemove::~DiagSessRemove()
{
}

} // namespace Coaster
