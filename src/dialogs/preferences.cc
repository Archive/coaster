/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gtkmm/treeview.h>
#include "dialogs/preferences.h"

#include "cstr-strings.h"
#include "cstr-intl.h"
#include "client.h"
#include "util.h"
#include "widgets/prefs-list.h"
#include "widgets/stock-label-button.h"
#include "baconmm/cd-selection.h"

#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/entry.h>
#include <gtkmm/label.h>
#include <gtkmm/notebook.h>
#include <gtkmm/table.h>
#include <gtkmm/filechooserdialog.h>
#include <gtkmm/stock.h>

#include <libglademm/xml.h>

namespace Coaster
{

namespace Dialogs
{

Preferences::Preferences(Gtk::Window& parent)
: Bakery::Dialog_Preferences(parent, COASTER_GCONFDIR,
                             glade_prefs_filename, "prefs_main_vbox", true),
  m_pNotebook(0), m_pReader_Label(0), m_pWriter_Label(0), m_pDev_Table(0),
  m_pReader(0), m_pWriter(0), m_pSound_Check(0), m_pBurnFin_Check(0), m_pBurnFin_Entry(0),
  m_pBurnFin_Button(0), m_pBurnErr_Check(0), m_pBurnErr_Entry(0), m_pBurnErr_Button(0)
{
  if(m_pConfClient)
    delete m_pConfClient;

  set_title(_("Coaster Preferences"));
  m_pConfClient = new Coaster::Client();

  m_refGlade->get_widget_derived("prefs_categories", m_pCategories);
  m_refGlade->get_widget("prefs_notebook", m_pNotebook);
  m_refGlade->get_widget("prefs_gen_reader_label", m_pReader_Label);
  m_refGlade->get_widget("prefs_gen_writer_label", m_pWriter_Label);
  m_refGlade->get_widget("prefs_gen_dev_table", m_pDev_Table);
  m_refGlade->get_widget("prefs_not_sound_check", m_pSound_Check);
  m_refGlade->get_widget("prefs_not_burnfin_check", m_pBurnFin_Check);
  m_refGlade->get_widget("prefs_not_burnfin_entry", m_pBurnFin_Entry);
  m_refGlade->get_widget("prefs_not_burnfin_button", m_pBurnFin_Button);
  m_refGlade->get_widget("prefs_not_burnerr_check", m_pBurnErr_Check);
  m_refGlade->get_widget("prefs_not_burnerr_entry", m_pBurnErr_Entry);
  m_refGlade->get_widget("prefs_not_burnerr_button", m_pBurnErr_Button);

  m_pNotebook->set_show_tabs(false);
  m_pNotebook->set_show_border(false);

  // General tab widgets
  m_pReader = new Bacon::CdSelection();
  m_pReader->property_file_image() = false;
  m_pReader->property_show_recorders_only() = false;
  m_pReader->show_all_children();
  m_pReader_Label->set_mnemonic_widget(*m_pReader);

  m_pDev_Table->attach(*Gtk::manage(m_pReader), 1, 2, 0, 1, Gtk::FILL, Gtk::FILL);

  m_pWriter = new Bacon::CdSelection();
  m_pWriter->property_file_image() = false;
  m_pWriter->property_show_recorders_only() = false;
  m_pWriter->show_all_children();
  m_pWriter_Label->set_mnemonic_widget(*m_pWriter);

  m_pDev_Table->attach(*Gtk::manage(m_pWriter), 1, 2, 1, 2, Gtk::FILL, Gtk::FILL);

  // Connect widgets to gconf keys
  m_pConfClient->add_instant("reader", *m_pReader);
  m_pConfClient->add_instant("writer", *m_pWriter);
 
  connect_widget("undo_actions_limit", "prefs_gen_undo_spin");

  connect_widget("layouts/default_size", "prefs_lay_size_combo");
  connect_widget("layouts/remove_warn", "prefs_lay_removewarn_check");
  connect_widget("layouts/title_check", "prefs_lay_titlechk_check");

  connect_widget("notifications/sound_enable", "prefs_not_sound_check");
  connect_widget("notifications/burn_finished", "prefs_not_burnfin_check");
  connect_widget("notifications/burn_finished_file", "prefs_not_burnfin_entry");
  connect_widget("notifications/burn_error", "prefs_not_burnerr_check");
  connect_widget("notifications/burn_error_file", "prefs_not_burnerr_entry");

  // Initialize the entries and buttons
  m_pBurnFin_Check->set_sensitive(m_pSound_Check->get_active());
  m_pBurnErr_Check->set_sensitive(m_pSound_Check->get_active());
  
  m_pBurnFin_Entry->set_sensitive(m_pBurnFin_Check->get_active());
  m_pBurnFin_Button->set_sensitive(m_pBurnFin_Check->get_active());

  m_pBurnErr_Entry->set_sensitive(m_pBurnErr_Check->get_active());
  m_pBurnErr_Button->set_sensitive(m_pBurnFin_Check->get_active());

  // Connect the signals
  m_pCategories->signal_category_changed().connect(sigc::mem_fun(*this, &Preferences::on_category_changed));
 
  m_pSound_Check->signal_toggled().connect(sigc::mem_fun(*this, &Preferences::on_sound_enable_changed));

  m_pBurnFin_Check->signal_toggled().connect(sigc::bind(sigc::mem_fun(*this, &Preferences::on_checkbutton_changed), m_pBurnFin_Check, m_pBurnFin_Entry, m_pBurnFin_Button));
  m_pBurnFin_Button->signal_clicked().connect(sigc::bind(sigc::mem_fun(*this, &Preferences::on_file_button_clicked), m_pBurnFin_Entry, "notifications/burn_finished_file"));
  
  m_pBurnErr_Check->signal_toggled().connect(sigc::bind(sigc::mem_fun(*this, &Preferences::on_checkbutton_changed), m_pBurnErr_Check, m_pBurnErr_Entry, m_pBurnErr_Button));
  m_pBurnErr_Button->signal_clicked().connect(sigc::bind(sigc::mem_fun(*this, &Preferences::on_file_button_clicked), m_pBurnErr_Entry, "notifications/burn_error_file"));

  set_default_size(300,400);
  set_position(Gtk::WIN_POS_CENTER);

  show_all_children();
}

Preferences::~Preferences()
{}

void Preferences::on_category_changed(int id)
{
  m_pNotebook->set_current_page(id);
}

void Preferences::on_sound_enable_changed()
{
  bool active = m_pSound_Check->get_active();
  m_pBurnFin_Check->set_sensitive(active);
  m_pBurnErr_Check->set_sensitive(active);
}

void Preferences::on_checkbutton_changed(Gtk::CheckButton* check,
                                         Gtk::Widget* entry,
                                         Gtk::Widget* button)
{
  entry->set_sensitive(check->get_active());
  button->set_sensitive(check->get_active());
}

void Preferences::on_file_button_clicked(Gtk::Entry* entry,
                                         const Glib::ustring& key)
{
  Gtk::FileChooserDialog fc(*this, "Choose sound to play", Gtk::FILE_CHOOSER_ACTION_OPEN);

  { // Set up button widgets
    Gtk::Button* open_button = new Widgets::SLButton(Gtk::Stock::OPEN, _("_Open"));

    fc.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
    fc.add_action_widget(*Gtk::manage(open_button), Gtk::RESPONSE_OK);
    fc.set_default_response(Gtk::RESPONSE_OK);
    fc.set_filename(Glib::filename_from_utf8(Util::get_string(key)));
  }
  
  int result = fc.run();

  switch(result)
  {
    case Gtk::RESPONSE_CANCEL:
    default:
      break;
    case Gtk::RESPONSE_OK:
      entry->set_text(Glib::filename_to_utf8(fc.get_filename()));
      break;
  }
}

} // namespace Dialogs

} // namespace Coaster
