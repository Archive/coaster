/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_CHOOSER_H_
#define _COASTER_CHOOSER_H_

#include <gtkmm/filechooserdialog.h>

namespace Gtk
{
class CheckButton;
class Button;
}

namespace Coaster
{

namespace Dialogs
{

typedef enum
{
  CHOOSER_TYPE_FILES,
  CHOOSER_TYPE_FOLDER,
  CHOOSER_TYPE_OPEN
} ChooserType;

class Chooser : public Gtk::FileChooserDialog
{
public:
  Chooser(Gtk::Window& parent,
          ChooserType type,
          bool audio = false);
  virtual ~Chooser();

  bool get_hidden_files() const;
  bool get_recursive()    const;
  void set_recursive(bool recursive = true);
private:
  Gtk::FileFilter* m_pAllFiles;

  Gtk::FileFilter* m_pAudioFiles;

  Gtk::FileFilter* m_pAllLayoutFiles;
  Gtk::FileFilter* m_pDataLayoutFiles;
  Gtk::FileFilter* m_pAudioLayoutFiles;
  Gtk::FileFilter* m_pVideoLayoutFiles;
  
  Gtk::CheckButton* m_pHiddenFiles;
  Gtk::CheckButton* m_pRecursive;

  void on_hidden_files();

  ChooserType m_chooser_type_;
  bool        m_audio_filters_;
};

} // namespace Dialogs

} //namespace Coaster

#endif // _COASTER_CHOOSER_H_
