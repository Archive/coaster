/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_DIALOG_BURN_
#define _COASTER_DIALOG_BURN_

#include "sharedptr.h"

#include <gtkmm/dialog.h>
#include <cd-drive.h>

namespace Bacon
{
class CdSelection;
}

namespace Gtk
{
class HBox;
class Image;
class Label;
class Button;
class ComboBox;
class ProgressBar;
class CheckButton;
} // namespace Gtk

namespace Gnome
{
namespace Glade
{
class Xml;
} // namespace Glade
} // namespace Gnome

namespace Coaster
{

namespace Widgets
{
class ComboSpeed;
} // namespace Widgets

namespace Dialogs
{

class Burn : public Gtk::Dialog
{
public:
  Burn(BaseObjectType* cobject,
       const Glib::RefPtr<Gnome::Glade::Xml>& refXml);
  virtual ~Burn();

  CDDrive* get_cd_drive() const;
  bool get_dummy() const;
  int get_speed() const;
  bool get_eject() const;

  void set_action_widgets_sensitive(bool sensitive = true);

protected:
  void on_combo_changed(const Glib::ustring& device);

private:
  Gtk::Image* m_pImage;
  
  Gtk::Label* m_pLabel_Drive;
  Gtk::Label* m_pLabel_Speed;
  
  Gtk::HBox*  m_pHBox_Drive;
  Gtk::HBox*  m_pHBox_Speed;

  Gtk::CheckButton* m_pChkBtn_Dummy;
  Gtk::CheckButton* m_pChkBtn_Eject;
  
  Gtk::Button* m_pOK;
  Gtk::Button* m_pCancel;
  
  Bacon::CdSelection*  m_pComboDrives;
  Widgets::ComboSpeed* m_pComboSpeed;
};

} // namespace Dialogs

} // namespace Coaster

#endif // _COASTER_DIALOG_BURN_
