/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_DIALOG_SESS_REM_
#define _COASTER_DIALOG_SESS_REM_

#include <gtkmm/messagedialog.h>

namespace Coaster
{

class DiagSessRemove : public Gtk::MessageDialog
{
public:
  DiagSessRemove(bool prev = true,
                 bool next = true);
  virtual ~DiagSessRemove();

  //Return values
  enum enumButtons
  {
    BUTTON_Prev,
    BUTTON_Next,
    BUTTON_Discard,
    BUTTON_Cancel
  };
};

} // namespace Coaster

#endif // _COASTER_DIALOG_SESS_REM_
