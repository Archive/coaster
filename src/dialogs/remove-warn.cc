/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "dialogs/remove-warn.h"
#include "client.h"

#include <libglademm/xml.h>

namespace Coaster
{

namespace Dialogs
{

RemoveWarn::RemoveWarn(BaseObjectType* cobject,
                       const Glib::RefPtr<Gnome::Glade::Xml>& refXml)
: ClientDialog(cobject, refXml)
{
  connect_widget("layouts/remove_warn", "removewarn_check");
  load();
}

RemoveWarn::~RemoveWarn()
{}

} // namespace Dialogs

} // namespace Coaster
