/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_DOCUMENT_H_
#define _COASTER_DOCUMENT_H_

#include "cstr-enums.h"
#include "data/data-prop.h"

#include <bakery/Document/Document_XML.h>
#include <gtkmm/treemodel.h>
#include <libgnomevfsmm/types.h>

namespace Coaster
{

namespace Data
{
class Store;
} // namespace Data
  
class Document : public Bakery::Document_XML
{
public:
  Document(LayoutType type);
  virtual ~Document();

  virtual bool load_after();
  virtual bool load();

  virtual void start_save();

  virtual LayoutType get_layout_type() const;
  
  xmlpp::Node::NodeList get_session_nodes();

  void set_document_properties(const Data::Properties& dp);
  void get_document_properties(Data::Properties& dp) const;

  void save_tree(const Glib::RefPtr<Data::Store>& refStore);
  void load_tree(Glib::RefPtr<Data::Store>& refStore,
                 const sigc::slot<bool,const Gnome::Vfs::FileSize&>& slot_add);

protected:
  virtual void handle_exception(const std::exception& ex) const;
private:
  LayoutType m_layout_type_;
};

} // namespace Coaster

#endif // _COASTER_DOCUMENT_H_
