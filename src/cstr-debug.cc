/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "cstr-debug.h"

#include <iostream>
#include <libxml++/document.h>

namespace Coaster
{

extern bool coaster_debug;
extern bool coaster_debug_xml;
extern bool coaster_debug_dnd;

void debug(const Glib::ustring& string)
{
  if(coaster_debug)
    std::cout << string << std::endl;
}

void debug(int i)
{
  if(coaster_debug)
    std::cout << i << std::endl;
}

bool debug()
{
  return coaster_debug;
}

void debug(const xmlpp::Element* node)
{
  if(coaster_debug_xml)
  {
    xmlpp::Document doc;
    doc.create_root_node_by_import(node);
    std::cout << doc.write_to_string_formatted() << std::endl;
  }
}

void debug(xmlpp::Document* doc)
{
  if(coaster_debug_xml)
    std::cout << doc->write_to_string_formatted() << std::endl;
}

void debug_dnd(const Glib::ustring& string)
{
  if(coaster_debug_dnd)
    std::cout << string << std::endl;
}

} // namespace Coaster
