#ifndef _BACONMM_CD_SELECTION_H
#define _BACONMM_CD_SELECTION_H

#include <glibmm.h>

/* cd-selection.h
 * 
 * Copyright (C) 2002-2004 The Coaster Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtkmm/combobox.h>
#include <bacon-cd-selection.h>
#include <cd-drive.h>

namespace Bacon
{ class CdSelection_Class; } // namespace Bacon
namespace Bacon
{

class CdSelection : public Gtk::ComboBox
{
public:
#ifndef DOXYGEN_SHOULD_SKIP_THIS
  typedef CdSelection CppObjectType;
  typedef CdSelection_Class CppClassType;
  typedef BaconCdSelection BaseObjectType;
  typedef BaconCdSelectionClass BaseClassType;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

  virtual ~CdSelection();

#ifndef DOXYGEN_SHOULD_SKIP_THIS

private:
  friend class CdSelection_Class;
  static CppClassType cd_selection_class_;

  // noncopyable
  CdSelection(const CdSelection&);
  CdSelection& operator=(const CdSelection&);

protected:
  explicit CdSelection(const Glib::ConstructParams& construct_params);
  explicit CdSelection(BaconCdSelection* castitem);

#endif /* DOXYGEN_SHOULD_SKIP_THIS */

public:
#ifndef DOXYGEN_SHOULD_SKIP_THIS
  static GType get_type()      G_GNUC_CONST;
  static GType get_base_type() G_GNUC_CONST;
#endif

  ///Provides access to the underlying C GtkObject.
  BaconCdSelection*       gobj()       { return reinterpret_cast<BaconCdSelection*>(gobject_); }

  ///Provides access to the underlying C GtkObject.
  const BaconCdSelection* gobj() const { return reinterpret_cast<BaconCdSelection*>(gobject_); }


public:
  //C++ methods used to invoke GTK+ virtual functions:

protected:
  //GTK+ Virtual Functions (override these to change behaviour):

  //Default Signal Handlers::


private:

public:
  CdSelection();

  void set_device(const Glib::ustring& device);

  Glib::ustring get_device() const;

  Glib::ustring get_default_device() const;

  const CDDrive* get_cdrom() const;

  Glib::PropertyProxy<Glib::ustring> property_device();
  Glib::PropertyProxy<bool> property_file_image();
  Glib::PropertyProxy<bool> property_show_recorders_only();

  Glib::SignalProxy1<void,const Glib::ustring&> signal_device_changed();

  void on_device_changed(const Glib::ustring& device);
};

} // namespace Bacon

namespace Glib
{
  Bacon::CdSelection* wrap(BaconCdSelection* object, bool take_copy = false);
}


#endif /* _BACONMM_CD_SELECTION_H */
