#include "baconmm/cd-selection.h"
#include "baconmm/cd-selection_p.h"

/* 
 * Copyright 2002-2004 The Coaster Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <bacon-cd-selection.h>
#include <cd-drive.h>

namespace
{

void CdSelection_signal_device_changed_callback(BaconCdSelection* self, const char* p0, void* data)
{
  using namespace Bacon;
  typedef sigc::slot< void,const Glib::ustring& > SlotType;

  // Do not try to call a signal on a disassociated wrapper.
  if(Glib::ObjectBase::_get_current_wrapper((GObject*) self))
  {
    try
    {
      if(sigc::slot_base *const slot = Glib::SignalProxyNormal::data_to_slot(data))
        (*static_cast<SlotType*>(slot))(Glib::ustring(p0));
    }
    catch(...)
    {
      Glib::exception_handlers_invoke();
    }
  }
}
  
const Glib::SignalProxyInfo CdSelection_signal_device_changed_info =
{
  "device_changed",
  (GCallback) &CdSelection_signal_device_changed_callback,
  (GCallback) &CdSelection_signal_device_changed_callback
};

} // anonymous namespace

namespace Glib
{

Bacon::CdSelection* wrap(BaconCdSelection* object, bool take_copy)
{
  return dynamic_cast<Bacon::CdSelection *> (Glib::wrap_auto ((GObject*)(object), take_copy));
}

} /* namespace Glib */

namespace Bacon
{

/* The *_Class implementation: */

const Glib::Class& CdSelection_Class::init()
{
  if(!gtype_) // create the GType if necessary
  {
    // Glib::Class has to know the class init function to clone custom types.
    class_init_func_ = &CdSelection_Class::class_init_function;

    // This is actually just optimized away, apparently with no harm.
    // Make sure that the parent type has been created.
    //CppClassParent::CppObjectType::get_type();

    // Create the wrapper type, with the same class/instance size as the base type.
    register_derived_type(bacon_cd_selection_get_type());

    // Add derived versions of interfaces, if the C type implements any interfaces:
  }

  return *this;
}

void CdSelection_Class::class_init_function(void* g_class, void* class_data)
{
  BaseClassType *const klass = static_cast<BaseClassType*>(g_class);
  CppClassParent::class_init_function(klass, class_data);

  klass->device_changed = &device_changed_callback;
}

void CdSelection_Class::device_changed_callback(GtkWidget* self, const char* device_path)
{
  CppObjectType *const obj = dynamic_cast<CppObjectType*>(
      Glib::ObjectBase::_get_current_wrapper((GObject*)self));

  // Non-gtkmmproc-generated custom classes implicitly call the default
  // Glib::ObjectBase constructor, which sets is_derived_. But gtkmmproc-
  // generated classes can use this optimisation, which avoids the unnecessary
  // parameter conversions if there is no possibility of the virtual function
  // being overridden:
  if(obj && obj->is_derived_())
  {
    try // Trap C++ exceptions which would normally be lost because this is a C callback.
    {
      // Call the virtual member method, which derived classes might override.
      obj->on_device_changed(Glib::convert_const_gchar_ptr_to_ustring(device_path));
    }
    catch(...)
    {
      Glib::exception_handlers_invoke();
    }
  }
  else
  {
    BaseClassType *const base = static_cast<BaseClassType*>(
        g_type_class_peek_parent(G_OBJECT_GET_CLASS(self)) // Get the parent class of the object class (The original underlying C class).
    );

    // Call the original underlying C function:
    if(base && base->device_changed)
      (*base->device_changed)(self,device_path);
  }
}

Glib::ObjectBase* CdSelection_Class::wrap_new(GObject* o)
{
  return manage(new CdSelection((BaconCdSelection*)(o)));

}


/* The implementation: */

CdSelection::CdSelection(const Glib::ConstructParams& construct_params)
:
  Gtk::ComboBox(construct_params)
{
  }

CdSelection::CdSelection(BaconCdSelection* castitem)
:
  Gtk::ComboBox((GtkComboBox*)(castitem))
{
  }

CdSelection::~CdSelection()
{
  destroy_();
}

CdSelection::CppClassType CdSelection::cd_selection_class_; // initialize static member

GType CdSelection::get_type()
{
  return cd_selection_class_.init().get_type();
}

GType CdSelection::get_base_type()
{
  return bacon_cd_selection_get_type();
}

CdSelection::CdSelection()
: Glib::ObjectBase(0), //Mark this class as gtkmmproc-generated, rather than a custom class, to allow vfunc optimisations.
  Gtk::ComboBox(Glib::ConstructParams(cd_selection_class_.init()))
{
}

void CdSelection::set_device(const Glib::ustring& device)
{
  bacon_cd_selection_set_device(gobj(), device.c_str());
}

Glib::ustring CdSelection::get_device() const
{
  return Glib::convert_const_gchar_ptr_to_ustring(bacon_cd_selection_get_device(const_cast<BaconCdSelection*>(gobj())));
}

Glib::ustring CdSelection::get_default_device() const
{
  return Glib::convert_const_gchar_ptr_to_ustring(bacon_cd_selection_get_default_device(const_cast<BaconCdSelection*>(gobj())));
}

const CDDrive* CdSelection::get_cdrom() const
{
  return bacon_cd_selection_get_cdrom(const_cast<BaconCdSelection*>(gobj()));
}

Glib::PropertyProxy<Glib::ustring> CdSelection::property_device()
{
  return Glib::PropertyProxy<Glib::ustring>(this, "device");
}

Glib::PropertyProxy<bool> CdSelection::property_file_image()
{
  return Glib::PropertyProxy<bool>(this, "file_image");
}

Glib::PropertyProxy<bool> CdSelection::property_show_recorders_only()
{
  return Glib::PropertyProxy<bool>(this, "show_recorders_only");
}

Glib::SignalProxy1<void,const Glib::ustring&> CdSelection::signal_device_changed()
{
  return Glib::SignalProxy1<void,const Glib::ustring&>(this, &CdSelection_signal_device_changed_info);
}

void CdSelection::on_device_changed(const Glib::ustring& device_path)
{
  BaseClassType *const base = static_cast<BaseClassType*>(
      g_type_class_peek_parent(G_OBJECT_GET_CLASS(gobject_)) // Get the parent class of the object class (The original underlying C class).
  );

  if(base && base->device_changed)
    (*base->device_changed)(GTK_WIDGET(gobj()),device_path.c_str());
}

} // namespace Bacon
