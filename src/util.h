/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_UTIL_H_
#define _COASTER_UTIL_H_

#include <glibmm/ustring.h>

#include <gtkmm/icontheme.h>
#include <gtkmm/window.h>

namespace Gdk
{
class Pixbuf;
}

namespace Coaster
{

namespace Util
{

// GConf
bool get_bool(const Glib::ustring& key);
Glib::ustring get_string(const Glib::ustring& key);
int get_int(const Glib::ustring& key);

// String/Filename functions:
Glib::ustring create_full_uri(const Glib::ustring& path);
Glib::ustring create_full_path(const Glib::ustring& path);
bool is_uri(const Glib::ustring& path);

// Parent
inline Gtk::Window& get_win(Gtk::Widget* widget)
{
  return static_cast<Gtk::Window&>(*(widget->get_toplevel()));
}

} // namespace Util

} // namespace Coaster

#endif // _COASTER_UTIL_
