/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "document.h"
#include "widgets/view-base.h"
#include "data/data-view.h"
#include "data/data-file.h"
#include "data/data-store.h"
#include "data/data-columns.h"

#include "xml-io.h"
#include "ucompose.h"

#include <gtkmm/treestore.h>
#include <bakery/App/GtkDialogs.h>

namespace Coaster
{

Document::Document(LayoutType type)
: m_layout_type_(type)
{
  Glib::ustring dtd_name = "coaster-data.dtd";

  switch(m_layout_type_)
  {
    case COASTER_LAYOUT_AUDIO:
      dtd_name = "coaster-audio.dtd";
      m_pView = new Data::View(); // fix this
      break;
    case COASTER_LAYOUT_VIDEO:
      dtd_name = "coaster-video.dtd";
      m_pView = new Data::View(); // fix this
      break;
    case COASTER_LAYOUT_DATA:
      m_pView = new Data::View();
    default:
      break;
  }

  set_file_extension("blf");
  set_write_formatted();

  set_dtd_name(String::ucompose("http://www.coaster-burn.org/%1", dtd_name));
  set_dtd_root_node_name("disclayout");
}

Document::~Document()
{}

bool Document::load_after()
{
  m_DOM_Parser.set_validate();
  return Document_XML::load_after();
}

bool Document::load()
{
  bool bTest = read_from_disk();
  if(bTest)
    bTest = load_after();
  
  return bTest;
}

void Document::start_save()
{
  m_pDOM_Document = m_DOM_Parser.get_document();

  Glib::ustring pub_id = "-//COASTER//DTD Data";
  Glib::ustring type = "data";
  switch(m_layout_type_)
  {
    case COASTER_LAYOUT_AUDIO:
      pub_id = "-//COASTER//DTD Audio";
      type = "audio";
      break;
    case COASTER_LAYOUT_VIDEO:
      pub_id = "-//COASTER//DTD Video";
      type = "video";
      break;
    case COASTER_LAYOUT_DATA:
    default:
      break;
  }

  m_pDOM_Document->set_internal_subset(m_strRootNodeName, pub_id, m_strDTD_Name);
  m_pDOM_Document->create_root_node(m_strRootNodeName);
  get_node_document()->set_attribute("type", type);
}

LayoutType Document::get_layout_type() const
{
  return m_layout_type_;
}

xmlpp::Node::NodeList Document::get_session_nodes()
{
  xmlpp::Node::NodeList result;
  try
  {
    xmlpp::Element* nodeRoot = get_node_document();
    result = nodeRoot->get_children("session");
  }
  catch(const std::exception& ex)
  {
    handle_exception(ex);
  }

  return result;
}

/*void Document::set_document_properties(const Data::Properties& dp, CDSizeType size)
{
  Glib::ustring temp_size_str = "650";
  Glib::ustring temp_iso_level_str = "1";
  Glib::ustring temp_joliet_str = "no";
  Glib::ustring temp_rockridge_str = "no";
  Glib::ustring temp_session_str = "cdrom";
  
  xmlpp::Element *nodeRoot = get_node_document();
 
  switch(size)
  {
    case COASTER_CD_SIZE_700:
      temp_size_str = "700";
      break;
    case COASTER_CD_SIZE_650:
    default:
      temp_size_str = "650";
      break;
  }
  nodeRoot->set_attribute("size", temp_size_str);

  nodeRoot->set_attribute("title", dp.title);
  nodeRoot->set_attribute("catnum", dp.catnum);
  nodeRoot->set_attribute("publisher", dp.publisher);
  nodeRoot->set_attribute("author", dp.author);

  if(dp.isolevel == COASTER_ISO_LEVEL2)
    temp_iso_level_str = "2";

  if(dp.joliet)
    temp_joliet_str = "yes";
  if(dp.rockridge)
    temp_rockridge_str = "yes";
  
  switch(dp.sessionformat)
  {
    case COASTER_SESSION_FMT_CDI:
      temp_session_str = "cdi";
      break;
    case COASTER_SESSION_FMT_CDXA:
      temp_session_str = "cdxa";
      break;
    case COASTER_SESSION_FMT_CDROM:
      temp_session_str = "cdrom";
      break;
  }
  
  nodeRoot->set_attribute("isolevel", temp_iso_level_str);
  nodeRoot->set_attribute("joliet", temp_joliet_str);
  nodeRoot->set_attribute("rockridge", temp_rockridge_str);
  nodeRoot->set_attribute("sessionformat", temp_session_str);
}*/

void Document::set_document_properties(const Data::Properties& dp)
{
  Glib::ustring temp_append_date_str = "no";
  Glib::ustring temp_cd_size_str = "650";
  Glib::ustring temp_iso_level_str = "1";
  Glib::ustring temp_joliet_str = "no";
  Glib::ustring temp_rockridge_str = "no";
  Glib::ustring temp_session_str = "cdrom";
  
  xmlpp::Element *nodeRoot = get_node_document();
 
  if(dp.size == COASTER_CD_SIZE_700)
    temp_cd_size_str = "700";
  nodeRoot->set_attribute("size", temp_cd_size_str);

  nodeRoot->set_attribute("title", dp.title);

  if(dp.append_date)
    temp_append_date_str = "yes";
  nodeRoot->set_attribute("append_date", temp_append_date_str);

  nodeRoot->set_attribute("catnum", dp.catnum);
  nodeRoot->set_attribute("publisher", dp.publisher);
  nodeRoot->set_attribute("author", dp.author);

  if(dp.isolevel == COASTER_ISO_LEVEL2)
    temp_iso_level_str = "2";

  if(dp.joliet)
    temp_joliet_str = "yes";
  if(dp.rockridge)
    temp_rockridge_str = "yes";
  
  switch(dp.sessionformat)
  {
    case COASTER_SESSION_FMT_CDI:
      temp_session_str = "cdi";
      break;
    case COASTER_SESSION_FMT_CDXA:
      temp_session_str = "cdxa";
      break;
    case COASTER_SESSION_FMT_CDROM:
      temp_session_str = "cdrom";
      break;
  }
  
  nodeRoot->set_attribute("isolevel", temp_iso_level_str);
  nodeRoot->set_attribute("joliet", temp_joliet_str);
  nodeRoot->set_attribute("rockridge", temp_rockridge_str);
  nodeRoot->set_attribute("sessionformat", temp_session_str);
}

void Document::get_document_properties(Data::Properties& dp) const
{
  xmlpp::Element *nodeRoot = const_cast<xmlpp::Element*>(get_node_document());
 
  CDSizeType tempSizeType;
  Glib::ustring tempSizeTypeStr = get_node_attribute_value(nodeRoot, "size");

  if(tempSizeTypeStr == "")
    tempSizeType = COASTER_CD_SIZE_NONE;
  else if(tempSizeTypeStr == "700")
    tempSizeType = COASTER_CD_SIZE_700;
  else
    tempSizeType = COASTER_CD_SIZE_650;

  dp.size = tempSizeType;

  dp.title = get_node_attribute_value(nodeRoot, "title");
  dp.append_date = (get_node_attribute_value(nodeRoot, "append_date") == "yes" ? true : false);
  dp.catnum = get_node_attribute_value(nodeRoot, "catnum");
  dp.publisher = get_node_attribute_value(nodeRoot, "publisher");
  dp.author = get_node_attribute_value(nodeRoot, "author");

  dp.isolevel = (get_node_attribute_value(nodeRoot, "isolevel") == "1" ? 
                                            COASTER_ISO_LEVEL1 : COASTER_ISO_LEVEL2);
  dp.joliet = (get_node_attribute_value(nodeRoot, "joliet") == "no" ? false : true);
  dp.rockridge = (get_node_attribute_value(nodeRoot, "rockridge") == "no" ? false : true);

  SessionFormat tempFormat;
  Glib::ustring tempStringType = get_node_attribute_value(nodeRoot, "sessionformat");

  if(tempStringType == "cdi")
    tempFormat = COASTER_SESSION_FMT_CDI;
  else if(tempStringType == "cdxa")
    tempFormat = COASTER_SESSION_FMT_CDXA;
  else
    tempFormat = COASTER_SESSION_FMT_CDROM;

  dp.sessionformat = tempFormat;
}

/*Data::Properties Document::get_data_document_properties() const
{
  Data::Properties dp;
  xmlpp::Element *nodeRoot = get_node_document();

  CDSizeType tempSize;
  Glib::ustring tempSizeStr = get_node_attribute_value(nodeRoot, "size");

  if(tempSizeStr == "700")
    tempSize = COASTER_CD_SIZE_700;
  else
    tempSize = COASTER_CD_SIZE_650;

  dp.size = tempSize;
  
  dp.title = get_node_attribute_value(nodeRoot, "title");
  dp.catnum = get_node_attribute_value(nodeRoot, "catnum");
  dp.publisher = get_node_attribute_value(nodeRoot, "publisher");
  dp.author = get_node_attribute_value(nodeRoot, "author");

  dp.isolevel = (get_node_attribute_value(nodeRoot, "isolevel") == "1" ? 
                                            COASTER_ISO_LEVEL1 : COASTER_ISO_LEVEL2);
  dp.joliet = (get_node_attribute_value(nodeRoot, "joliet") == "no" ? false : true);
  dp.rockridge = (get_node_attribute_value(nodeRoot, "rockridge") == "no" ? false : true);
  
  SessionFormat tempFormat;
  Glib::ustring tempStringType = get_node_attribute_value(nodeRoot, "sessionformat");

  if(tempStringType == "cdi")
    tempFormat = COASTER_SESSION_FMT_CDI;
  else if(tempStringType == "cdxa")
    tempFormat = COASTER_SESSION_FMT_CDXA;
  else
    tempFormat = COASTER_SESSION_FMT_CDROM;

  dp.sessionformat = tempFormat;

  return dp;
}*/

void Document::save_tree(const Glib::RefPtr<Data::Store>& refStore)
{
  xmlpp::Element* nodeRoot = m_pDOM_Document->get_root_node();

  if(!(refStore->children().empty()))
  {
    XmlIO::tree_branch_to_xml(refStore->children(), nodeRoot);
  }
}

void Document::load_tree(Glib::RefPtr<Data::Store>& refStore,
                         const sigc::slot<bool,const Gnome::Vfs::FileSize&>& slot_add)
{
  using namespace Gtk;

  Gtk::TreeNodeChildren children = refStore->children();
  XmlIO::tree_branch_from_xml(children,
                              m_pDOM_Document->get_root_node(),
                              refStore,
                              slot_add);
}

void Document::handle_exception(const std::exception& ex) const
{
  std::cerr << "Exception: " << ex.what() << std::endl;
}

} // namespace Coaster
