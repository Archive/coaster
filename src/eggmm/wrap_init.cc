
#include <glib.h>

// Disable the 'const' function attribute of the get_type() functions.
// GCC would optimize them out because we don't use the return value.
#undef  G_GNUC_CONST
#define G_GNUC_CONST /* empty */

#include "wrap_init.h"
#include <glibmm/error.h>
#include <glibmm/object.h>

// #include the widget headers so that we can call the get_type() static methods:

#include "eggmm/tray-icon.h"
#include "eggmm/status-icon.h"

extern "C"
{

//Declarations of the *_get_type() functions:

GType egg_status_icon_get_type(void);
GType egg_tray_icon_get_type(void);

//Declarations of the *_error_quark() functions:

} // extern "C"


//Declarations of the *_Class::wrap_new() methods, instead of including all the private headers:

namespace Egg {  class StatusIcon_Class { public: static Glib::ObjectBase* wrap_new(GObject*); };  }
namespace Egg {  class TrayIcon_Class { public: static Glib::ObjectBase* wrap_new(GObject*); };  }

namespace Egg { 

void wrap_init()
{
  // Register Error domains:

// Map gtypes to gtkmm wrapper-creation functions:
  Glib::wrap_register(egg_status_icon_get_type(), &Egg::StatusIcon_Class::wrap_new);
  Glib::wrap_register(egg_tray_icon_get_type(), &Egg::TrayIcon_Class::wrap_new);

  // Register the gtkmm gtypes:
  Egg::StatusIcon::get_type();
  Egg::TrayIcon::get_type();

} // wrap_init()

} //Egg


