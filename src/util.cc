/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "util.h"

#include "cstr-strings.h"

#include <config.h>

#include "ucompose.h"

#include <gdkmm/pixbuf.h>

#include <gconfmm/client.h>
#include <libgnomevfsmm/uri.h>
#include <libgnomevfsmm/utils.h>

#include <iostream>

namespace Coaster
{

namespace Util
{

bool get_bool(const Glib::ustring& key)
{
  return Gnome::Conf::Client::get_default_client()->get_bool(COASTER_GCONFDIR + "/" + key);
}

Glib::ustring get_string(const Glib::ustring& key)
{
  return Gnome::Conf::Client::get_default_client()->get_string(COASTER_GCONFDIR + "/" + key);
}

int get_int(const Glib::ustring& key)
{
  return Gnome::Conf::Client::get_default_client()->get_int(COASTER_GCONFDIR + "/" + key);
}

Glib::ustring create_full_uri(const Glib::ustring& path)
{
  if(!path.size())
    return Glib::ustring();

  if(is_uri(path))
    return path;

  if(path[0] == '/' || path[0] == '~')
  {
    Glib::ustring escaped = Gnome::Vfs::escape_path_string(path);
    Glib::ustring retval = Glib::ustring("file://") + escaped;
    return retval;
  }

  Glib::ustring curdir = Glib::get_current_dir();
  Glib::ustring escaped = Gnome::Vfs::escape_path_string(curdir);
  Glib::ustring curdir_withslash = String::ucompose("file://%1%2", escaped, G_DIR_SEPARATOR_S);

  escaped = Gnome::Vfs::escape_path_string(path);
  Glib::ustring retval = Gnome::Vfs::Uri::make_full_from_relative(curdir_withslash, escaped);
  return retval;
}

Glib::ustring create_full_path(const Glib::ustring& path)
{
  if(!path.size())
    return Glib::ustring();

  if(path[0] == '/' || path[0] == '~')
    return path;

  Glib::ustring curdir = Glib::get_current_dir();
  Glib::ustring curdir_withslash = String::ucompose("file://%1%2", curdir, G_DIR_SEPARATOR_S, path);
  return curdir_withslash;
}

bool is_uri(const Glib::ustring& path)
{
  return (path.find("file://") != Glib::ustring::npos);
}

} // namespace Util

} // namespace Coaster
