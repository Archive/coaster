/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_DEBUG_H_
#define _COASTER_DEBUG_H_

#include <glibmm/ustring.h>
#include <ucompose.h>

namespace xmlpp
{
class Document;
class Element;
} // namespace xmlpp

namespace Coaster
{

// Debugging Statements
void debug(const Glib::ustring& string);
void debug(int i);
bool debug();

template<typename T1>
inline void debug(const Glib::ustring& string,
                  const T1& o1)
{
  debug(String::ucompose("%1%2",string,o1));
}

template<typename T1,typename T2>
inline void debug(const Glib::ustring& string,
                  const T1& o1,
                  const T2& o2)
{
  debug(String::ucompose("%1%2%3",string,o1,o2));
}

// Xml Debugging Statements
void debug(const xmlpp::Element* node);
void debug(xmlpp::Document* doc);

// Drag-n-Drop Debugging Statements
void debug_dnd(const Glib::ustring& string);

template<typename T1>
inline void debug_dnd(const Glib::ustring& string,
                      const T1& o1)
{
  debug_dnd(String::ucompose("%1%2",string,o1));
}

template<typename T1,typename T2>
inline void debug_dnd(const Glib::ustring& string,
                      const T1& o1,
                      const T2& o2)
{
  debug_dnd(String::ucompose("%1%2%3",string,o1,o2));
}

} // namespace Coaster

#endif // _COASTER_DEBUG_H_
