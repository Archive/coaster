/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <unistd.h>
#include <config.h>

#include "cstr-intl.h"
#include "cstr-debug.h"
#include "cstr-strings.h"

#include "ucompose.h"
#include "mainwindow.h"
#include "instance-manager.h"
#include "stock-registration.h"

#include <gtkmm/main.h>
#include <libgnome/gnome-init.h>
#include <libgnomeui/gnome-ui-init.h>
#include <bakery/init.h>

#include "eggmm/wrap_init.h"
#include "baconmm/wrap_init.h"

#include <exception>
#include <list>

namespace Coaster
{
bool coaster_debug = false;
bool coaster_debug_xml = false;
bool coaster_debug_dnd = false;
}

namespace
{

int d = 0;
int dx = 0;
int dd = 0;
char *debug_modules = NULL;
const struct poptOption options[] = {
  {"debug", '\0', POPT_ARG_NONE, &d, 0, N_("Enable debugging statements"), NULL},
  {"debug-xml", '\0', POPT_ARG_NONE, &dx, 0, N_("Enable xml debugging statements"), NULL},
  {"debug-dnd", '\0', POPT_ARG_NONE, &dd, 0, N_("Enable DnD debugging statements"), NULL},
	{"debug-modules", '\0', POPT_ARG_STRING, &debug_modules, 0,
	 N_("Modules to enable debugging in"), N_("MODULES")},
	{NULL, '\0', 0, NULL, 0} /* end the list */
};

} // anonymous namespace

int main(int argc,
         char **argv)
{
  //Make this application use the current locale for gettext() translation:
  bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);  //LOCALEDIR is defined in the Makefile.am
  bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
  textdomain(GETTEXT_PACKAGE);
  
	try
	{
    gnome_program_init(PACKAGE_NAME, PACKAGE_VERSION, LIBGNOMEUI_MODULE,
                       argc, argv,
                       GNOME_PARAM_POPT_TABLE, options,
                       GNOME_PARAM_POPT_FLAGS, 0,
                       GNOME_PROGRAM_STANDARD_PROPERTIES, 0);

    Gtk::Main mainInstance(argc, argv);
    Egg::wrap_init();
#ifdef COASTER_BACKEND_NAUTILUS
    Bacon::wrap_init();
#endif // COASTER_BACKEND_NAUTILUS

    if(d)
      Coaster::coaster_debug = true;
    if(dx)
      Coaster::coaster_debug_xml = true;
    if(dd)
      Coaster::coaster_debug_dnd = true;
    
    Bakery::init();

    Glib::ustring input_uri;
    if(argc > 1)
    {
      for(int i = 1 ; i < argc ; i++)
      {
        input_uri = argv[i];

        //Ignore arguements starting with "--", or "-".
        if(input_uri.size() && input_uri[0] == '-')
        {
          input_uri = Glib::ustring();
          continue;
        }
        else
          break;
      }
    }

    if(!input_uri.empty() && Coaster::debug())
      std::cout << input_uri << std::endl;

    // Stock stuff
    Coaster::register_stock_items();
    Coaster::set_window_icon(COASTER_ICONDIR G_DIR_SEPARATOR_S "coaster.png");

    // Instance manager initialization
    Coaster::SharedPtr<Coaster::InstanceMgr> instmgr(Coaster::InstanceMgr::instance());
    instmgr->init();

    Glib::RefPtr<Gnome::Glade::Xml> refXml = Gnome::Glade::Xml::create(Coaster::glade_main_filename, "window_main");
    Coaster::MainWindow *pMainWindow = 0;
    refXml->get_widget_derived("window_main", pMainWindow);

    pMainWindow->set_command_line_args(argc, argv);
    bool test = pMainWindow->init(input_uri);

    if(test)
    {
      gdk_threads_enter();
      Gtk::Main::run(*pMainWindow);
      gdk_threads_leave();
    }
    else
    {
      delete pMainWindow;
    }
	}
	catch(const Glib::Error& error)
	{
		const Glib::ustring what = error.what();
		g_error("unhandled exception: %s", what.c_str());
	}
	catch(const std::exception& except)
	{
		g_error("unhandled exception: %s", except.what());
	}
	catch(...)
	{
		g_error("unhandled exception: type unknown");
	}

	return 0;
}
