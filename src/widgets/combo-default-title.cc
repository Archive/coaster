/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "widgets/combo-default-title.h"
#include "ucompose.h"

#include <gtkmm/liststore.h>
#include <gtkmm/cellrenderertext.h>

#include <libglademm/xml.h>

namespace Coaster
{

ComboDefaultTitle::ComboDefaultTitle()
: Gtk::ComboBox()
{
  using namespace Gtk;
  ComboColumns columns;
  m_refStore = ListStore::create(columns);

  set_model(m_refStore);

  CellRendererText *const cell_name = new CellRendererText();
  pack_start(*manage(cell_name), true);
  add_attribute(cell_name->property_text(), columns.m_col_name);

  TreeIter iter = m_refStore->append();
  (*iter).set_value(columns.m_col_name, Glib::ustring(_("None")));
  (*iter).set_value(columns.m_col_gconf_value, Glib::ustring("none"));

  iter = m_refStore->append();
  (*iter).set_value(columns.m_col_name, Glib::ustring(_("Date")));
  (*iter).set_value(columns.m_col_gconf_value, Glib::ustring("date"));

  iter = m_refStore->append();
  (*iter).set_value(columns.m_col_name, Glib::ustring(_("Ask before burning if not set")));
  (*iter).set_value(columns.m_col_gconf_value, Glib::ustring("ask"));
}

ComboDefaultTitle::~ComboDefaultTitle()
{}

Glib::ustring ComboDefaultTitle::get_gconf_value() const
{
  ComboColumns columns;
  if(get_active_row_number() == -1)
    return "";
  else
    return (*(get_active())).get_value(columns.m_col_gconf_value);
}

void ComboDefaultTitle::set_from_gconf_value(const Glib::ustring& value)
{
  if(value == "none")
    set_active(0);
  else if(value == "date")
    set_active(1);
  else // value == "ask" or something else
    set_active(2);
}

} // namespace Coaster
