/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "widgets/cd-tray-icon.h"

#include <gdkmm/pixbuf.h>
#include <gtkmm/eventbox.h>
#include <gtkmm/image.h>

namespace Coaster
{

namespace Widgets
{

CDTrayIcon::CDTrayIcon(const Glib::RefPtr<Gdk::Pixbuf>& pixbuf)
: Egg::TrayIcon("Coaster Tray Icon"), m_pEventBox(0), m_pImage(0)
{
  m_Tooltips.enable();

  m_pEventBox = new Gtk::EventBox();
  m_pImage = new Gtk::Image(pixbuf);
  
  m_pEventBox->add(*manage(m_pImage));
  add(*manage(m_pEventBox));
}

CDTrayIcon::~CDTrayIcon()
{}

void CDTrayIcon::set_pixbuf(const Glib::RefPtr<Gdk::Pixbuf>& pixbuf)
{
  m_pImage->set(pixbuf);
}

void CDTrayIcon::set_tip(const Glib::ustring& tip, const Glib::ustring& private_tip)
{
  m_Tooltips.set_tip(*m_pEventBox, tip, private_tip);
}

} // namespace Widgets

} // namespace Coaster
