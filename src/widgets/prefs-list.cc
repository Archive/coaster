/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "widgets/prefs-list.h"
#include "widgets/cellrenderer-icontext.h"
#include "cstr-intl.h"

#include <gtkmm/liststore.h>
#include <gtkmm/stock.h>
#include <libglademm/xml.h>

namespace
{

typedef struct
{
  const Glib::ustring name;
  const Gtk::StockID  stock_id;
  int id;
} Category;

Category categories[] = {
  { N_("General"), Gtk::Stock::PREFERENCES, 0 },
  { N_("Layouts"), Gtk::Stock::COPY, 1},
  { N_("Notifications"), Gtk::Stock::DIALOG_WARNING, 2}
};

int n_categories = G_N_ELEMENTS(categories);

} // anonymous namespace

namespace Coaster
{

namespace Widgets
{

PrefsList::PrefsList(BaseObjectType* cobject,
                     const Glib::RefPtr<Gnome::Glade::Xml>& refGlade)
: Gtk::TreeView(cobject), m_colCategory(0)
{
  using namespace Gtk;
  
  m_refSelection = get_selection();

  m_refStore = ListStore::create(m_model_columns);
  set_model(m_refStore);

  for(int i = 0 ; i < n_categories ; i++)
  {
    Glib::RefPtr<Gdk::Pixbuf> p = render_icon(categories[i].stock_id, Gtk::ICON_SIZE_LARGE_TOOLBAR);
    Gtk::TreeRow row = *(m_refStore->append());
    row[m_model_columns.m_col_name] = _(categories[i].name.c_str());
    row[m_model_columns.m_col_icon] = p;
    row[m_model_columns.m_col_id] = categories[i].id;
  }

  {
    m_colCategory = new Column(_("Category"));
    append_column(*manage(m_colCategory));

    CellRendererIconText *const cell_icontext = new CellRendererIconText();

    cell_icontext->property_editable() = false;
    m_colCategory->pack_start(*manage(cell_icontext));
    m_colCategory->set_cell_data_func(*cell_icontext, sigc::mem_fun(*this,
                                                                    &PrefsList::icon_data_func));
    m_colCategory->add_attribute(cell_icontext->property_text(), m_model_columns.m_col_name);
    m_colCategory->set_spacing(2);
  }

  m_refSelection->signal_changed().connect(sigc::mem_fun(*this, &PrefsList::on_selection_changed));
  m_refSelection->select(m_refStore->children().begin());

  show_all_children();
}

PrefsList::~PrefsList()
{}

PrefsList::type_signal_category_changed& PrefsList::signal_category_changed()
{
  return signal_category_changed_;
}

void PrefsList::on_selection_changed()
{
  if(m_refSelection->count_selected_rows() != 0)
  {
    Gtk::TreeIter iter = m_refSelection->get_selected();
    signal_category_changed_.emit((*iter).get_value(m_model_columns.m_col_id));
  }
}

void PrefsList::icon_data_func(Gtk::CellRenderer* cell,
                               const Gtk::TreeModel::iterator& iter)
{
  CellRendererIconText& renderer = dynamic_cast<CellRendererIconText&>(*cell);

  renderer.property_pixbuf() = (*iter).get_value(m_model_columns.m_col_icon);
}

} // namespace Widgets

} // namespace Coaster
