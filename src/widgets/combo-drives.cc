/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "widgets/combo-drives.h"
#include "ucompose.h"
#include "instance-manager.h"

#include <gtkmm/liststore.h>
#include <gtkmm/cellrenderertext.h>

#include <gconfmm/client.h>

#include <libburn/libburn.h>

namespace Coaster
{

ComboDrives::ComboDrives(bool writers_only,
                         const Glib::ustring& drive_key)
: Gtk::ComboBox(),
  drives(0), n_drives(0), key(drive_key), instmgr(InstanceMgr::instance())
{
  init(writers_only);

  set_active_drive();
}

ComboDrives::ComboDrives(bool writers_only,
                         const Glib::ustring& drive_key,
                         type_slot_speed_pop& slot)
: drives(0), n_drives(0), speed_pop(slot), key(drive_key), instmgr(InstanceMgr::instance())
{
  init(writers_only);

  set_active_drive();
}

ComboDrives::~ComboDrives()
{}

void ComboDrives::init(bool writers_only)
{
  ComboColumns columns;
  m_refListModel = Gtk::ListStore::create(columns);

  set_model(m_refListModel);

  Gtk::CellRendererText *const cell_name = new Gtk::CellRendererText();
  pack_start(*manage(cell_name), true);
  add_attribute(cell_name->property_text(), columns.m_col_name);

  drives = instmgr->get_drives();
  n_drives = instmgr->get_n_drives();

  if(drives)
  {
    for( int i = 0 ; i < instmgr->get_n_drives() ; i++ )
    {
      if(writers_only && (!drives[i].write_cdr || !drives[i].write_cdrw))
        continue;
      Gtk::TreeIter iter = m_refListModel->append();
      (*iter).set_value(columns.m_col_name,
                        String::ucompose("%1 - %2", drives[i].vendor, drives[i].product));
      (*iter).set_value(columns.m_col_drive_info, &drives[i]);
    }
  }
}

bool ComboDrives::is_selected_erasable() const
{
  using namespace burn;
  
  ComboColumns columns;
  Gtk::TreeIter iter = get_active();

  burn_drive_info *drive_info = (*iter).get_value(columns.m_col_drive_info);
  while(!burn_drive_grab(drive_info->drive, 0));
  burn_disc_status s;
  while((s = burn_disc_get_status(drive_info->drive)) == BURN_DISC_UNREADY) usleep(1000);

  bool result = burn_disc_erasable(drive_info->drive) && ( s == BURN_DISC_FULL );
  
  // Do these next steps to make sure we can release the drive
  burn_drive_status status;
  do
  {
    status = burn_drive_get_status(drive_info->drive, NULL);
  } while(status != BURN_DRIVE_IDLE);
  
  burn_drive_release(drive_info->drive, 0);
  
  return (result);
}

bool ComboDrives::is_selected_burnable() const
{
  using namespace burn;
  
  ComboColumns columns;
  Gtk::TreeIter iter = get_active();

  burn_drive_info *drive_info = (*iter).get_value(columns.m_col_drive_info);
  while(!burn_drive_grab(drive_info->drive, 0));
  burn_disc_status s;
  while((s = burn_disc_get_status(drive_info->drive)) == BURN_DISC_UNREADY) usleep(1000);

  bool result = ( s == BURN_DISC_BLANK );

  // Do these next steps to make sure we can release the drive
  burn_drive_status status;
  do
  {
    status = burn_drive_get_status(drive_info->drive, NULL);
  } while(status != BURN_DRIVE_IDLE);

  burn_drive_release(drive_info->drive, 0);

  return result;
}

burn::burn_drive_info* ComboDrives::get_selected_drive_info() const
{
  ComboColumns columns;
  Gtk::TreeIter iter = get_active();
  return (*iter).get_value(columns.m_col_drive_info);
}

void ComboDrives::on_changed()
{
  Gtk::ComboBox::on_changed();

  if(speed_pop)
  {
    if(key == GCONF_OPTICAL_WRITER_DEV)
      speed_pop(get_selected_drive_info()->drive, GCONF_WRITE_SPEED);
    else if(key == GCONF_OPTICAL_READER_DEV)
      speed_pop(get_selected_drive_info()->drive, GCONF_READ_SPEED);
    else
      return;
  }   
}

void ComboDrives::set_active_drive()
{
  struct burn::burn_drive_info* drive_info;
  Glib::ustring name;
  Gtk::TreeIter iter;
  ComboColumns columns;
  SharedPtr<InstanceMgr> instmgr = InstanceMgr::instance();

  Glib::ustring location = instmgr->m_refGConfClient->get_string(key);

  for( iter = m_refListModel->children().begin() ; iter != m_refListModel->children().end() ; iter++)
  {
    drive_info = (*iter).get_value(columns.m_col_drive_info);
    if(g_ascii_strcasecmp(drive_info->location, location.c_str()) == 0)
    {
      set_active(iter);
      break;
    }
  }
}

} // namespace Coaster
