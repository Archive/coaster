/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_LAYOUT_BASE_H_
#define _COASTER_LAYOUT_BASE_H_

#include <gtkmm/treeview.h>
#include <gtkmm/notebook.h>

namespace Coaster
{

namespace Widgets
{

template< class T_Store, class T_Model >
class Layout_Base : public Gtk::TreeView
{
public:
  Layout_Base()
  {
    using namespace Gtk;

    m_selection_ = get_selection();
    m_selection_->set_mode(SELECTION_MULTIPLE);

    set_reorderable();
    set_rules_hint();

    T_Model model;
    m_refStore = T_Store::create(model);
    set_model(m_refStore);

    set_headers_visible(true);
  }

  virtual ~Layout_Base()
  {}

  typedef sigc::signal<void, int> type_signal_selection_changed;
  typedef sigc::signal<void, bool> type_signal_modified;

  type_signal_selection_changed& signal_selection_changed()
  {
    return signal_selection_changed_;
  }

  type_signal_modified& signal_modified()
  {
    return signal_modified_;
  }

  
  typedef std::list<Gtk::TreePath> type_listTP;
  typedef Layout_Base<T_Store,T_Model> type_base;;

  virtual Glib::RefPtr<T_Store>& get_tree()
  {
    return m_refStore;
  }

  virtual bool has_files() const
  {
    return !(m_refStore->children().empty());
  }

  virtual int get_number_selected() const
  {
    return m_selection_->count_selected_rows();
  }

  virtual void on_clear()
  {
    m_refStore->clear();
    signal_modified_.emit(true);
  }

  virtual void on_select_all()
  {
    m_selection_->select_all();
  }

  virtual void on_cut() = 0;
  virtual void on_copy() = 0;
  virtual void on_paste() = 0;
  virtual void on_search_clicked() = 0;
  virtual void on_add_files() = 0;
  virtual void on_rename_selected() = 0;
  virtual void on_remove_selected() = 0;

protected:
  // member methods
  virtual void init_columns() = 0; // override
  virtual void init_drag_n_drop() = 0; //override
  virtual void search(const Glib::ustring& search_string) = 0;

  virtual bool on_search_foreach_case(const Gtk::TreeIter& iter,
                                      const Glib::ustring& search_string) = 0;
  virtual bool on_search_foreach_nocase(const Gtk::TreeIter& iter,
                                        const Glib::ustring& search_string) = 0;
  virtual void on_tree_selection_changed() = 0;
  
  // member variables
  Glib::RefPtr<T_Store> m_refStore;
  Glib::RefPtr<Gtk::TreeSelection> m_selection_;

  Gtk::TreePath m_root_node_;
  Glib::ustring m_directory_visiting_;

  // Signals
  type_signal_modified signal_modified_;
  type_signal_selection_changed signal_selection_changed_;
};

} // namespace Widgets
  
} // namespace Coaster

#endif // _COASTER_LAYOUT_BASE_H_
