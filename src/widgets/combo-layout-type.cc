/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "widgets/combo-layout-type.h"
#include "ucompose.h"
#include "instance-manager.h"

#include <gtkmm/liststore.h>
#include <gtkmm/cellrenderertext.h>
#include <gtkmm/cellrendererpixbuf.h>

#include <libgnomeuimm/stock.h>

namespace Coaster
{

ComboLayoutType::ComboLayoutType()
: Gtk::ComboBox()
{
  ComboColumns columns;
  m_refListModel = Gtk::ListStore::create(columns);

  set_model(m_refListModel);

  clear();

  Gtk::CellRendererPixbuf *const cell_icon = new Gtk::CellRendererPixbuf();
  pack_start(*manage(cell_icon), false);
  add_attribute(cell_icon->property_pixbuf(), columns.m_col_pixbuf);
  
  Gtk::CellRendererText *const cell_name = new Gtk::CellRendererText();
  pack_start(*manage(cell_name), true);
  add_attribute(cell_name->property_text(), columns.m_col_type);

  Gtk::TreeIter iter = m_refListModel->append();
  (*iter).set_value(columns.m_col_pixbuf, 
                    render_icon(Gtk::StockID("gnome-stock-multiple-file"),
                                Gtk::ICON_SIZE_MENU, "coaster-burn-type-cb"));
  (*iter).set_value(columns.m_col_type, Glib::ustring("Data"));
  (*iter).set_value(columns.m_col_layout_type, COASTER_LAYOUT_DATA);

  iter = m_refListModel->append();
  (*iter).set_value(columns.m_col_pixbuf, 
                    render_icon(Gtk::StockID("gnome-stock-volume"),
                                Gtk::ICON_SIZE_MENU, "coaster-burn-type-cb"));
  (*iter).set_value(columns.m_col_type, Glib::ustring("Audio"));
  (*iter).set_value(columns.m_col_layout_type, COASTER_LAYOUT_AUDIO);

  iter = m_refListModel->append();
  (*iter).set_value(columns.m_col_pixbuf, 
                    render_icon(Gtk::StockID("gnome-stock-volume"),
                                Gtk::ICON_SIZE_MENU, "coaster-burn-type-cb"));
  (*iter).set_value(columns.m_col_type, Glib::ustring("Video"));
  (*iter).set_value(columns.m_col_layout_type, COASTER_LAYOUT_VIDEO);
}

ComboLayoutType::~ComboLayoutType()
{}

LayoutType ComboLayoutType::get_selected_layout_type() const
{
  ComboColumns columns;
  Gtk::TreeIter iter = get_active();
  return (*iter).get_value(columns.m_col_layout_type);
}

int ComboLayoutType::get_active_row_number() const
{
  int result = ComboBox::get_active_row_number();

  if(result == 1 || result == 2)
    return -1;
  else
    return result;
}

} // namespace Coaster
