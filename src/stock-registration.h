#ifndef _COASTER_STOCK_REG_H_
#define _COASTER_STOCK_REG_H_

#include <glibmm/ustring.h>

namespace Coaster
{

void register_stock_items();
void set_window_icon(const Glib::ustring& filename);

} // namespace Coaster

#endif // _COASTER_STOCK_REG_H_
