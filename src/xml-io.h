/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_XML_IO_H_
#define _COASTER_XML_IO_H_

#include "cstr-types.h"

#include <gtkmm/treeiter.h>
#include <libgnomevfsmm/types.h>

namespace xmlpp
{
class Document;
class Element;
} // namespace xmlpp

namespace Coaster
{

namespace Data
{
class Store;
} // namespace Data

namespace XmlIO
{

xmlpp::Document* tree_to_document(const Gtk::TreeNodeChildren& group,
                                  bool internal = false,
                                  bool save_ids = false,
                                  RowId pid = 0);

void rows_to_xml(const std::list<Gtk::TreeRow>& rows,
                 xmlpp::Element* parent,
                 bool internal = false,
                 bool save_ids = false);

void tree_branch_to_xml(const Gtk::TreeRow& row,
                        xmlpp::Element* parent,
                        bool internal = false,
                        bool save_ids = false,
                        RowId pid = 0);
void tree_branch_to_xml(const Gtk::TreeNodeChildren &group,
                        xmlpp::Element *parent,
                        bool internal = false,
                        bool save_ids = false,
                        RowId pid = 0);

void tree_branch_from_xml(Gtk::TreeRow& row,
                          const xmlpp::Element* parent,
                          Glib::RefPtr<Data::Store>& refStore,
                          const sigc::slot<bool,const Gnome::Vfs::FileSize&>& add_slot,
                          bool save_ids = false);
void tree_branch_from_xml(Gtk::TreeNodeChildren& children,
                          const xmlpp::Element *parent,
                          Glib::RefPtr<Data::Store>& refStore,
                          const sigc::slot<bool,const Gnome::Vfs::FileSize&>& add_slot,
                          bool save_ids = false);
void tree_branch_from_xml(std::list<Gtk::TreeRow>& rows,
                          Gtk::TreeRow& row,
                          const xmlpp::Element* parent,
                          Glib::RefPtr<Data::Store>& refStore,
                          const sigc::slot<bool,const Gnome::Vfs::FileSize&>& add_slot);
void tree_branch_from_xml(std::list<Gtk::TreeRow>& rows,
                          Gtk::TreeNodeChildren& children,
                          const xmlpp::Element *parent,
                          Glib::RefPtr<Data::Store>& refStore,
                          const sigc::slot<bool,const Gnome::Vfs::FileSize&>& add_slot);

void add_back_from_xml(const xmlpp::Element* parent,
                       Glib::RefPtr<Data::Store>& refStore,
                       const sigc::slot<bool,const Gnome::Vfs::FileSize&>& add_slot);
void add_back_from_xml(const xmlpp::Element* parent,
                       Gtk::TreeNodeChildren& children,
                       Glib::RefPtr<Data::Store>& refStore,
                       const sigc::slot<bool,const Gnome::Vfs::FileSize&>& add_slot);

std::list<Gtk::TreeRow> remove_from_xml(const xmlpp::Element* parent,
                                        Glib::RefPtr<Data::Store>& refStore);

void rename_from_xml(const xmlpp::Element* node,
                     Glib::RefPtr<Data::Store>& refStore,
                     bool redo);

} // namespace XmlIO

} // namespace Coaster

#endif // _COASTER_XML_IO_H_
