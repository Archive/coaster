#ifndef _COASTER_CD_IO_COMMON_H_
#define _COASTER_CD_IO_COMMON_H_

#include <libburn/libisofs.h>
#include <glibmm/refptr.h>

#include <deque>
#include "data/data-prop.h"

namespace Gtk
{
class TreeStore;
} // namespace Gtk;

namespace Coaster
{

iso::iso_volumeset* parse_stores_to_volset(const std::deque<Glib::RefPtr<Gtk::TreeStore> >& store_array, const Coaster::Data::Properties& dp);
  
}

#endif // _COASTER_CD_IO_COMMON_H_
