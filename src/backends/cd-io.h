/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_CD_IO_H_
#define _COASTER_CD_IO_H_

#include "data/data-prop.h"

#include "sharedptr.h"
#include "baconmm/cd-recorder-cpp.h"

#include <gtkmm/window.h>
#include <cd-drive.h>

#include <deque>

namespace xmlpp
{
class Document;
} // namespace xmlpp

namespace Coaster
{

class InstanceMgr;

namespace Dialogs
{
class BurnProgress;
} // namespace Dialogs

namespace IO
{

class MakeIso;

class BurnDisc : public sigc::trackable
{
public:
  BurnDisc(const Gtk::Window& parent_win,
           const CDDrive* rec,
           bool eject_cd,
           int speed,
           bool dummy,
           const xmlpp::Document* CDStructure,
           const Data::Properties& dp);
  BurnDisc(const Gtk::Window& parent_win,
           const CDDrive* rec,
           bool eject_cd,
           int speed,
           bool dummy,
           const std::string& filename);
  ~BurnDisc();

  enum CancelState
  {
    CANCEL_NONE,
	  CANCEL_MAKE_ISO,
	  CANCEL_CD_RECORD
  };

  bool start_burn();

protected:
  void on_cancel_clicked();
  void on_progress_changed(double fraction);
  void on_text_changed(const Glib::ustring& text);
  void on_action_changed(Bacon::CD_RecorderActions action,
                         Bacon::CD_RecorderMedia media);

private:
  std::auto_ptr<Dialogs::BurnProgress> m_pBurnProgress;
  SharedPtr<InstanceMgr> instmgr;
  SharedPtr<MakeIso> mkiso;
  std::string m_filename;

  Glib::RefPtr<Bacon::CD_Recorder> m_refCD_Recorder;

  Gtk::Window* m_parent_win;
  CDDrive*     m_pCDDrive;
  bool         m_bEject_CD;
  int          m_dSpeed;
  bool         m_bDummy;

  xmlpp::Document* m_pDocument;
  Data::Properties m_DataProperties;

  CancelState cancel;
};
       
} // namespace IO

} // namespace Coaster

#endif // _COASTER_CD_IO_H_
