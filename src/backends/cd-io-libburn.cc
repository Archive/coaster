/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "cd-io.h"
#include "util.h"
#include "ucompose.h"
#include "cd-io-common.h"

#include <gtkmm/treestore.h>

#include <libburn/libburn.h>
#include <libburn/libisofs.h>

namespace
{

bool prepare_drive(burn::burn_drive_info* drive_info, burn::burn_disc_status status)
{
  using namespace burn;

  while(!burn_drive_grab(drive_info->drive, 0));

  while(burn_drive_get_status(drive_info->drive, NULL)) Glib::usleep(100000);

  burn_disc_status s;
  while((s = burn_disc_get_status(drive_info->drive)) == BURN_DISC_UNREADY) Glib::usleep(100000);

  Coaster::Util::debug(String::ucompose("IO Status: %1", s));
  if(s != status)
  {
    burn_drive_release(drive_info->drive, 0);
    //Gtk::MessageDialog
    return false;
  }
  
  return true;
}
  
} // anonymous namespace

namespace Coaster
{
  
namespace IO
{

void erasedisc(burn::burn_drive_info* drive_info, bool fast, const sigc::slot<void>& slot_pulse)
{
  using namespace burn;

  if(!prepare_drive(drive_info, BURN_DISC_FULL))
    return;

  Util::debug("IO Erase: Blanking disc...");
  burn_disc_erase(drive_info->drive, fast);

  burn_progress p;
  const Glib::RefPtr<Glib::MainContext> context = Glib::MainContext::get_default();
  while (burn_drive_get_status(drive_info->drive, &p))
  {
    Util::debug(String::ucompose("IO Erase: %1", p.sector));
    
    do {} while(context->iteration(false));
    slot_pulse();
    Glib::usleep(100000);
  }

  burn_drive_release(drive_info->drive, 0);
}

bool burndisc(burn::burn_drive_info* drive_info, int speed, const sigc::slot<void,double,double>& slot_progress, std::deque<Glib::RefPtr<Gtk::TreeStore> >& store_array, const DataProperties& dp)
{
  burn::burn_disc* disc = burn::burn_disc_create();
  burn::burn_session* session = burn::burn_session_create();
  
  burn::burn_disc_add_session(disc, session, BURN_POS_END);

  burn::burn_track* tr = burn::burn_track_create();
  burn::burn_track_define_data(tr, 0, 0, 0, BURN_MODE1);

  iso::iso_volumeset* volset = parse_stores_to_volset(store_array, dp);

  burn::burn_source* src = (burn::burn_source*)iso::iso_source_new(volset, 0);

  if(burn::burn_track_set_source(tr, src) != burn::BURN_SOURCE_OK)
  {
    Util::debug("Burn: IO: burn source not OK");
    return false;
  }

  burn::burn_session_add_track(session, tr, BURN_POS_END);

  if(!prepare_drive(drive_info, burn::BURN_DISC_BLANK))
  {
    Util::debug("Burn: IO: disc not blank");
    return false;
  }

  burn::burn_write_opts* o = burn::burn_write_opts_new(drive_info->drive);
  burn::burn_write_opts_set_perform_opc(o, 0);
  burn::burn_write_opts_set_write_type(o, burn::BURN_WRITE_TAO, burn::BURN_BLOCK_MODE1);
  burn::burn_write_opts_set_simulate(o, 1);
  burn::burn_drive_set_speed(drive_info->drive, 0, speed);

  burn::burn_disc_write(o, disc);

  burn::burn_progress p;
  const Glib::RefPtr<Glib::MainContext> context = Glib::MainContext::get_default();
  while (burn::burn_drive_get_status(drive_info->drive, &p))
  {
    Util::debug(String::ucompose("IO Burn: %1", p.sector));
    
    do {} while(context->iteration(false));
    slot_progress(p.track/p.tracks,p.sector/p.sectors);
    Glib::usleep(100000);
  }

  burn::burn_drive_release(drive_info->drive, 0);
  burn::burn_write_opts_free(o);
  
  burn::burn_track_free(tr);
  burn::burn_source_free(src);
  burn::burn_session_free(session);
  burn::burn_disc_free(disc);
  iso::iso_volumeset_free(volset);
  
  return true;
}

} // namespace IO

} // namespace Coaster
