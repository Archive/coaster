#include "cd-io-common.h"

#include "util.h"
#include "cstr-enums.h"

#include <gtkmm/treestore.h>
#include <iostream>

namespace
{

void tree_branch_to_iso(const Gtk::TreeModel::iterator& group, iso::iso_tree_dir** parent)
{
  using namespace Gtk;

  const Coaster::Data::Columns& model_columns = Coaster::Data::columns();

  if(!group->children().empty())
  {
    for(TreeModel::iterator iter = group->children().begin() ; iter != group->children().end() ; ++iter)
    {
      Glib::ustring name = (*iter).get_value(model_columns.m_col_name);
      if( (*iter).get_value(model_columns.m_col_entity_type) != Coaster::COASTER_ENTITY_DIRECTORY )
      {
        Glib::ustring uri = (*iter).get_value(model_columns.m_col_uri);
        
        iso::iso_tree_file** file = iso::iso_tree_add_file(parent, uri.c_str(), 0, NULL);
        iso::iso_tree_file_set_name(file, parent, name.c_str());
      }
      else
      {
        iso::iso_tree_dir** dir = iso::iso_tree_add_dir(parent, name.c_str(), 0, NULL);
        tree_branch_to_iso(iter, dir);
      }
    }
  }
}
  
void tree_to_iso(const Glib::RefPtr<Gtk::TreeStore>& refTreeStore, iso::iso_tree_dir** root)
{
  using namespace Gtk;

  const Coaster::Data::Columns& model_columns = Coaster::Data::columns();

  if(!(refTreeStore->children().empty()))
  {
    for(TreeModel::iterator iter = refTreeStore->children().begin() ; iter != refTreeStore->children().end() ; ++iter)
    {
      Glib::ustring name = (*iter).get_value(model_columns.m_col_name);
      if( (*iter).get_value(model_columns.m_col_entity_type) != Coaster::COASTER_ENTITY_DIRECTORY )
      {
        Glib::ustring uri = (*iter).get_value(model_columns.m_col_uri);
        
        iso::iso_tree_file** file = iso::iso_tree_add_file(root, uri.c_str(), 0, NULL);
        iso::iso_tree_file_set_name(file, root, name.c_str());
      }
      else
      {
        iso::iso_tree_dir** dir = iso::iso_tree_add_dir(root, name.c_str(), 0, NULL);
        tree_branch_to_iso(iter, dir);
      }
    }
  }
}

} // anonymous namespace

namespace Coaster
{

iso::iso_volumeset* parse_stores_to_volset(const std::deque<Glib::RefPtr<Gtk::TreeStore> >& store_array, const Coaster::Data::Properties& dp)
{
  std::cout << "Title: " << dp.title << std::endl;
  std::cout << "CatNum: " << dp.catnum << std::endl;
  std::cout << "Publisher: " << dp.publisher << std::endl;
  std::cout << "Author: " << dp.author << std::endl;
  std::cout << "IsoLevel: " << dp.isolevel << std::endl;
  std::cout << "Joliet: " << dp.joliet << std::endl;
  std::cout << "RR: " << dp.rockridge << std::endl;
  std::cout << "Session Format: " << dp.sessionformat << std::endl;

  Glib::RefPtr<Gtk::TreeStore> ts = store_array[0];
  gchar* volume_names[] = { "CDROM" };
  
  iso::iso_volumeset* volset = iso::iso_volumeset_new(1, dp.title.c_str(), volume_names, dp.publisher.c_str(), dp.publisher.c_str(), NULL, NULL);
 
  iso::iso_volumeset_set_iso_level(volset, (int)(dp.isolevel));
  iso::iso_volumeset_set_rr(volset, (int)(dp.rockridge));
  iso::iso_volumeset_set_joliet(volset, (int)(dp.joliet));

  iso::iso_tree_dir **root = iso::iso_volumeset_get_root(volset);

  tree_to_iso(ts, root);
  iso::iso_tree_print(root);

  return volset;
}
  
} // namespace Coaster
