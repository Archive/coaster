/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_MAINWINDOW_H_
#define _COASTER_MAINWINDOW_H_

#include <gtkmm/box.h>

#include <bakery/App/App_WithDoc_Gtk.h>

#include <libgnomevfsmm.h>

#include "cstr-enums.h"
#include "sharedptr.h"

namespace Gtk
{
class MessageDialog;
class Statusbar;
} // namespace Gtk

namespace Coaster
{

class Document;
class InstanceMgr;

namespace Widgets
{
class View_Base;
} // namespace Widgets

namespace Dialogs
{
class About;
} // namespace Dialogs

class MainWindow : public Bakery::App_WithDoc_Gtk
{
public:
  MainWindow(BaseObjectType* cobject,
             const Glib::RefPtr<Gnome::Glade::Xml>& refGlade);
  virtual ~MainWindow();

  bool init(const Glib::ustring& document_uri = Glib::ustring());
  void init_about_information();

  static void set_about_icon(const Glib::ustring& icon);

protected:
  Bakery::App* new_instance();

  typedef Bakery::App_WithDoc_Gtk type_base;
  
  void do_nothing();
  int add_ui_from_file(const Glib::ustring& ui_filename);
  Glib::ustring ui_file_select_open();
  bool open_document(const Glib::ustring& file_uri);

  // initialize for opening
  void pre_init_view_actions(MainWindow* pApp);

  void init_create_document();
  void init_create_document(LayoutType type);
  void init_ui_manager();
  void init_menus_file();
  void init_menus_edit();
  void init_menus_other();
  void init_menus_help();
  void init_view_actions();
  void init_unfinished_actions();
  void init_menus();
  void init_toolbars();
  void init_layout();

  enumSaveChanges ui_offer_to_save_changes();
 
  // Menu Handlers
  void on_menu_file_exit();
  void on_menu_file_new();

  void on_menu_help_contents();
  void on_help_error_close(int response);
  void on_menu_help_about();

  // signal handlers
  void on_ui_connect(const Glib::RefPtr<Gtk::Action>& action,
                     Gtk::Widget* widget);
  void on_ui_disconnect(const Glib::RefPtr<Gtk::Action>& action,
                        Gtk::Widget* widget);
  void on_show_menu_tip(const Glib::RefPtr<Gtk::Action>& action);
  void on_clear_menu_tip();
  void on_preferences_clicked();

  void on_burn_iso_clicked();
  void on_copy_cd_clicked();
  void on_erase_clicked();

  void on_instmgr_sensitive(bool sensitive);

private:
  // the InstanceMgr
  SharedPtr<InstanceMgr> instmgr;

  // other classes that only this interface uses
  std::string last_directory_;
 
  // Container Widgets
  Gtk::VBox m_Contents;
  Gtk::VBox* m_pMainVBox;

  // Content Widgets
  Coaster::Widgets::View_Base* m_pView;
  Gtk::Statusbar* m_pStatus;
  
  // new Action API
  Glib::RefPtr<Gtk::ActionGroup> m_refOtherActionGroup;
  int last_view_merge_ui;

  // Actions to (de)sensitize
  Glib::RefPtr<Gtk::Action> m_refBurnAction;
  Glib::RefPtr<Gtk::Action> m_refEraseAction;
  Glib::RefPtr<Gtk::Action> m_refEjectAction;

  // View actions
  Glib::RefPtr<Gtk::Action> m_refSelectAllAction;
  Glib::RefPtr<Gtk::Action> m_refClearAction;

  // Help Error dialog:
  static Gtk::MessageDialog* m_pErrorHelp;
  static bool m_bErrorHelp_Shown;

  // About dialog:
  static Dialogs::About* m_pAbout;
  static Glib::ustring   m_about_icon;
};

} // namespace Coaster

#endif // _COASTER_MAINWINDOW_H_
