/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_ENUMS_H_
#define _COASTER_ENUMS_H_

#include <libgnomevfsmm/enums.h>

namespace Coaster
{

typedef enum
{
  COASTER_LAYOUT_AUDIO,
  COASTER_LAYOUT_DATA,
  COASTER_LAYOUT_MIX,
  COASTER_LAYOUT_VIDEO
} LayoutType;

typedef enum
{
  COASTER_ENTITY_FILE,
  COASTER_ENTITY_AUDIO,
  COASTER_ENTITY_DIRECTORY,
} EntityType;

typedef enum
{
  COASTER_CD_SIZE_NONE = 0,
  COASTER_CD_SIZE_650 = 1,
  COASTER_CD_SIZE_700 = 2
} CDSizeType;
  
typedef enum
{
  COASTER_ISO_LEVEL1 = 1,
  COASTER_ISO_LEVEL2 = 2
} IsoLevel;

typedef enum
{
  COASTER_SESSION_FMT_CDROM,
  COASTER_SESSION_FMT_CDI,
  COASTER_SESSION_FMT_CDXA
} SessionFormat;

const Gnome::Vfs::FileInfoOptions COMMON_INFO_OPTIONS =
  Gnome::Vfs::FILE_INFO_DEFAULT |
  Gnome::Vfs::FILE_INFO_GET_MIME_TYPE |
  Gnome::Vfs::FILE_INFO_FOLLOW_LINKS;

} // namespace Coaster

#endif // _COASTER_ENUMS_H_
