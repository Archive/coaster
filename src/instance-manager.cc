/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "instance-manager.h"

#include "cstr-debug.h"
#include "ucompose.h"

#include "widgets/cd-tray-icon.h"
#include "data/data-columns.h"

#include <libgnomevfs/gnome-vfs-volume-monitor.h>
#include <libgnomevfsmm.h>

#include <gconfmm/client.h>

#include <gtkmm/layout.h>
#include <gtkmm/window.h>
#include <gtkmm/treestore.h>
#include <gtkmm/combobox.h>
#include <gtkmm/icontheme.h>

#include <libgnomeui/gnome-icon-lookup.h>

#include <iostream>

namespace Coaster
{

const Glib::ustring GCONF_READ_SPEED = "/desktop/gnome/burn_prefs/read_speed";
const Glib::ustring GCONF_WRITE_SPEED = "/desktop/gnome/burn_prefs/write_speed";
const Glib::ustring GCONF_OPTICAL_READER_DEV = "/desktop/gnome/burn_prefs/optical_reader_dev";
const Glib::ustring GCONF_OPTICAL_WRITER_DEV = "/desktop/gnome/burn_prefs/optical_writer_dev";
const Glib::ustring GCONF_EJECT_ON_FINISH = "/desktop/gnome/burn_prefs/eject_on_finish";

InstanceMgr::InstanceMgr()
: m_status_icon(0)
{}

InstanceMgr::~InstanceMgr()
{
  if(m_status_icon)
  {
    delete m_status_icon;
    m_status_icon = 0;
  }
}

SharedPtr<InstanceMgr> InstanceMgr::instance()
{
  static SharedPtr<InstanceMgr> instmgr(new InstanceMgr());
  return instmgr;
}

void InstanceMgr::init()
{
  if(!m_refIconTheme)
    m_refIconTheme = Gtk::IconTheme::get_default();
  if(!m_refClipboard)
    m_refClipboard = Gtk::Clipboard::get();
  if(!m_refVolumeMonitor)
    m_refVolumeMonitor = Gnome::Vfs::VolumeMonitor::create();
  if(m_refVolumeMonitor)
    m_refVolumeMonitor->reference();

  m_refIconTheme->signal_changed().connect(sigc::mem_fun(*this,
                                                         &InstanceMgr::on_icon_theme_changed));
}

void InstanceMgr::set_sensitive(bool sensitive)
{
  m_signal_sensitive_.emit(sensitive);
}

InstanceMgr::type_signal_sensitive& InstanceMgr::signal_sensitive()
{
  return m_signal_sensitive_;
}

InstanceMgr::type_signal_icon_theme_changed& InstanceMgr::signal_icon_theme_changed()
{
  return m_signal_icon_theme_changed_;
}

bool InstanceMgr::lookup_icon(const Glib::ustring& mime_type,
                              const Gtk::TreeRowReference& rowref,
                              const Glib::RefPtr<Gtk::TreeStore>& treestore)
{
  debug("TreePath instmgr: ",rowref.get_path().to_string());
  
  Gtk::TreeRow row = *(treestore->get_iter(rowref.get_path()));

  const Data::Columns& modelColumns = Data::columns();

  row[modelColumns.m_col_ref_pixbuf] = gtk_lookup_icon(mime_type);

  return false;
}

Glib::RefPtr<Gdk::Pixbuf> InstanceMgr::lookup_icon(const Glib::ustring& mime_type)
{
  return gtk_lookup_icon(mime_type);
}

void InstanceMgr::on_icon_theme_changed()
{
  debug("instmgr map reset");

  for(type_str_pix_hash::iterator i = m_hash_StrPixbuf.begin() ; i != m_hash_StrPixbuf.end() ; ++i)
  {
    i->second = gtk_lookup_icon(i->first, false);
  }

  m_signal_icon_theme_changed_.emit();
}

void InstanceMgr::create_status_icon(const Glib::RefPtr<Gdk::Pixbuf>& pixbuf)
{
  if(!m_status_icon)
  {
    m_status_icon = new Widgets::CDTrayIcon(pixbuf);
  }
}

void InstanceMgr::set_status_pixbuf(const Glib::RefPtr<Gdk::Pixbuf>& pixbuf)
{
  if(m_status_icon)
    m_status_icon->set_pixbuf(pixbuf);
  else
    create_status_icon(pixbuf);
}

void InstanceMgr::set_status_tip(const Glib::ustring& tip,
                                 const Glib::ustring& private_tip)
{
  if(m_status_icon)
    m_status_icon->set_tip(tip, private_tip);
  else
    g_warning("No status icon!");
}

void InstanceMgr::remove_status_icon()
{
  delete m_status_icon;
  m_status_icon = 0;
}

Glib::RefPtr<Gdk::Pixbuf> InstanceMgr::gtk_lookup_icon(const Glib::ustring& mime_type,
                                                       bool check_map)
{
  type_str_pix_hash::iterator pos =  m_hash_StrPixbuf.find(mime_type);
 
  if(!check_map || m_hash_StrPixbuf.empty() || pos  == m_hash_StrPixbuf.end())
  {
    Gtk::IconInfo icon_info;
    int width, height;
 
    Gtk::IconSize::lookup(Gtk::ICON_SIZE_MENU, width, height);
    Glib::ustring tempIconName =
      Glib::convert_return_gchar_ptr_to_ustring(gnome_icon_lookup(m_refIconTheme->gobj(),
                                                                  0, 0, 0, 0,
                                                                  mime_type.c_str(),
                                                                  GNOME_ICON_LOOKUP_FLAGS_NONE, 0));
    
    Glib::RefPtr<Gdk::Pixbuf> result = m_refIconTheme->load_icon(tempIconName,
                                                                 width,
                                                                 Gtk::ICON_LOOKUP_USE_BUILTIN);

    m_hash_StrPixbuf.insert(std::make_pair(mime_type,result));
    debug("Icon Cache: ","inserted " + mime_type + " successfully");
    return result;
  }
  else
  {
    debug("Icon Cache: ",mime_type," found already");
    return pos->second;
  }
}

} // namespace Coaster
