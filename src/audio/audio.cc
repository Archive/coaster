#include "audio.h"
#include "cstr-common.h"
#include "media-file.h"

#include <gtkmm.h>
#include <libgnomevfsmm/handle.h>
#include <libgnomevfsmm/directory-handle.h>

#include <bakery/Utilities/BusyCursor.h>

#include <sstream>
#include <vector>

namespace // Anonymous namespace
{

Coaster::MediaType get_mime_type(const Glib::RefPtr<const Gnome::Vfs::FileInfo>& info)
{
  using namespace Gnome::Vfs;
  using namespace Coaster;
  MediaType result = COASTER_MEDIA_NOT_ADDABLE;

  Glib::ustring mime_type = info->get_mime_type();

  if(mime_type == N_("application/ogg") || mime_type == N_("application/x-ogg"))
    result = COASTER_MEDIA_OGG;
  else if(mime_type == N_("audio/mpeg") || mime_type == N_("audio/x-mp3"))
    result = COASTER_MEDIA_MP3;
  else if(mime_type == N_("application/x-flac"))
    result = COASTER_MEDIA_FLAC;
  else if(mime_type == N_("application/x-wav"))
    result = COASTER_MEDIA_WAV;

  return result;
}

} // Anonymous namespace

namespace Coaster
{

AudioView::AudioView(BaseObjectType* cobject, const Glib::RefPtr<Gnome::Glade::Xml>& refXml)
  : Gtk::TreeView(cobject) //Glib::ObjectBase(typeid(AudioView))
{
  init();

  show_all_children();
}

AudioView::~AudioView()
{
}

//////////////////////////////////////////////////////////////////////////////////////////
//                                Member Methods                                        //
//////////////////////////////////////////////////////////////////////////////////////////

void AudioView::init()
{
  using namespace Gtk;

  m_selection_ = get_selection();
  m_selection_->set_mode(SELECTION_MULTIPLE);

  refLayTreeModel = ListStore::create(m_modelColumns);
  set_model(refLayTreeModel);
  set_reorderable();
  set_rules_hint();
  set_headers_visible(true);

  append_column_editable(_("Name"), m_modelColumns.m_col_name);
  append_column(_("Location"), m_modelColumns.m_col_uri);
  append_column(_("Duration"), m_modelColumns.m_col_duration);

  get_column(0)->set_resizable(true);
  get_column(1)->set_resizable(true);

  // This connects the changed() signal to our signal handler/caller
  // This is a cool little ditty :).
  signal_file_added.connect(sigc::mem_fun(*this, &AudioView::on_file_added));
  signal_file_removed.connect(sigc::mem_fun(*this, &AudioView::on_file_removed));

  init_popup();
}

void AudioView::init_popup()
{
  using namespace Gtk::Menu_Helpers;

  MenuList& menulist = m_Menu_Popup.items();

  menulist.push_back(StockMenuElem(Gtk::StockID(Gtk::Stock::ADD),
      sigc::mem_fun(*this, &AudioView::on_add_clicked) ) );
  menulist.push_back(StockMenuElem(Gtk::StockID(Gtk::Stock::REMOVE),
      sigc::mem_fun(*this, &AudioView::on_remove_clicked) ) );
  menulist.push_back(SeparatorElem());
  menulist.push_back(StockMenuElem(Gtk::StockID(Gtk::Stock::GO_UP)));
  menulist.push_back(StockMenuElem(Gtk::StockID(Gtk::Stock::GO_DOWN)));

  m_Menu_Popup.accelerate(*this);
}

void AudioView::add_files(const type_listUStrings& files)
{
  using namespace Gnome::Vfs;
  Bakery::BusyCursor cursor(static_cast<Gtk::Window&>(*this->get_toplevel()));

  type_listTP list_paths = m_selection_->get_selected_rows();

  Gtk::TreePath path;
  Gtk::TreeIter tree_iter;

  if(list_paths.size() > 0) // if there are rows selected...
  {
    path = list_paths.back(); // get the last path on the list
    tree_iter = refLayTreeModel->get_iter(path); // get the iterator for the last element
  }

  // loop over the list of files
  for(type_listUStrings::const_iterator iter = files.begin() ; iter != files.end() ; ++iter)
  {
    MediaFile media_file(*iter);

    if(media_file.is_valid())
    {
      Gtk::TreeRow row;

      // if we have 1 or more rows selected
      if(list_paths.size() > 0)
      {
        tree_iter++; // go to the next row because it inserts before
        row = *(refLayTreeModel->insert(tree_iter)); // create a new row where we're inserting
        tree_iter = row; // assign this row to the iterator so we can insert at this point
      }
      else // if we have 0 rows selected
      {
        row = *(refLayTreeModel->append()); // create a new row at the end of the list
      }

      // insert data into new row
      row[m_modelColumns.m_col_name] = media_file.get_title();
      row[m_modelColumns.m_col_duration] = media_file.get_duration_string();
      row[m_modelColumns.m_col_duration_secs] = media_file.get_duration_secs();
      row[m_modelColumns.m_col_uri] = *iter;

      signal_cd_time_add(media_file.get_duration_secs());
    }
    else // print an error
    {
      Util::debug("Invalid mime-type: " + media_file.get_info_name());
    }
  }
}

//////////////////////////////////////////////////////////////////////////////////////////
//                                  Signal Handlers                                     //
//////////////////////////////////////////////////////////////////////////////////////////

void AudioView::on_add_clicked()
{
  signal_add_clicked();
}

void AudioView::on_remove_clicked()
{
  signal_file_removed();
}

void AudioView::on_file_added(const type_listUStrings& files)
{
  using namespace Gnome::Vfs;

  DirectoryHandle dhandle;
  type_listUStrings::const_iterator iter = files.begin();

  try
  {
    add_files(files);
  }
  catch(...)
  {
    try
    {
      type_listUStrings child_files;
      directory_visiting_ = *iter;
     
      DirectoryHandle::visit(directory_visiting_, COMMON_INFO_OPTIONS, DIRECTORY_VISIT_LOOPCHECK,
          sigc::bind<type_listUStrings*>(
            sigc::mem_fun(*this, &AudioView::on_visit_directory), &child_files));

      directory_visiting_ = Glib::ustring();

      if( child_files.size() > 0 ) // there were valid media files
      {
        add_files(child_files);
      }
      else // the directory was empty/didn't have valid media files
      {
        Gtk::MessageDialog dialog(
            _("There are no valid media files in the directory you selected!"), 
            Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        dialog.set_transient_for((Gtk::Window&)(*this->get_toplevel()));
        dialog.run();
      }
    }
    catch(...)
    {
      Util::debug("Something is terribly wrong :(");
    }
  }
}

void AudioView::on_file_removed()
{
  using namespace Gtk;
  
  type_listTP list_paths = m_selection_->get_selected_rows();

  if(list_paths.size() > 0)
  {
    // Need to read in the selected rows each time thru because the paths change after
    // you erase a row.
    for(type_listTP::iterator iter = list_paths.begin() ; iter != list_paths.end() ; list_paths = m_selection_->get_selected_rows(), iter = list_paths.begin())
    {
      TreeRow row = *(refLayTreeModel->get_iter(*iter));
      signal_cd_time_remove(row[m_modelColumns.m_col_duration_secs]); // remove the time from the progress bar
      refLayTreeModel->erase(row);
    }
  }
  else
  {
    MessageDialog dialog(_("Nothing is selected!"), MESSAGE_ERROR, BUTTONS_OK, true);
    dialog.set_transient_for((Window&)(*this->get_toplevel()));
    dialog.run();
  }
}

bool AudioView::on_visit_directory(const Glib::ustring& rel_path, const Glib::RefPtr<const Gnome::Vfs::FileInfo>& info, bool recursing_will_loop, bool& recurse, type_listUStrings* files, Glib::ustring directory_visiting)
{
  if( get_mime_type(info) != COASTER_MEDIA_NOT_ADDABLE )
  {
    std::ostringstream output;

    output.imbue(std::locale(""));
    output << directory_visiting << "/" << rel_path;

    files->push_back(Glib::locale_to_utf8(output.str()));
  }
  return true;
}

bool AudioView::on_button_press_event(GdkEventButton* event)
{
  using namespace Gtk;

  bool result = false;
  
  if( (event->type == GDK_BUTTON_PRESS) && (event->button == 3) )
  {
    m_Menu_Popup.popup(event->button, event->time);
    result = true; 
  }
  else
  {
    result = Gtk::TreeView::on_button_press_event(event);
  }

  return result;
}

} // namespace Coaster
