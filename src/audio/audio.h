#ifndef _COASTER_AUDIO_LAYOUT_H_
#define _COASTER_AUDIO_LAYOUT_H_

#include <gtkmm/box.h>
#include <gtkmm/treeselection.h>
#include <gtkmm/treepath.h>
#include <glibmm.h>
#include <libglademm.h>
#include <libgnomevfsmm/file-info.h>

#include "cstr-common.h"
#include "document.h"

namespace Coaster
{

class AudioView : public Gtk::TreeView, 
                       public Bakery::View<Document>
{
  public:
    AudioView(BaseObjectType* cobject, const Glib::RefPtr<Gnome::Glade::Xml>& refXml);
    ~AudioView();

    typedef std::list<Gtk::TreePath> type_listTP;

    // overrides
//    virtual void load_from_document();
//    virtual void save_to_document();

  protected:
    // member methods
    void init();
    void init_popup();
    void add_files(const type_listUStrings& files);

    // signal handlers
    void on_add_clicked();
    void on_remove_clicked();
    void on_file_added(const type_listUStrings& files);
    void on_file_removed();
    bool on_visit_directory(const Glib::ustring& rel_path, const Glib::RefPtr<const Gnome::Vfs::FileInfo>& info, bool recursing_will_loop, bool& recurse, type_listUStrings* files);
    virtual bool on_button_press_event(GdkEventButton* event);

  private:
    // other classes that only this interface uses
    AudioColumns m_modelColumns;
    Glib::RefPtr<Gtk::ListStore> refLayTreeModel;
    Glib::RefPtr<Gtk::TreeSelection> m_selection_;
    Gtk::Menu m_Menu_Popup;
    Glib::ustring directory_visiting_;
    Glib::RefPtr<Gnome::Glade::Xml> m_Xml;
};

} // namespace Coaster

#endif // _COASTER_AUDIO_LAYOUT_H_
