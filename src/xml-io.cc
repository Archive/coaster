/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "xml-io.h"

#include "ucompose.h"

#include "data/data-utils.h"
#include "data/data-store.h"
#include "data/data-file.h"

#include <libxml++/document.h>
#include <libxml++/nodes/element.h>

#include <sstream>

namespace
{

bool find_row_with_pid(const Gtk::TreeIter& iter,
                       Coaster::RowId pid,
                       Gtk::TreeRow* new_row,
                       Glib::RefPtr<Coaster::Data::Store>& store)
{
  using namespace Coaster;
  const Data::Columns& columns = Data::columns();
  if((*iter).get_value(columns.m_col_id) == pid)
  {
    *new_row = *(store->append((*iter).children()));
    return true;
  }
  else
    return false;
}

bool find_row_with_id(const Gtk::TreeIter& iter,
                      Coaster::RowId id,
                      Gtk::TreeRow* row)
{
  using namespace Coaster;
  const Data::Columns& columns = Data::columns();
  if((*iter).get_value(columns.m_col_id) == id)
  {
    *row = (*iter);
    return true;
  }
  else
    return false;
}

template<typename T>
Glib::ustring to_string(const T& x)
{
  std::ostringstream oss;
  oss << x;
  return oss.str();
}

Coaster::RowId to_rowid(const Glib::ustring& string)
{
  if(string == "")
    return 0;

  std::istringstream iss(string);
  Coaster::RowId result;
  iss >> result;
  return result;
}

} // anonymous namespace

namespace Coaster
{

namespace XmlIO
{

xmlpp::Document* tree_to_document(const Gtk::TreeNodeChildren& group,
                                  bool internal,
                                  bool save_ids,
                                  RowId pid)
{
  xmlpp::Document* document = new xmlpp::Document();

  xmlpp::Element* rootNode = document->create_root_node("coaster-rows");

  tree_branch_to_xml(group, rootNode, internal, save_ids, pid);

  return document;
}

void rows_to_xml(const std::list<Gtk::TreeRow>& rows,
                 xmlpp::Element* parent,
                 bool internal,
                 bool save_ids)
{
  for(std::list<Gtk::TreeRow>::const_iterator i = rows.begin() ; i != rows.end() ; ++i)
  {
    if((*i).parent())
      tree_branch_to_xml(*i, parent, internal, save_ids, (*(*i).parent()).get_value(Data::columns().m_col_id));
    else
      tree_branch_to_xml(*i, parent, internal, save_ids);
  }
}

void tree_branch_to_xml(const Gtk::TreeRow& row,
                        xmlpp::Element* parent,
                        bool internal,
                        bool save_ids,
                        RowId pid)
{
  const Data::Columns& model_columns = Data::columns();
  
  if(!Data::is_tree_dir(row))
  {
    xmlpp::Element* new_elem = parent->add_child("data");
    new_elem->set_attribute("name", row.get_value(model_columns.m_col_name));
    new_elem->set_attribute("path", row.get_value(model_columns.m_col_uri));
    if(internal)
    {
      new_elem->set_attribute("size", to_string(row.get_value(model_columns.m_col_size)));
      new_elem->set_attribute("mime", row.get_value(model_columns.m_col_mime_type));

      if(save_ids)
      {
        new_elem->set_attribute("id", to_string(row.get_value(model_columns.m_col_id)));
        new_elem->set_attribute("pid", to_string(pid));
      }
    }
  }
  else
  {
    xmlpp::Element* new_elem = parent->add_child("directory");
    new_elem->set_attribute("name", row.get_value(model_columns.m_col_name));
    if(save_ids)
    {
      RowId row_id = row.get_value(model_columns.m_col_id);

      new_elem->set_attribute("id", to_string(row_id));
      new_elem->set_attribute("pid", to_string(pid));

      tree_branch_to_xml(row.children(), new_elem, internal, save_ids, row_id);
    }
    else
    {
      tree_branch_to_xml(row.children(), new_elem, internal);
    }
  }
}

void tree_branch_to_xml(const Gtk::TreeNodeChildren &group,
                        xmlpp::Element *parent,
                        bool internal,
                        bool save_ids,
                        RowId pid)
{
  const Data::Columns& model_columns = Data::columns();
  if(!group.empty())
  {
    for(Gtk::TreeModel::iterator iter = group.begin() ; iter != group.end() ; ++iter)
    {
      if(!Data::is_tree_dir(iter))
      {
        xmlpp::Element* new_elem = parent->add_child("data");
        new_elem->set_attribute("name", (*iter).get_value(model_columns.m_col_name));
        new_elem->set_attribute("path", (*iter).get_value(model_columns.m_col_uri));
        if(internal)
        {
          new_elem->set_attribute("size", to_string((*iter).get_value(model_columns.m_col_size)));
          new_elem->set_attribute("mime", (*iter).get_value(model_columns.m_col_mime_type));

          if(save_ids)
          {
            new_elem->set_attribute("id", to_string((*iter).get_value(model_columns.m_col_id)));
            new_elem->set_attribute("pid", to_string(pid));
          }
        }
      }
      else
      {
        xmlpp::Element* new_elem = parent->add_child("directory");
        new_elem->set_attribute("name", (*iter).get_value(model_columns.m_col_name));
        if(save_ids)
        {
          RowId row_id = (*iter).get_value(model_columns.m_col_id);
          
          new_elem->set_attribute("id", to_string(row_id));
          new_elem->set_attribute("pid", to_string(pid));

          tree_branch_to_xml((*iter).children(), new_elem, internal, save_ids, row_id);
        }
        else
        {
          tree_branch_to_xml((*iter).children(), new_elem, internal);
        }
      }
    }
  }
}

void tree_branch_from_xml(Gtk::TreeRow& row,
                          const xmlpp::Element* parent,
                          Glib::RefPtr<Data::Store>& refStore,
                          const sigc::slot<bool,const Gnome::Vfs::FileSize&>& add_slot,
                          bool save_ids)
{
  if(!Data::is_tree_dir(row))
  {
    Gtk::TreeRow parent_row = *(row.parent());
    tree_branch_from_xml(const_cast<Gtk::TreeNodeChildren&>(parent_row.children()), parent, refStore, add_slot, save_ids);
  }
  else
  {
    tree_branch_from_xml(const_cast<Gtk::TreeNodeChildren&>(row.children()), parent, refStore, add_slot, save_ids);
  }
}

void tree_branch_from_xml(Gtk::TreeNodeChildren &children,
                          const xmlpp::Element *parent,
                          Glib::RefPtr<Data::Store>& refStore,
                          const sigc::slot<bool,const Gnome::Vfs::FileSize&>& add_slot,
                          bool save_ids)
{
  xmlpp::Node::NodeList node_list = parent->get_children();
  if(!(node_list.empty()))
  {
    for(xmlpp::Node::NodeList::iterator iter = node_list.begin() ; iter != node_list.end() ; ++iter)
    {
      xmlpp::Element *current_elem = static_cast<xmlpp::Element*>(*iter);

      if( current_elem->get_name() == "data" )
      {
        Data::File data_file(current_elem->get_attribute("path")->get_value());
        if(data_file.is_valid())
        {
          Gtk::TreeRow new_row = *(refStore->append(children));
          Data::Row row(Gtk::TreeRowReference(refStore, refStore->get_path(new_row)),
                        current_elem->get_attribute("name")->get_value(),
                        data_file.get_size(),
                        COASTER_ENTITY_FILE,
                        current_elem->get_attribute("path")->get_value(),
                        data_file.get_mime_type());

          if(save_ids && current_elem->get_attribute("id"))
            Data::new_row(row, refStore, to_rowid(current_elem->get_attribute("id")->get_value()));
          else
            Data::new_row(row, refStore);

          add_slot(data_file.get_size());
        }
        else
        {
          //Bakery::GtkDialogs::ui_warning("File not found: " + data_file.get_uri());
        }
      }
      else if( current_elem->get_name() == "directory" )
      {
        Gtk::TreeRow new_row = *(refStore->append(children));
        Data::Row row(Gtk::TreeRowReference(refStore, refStore->get_path(new_row)),
                      current_elem->get_attribute("name")->get_value(),
                      0,
                      COASTER_ENTITY_DIRECTORY,
                      "",
                      "x-directory/normal");
        if(save_ids && current_elem->get_attribute("id"))
          Data::new_row(row, refStore, to_rowid(current_elem->get_attribute("id")->get_value()));
        else
          Data::new_row(row, refStore);
        
        Gtk::TreeNodeChildren new_children = new_row.children();
        tree_branch_from_xml(new_children, current_elem, refStore, add_slot, save_ids);
      }
    }
  }
}

void tree_branch_from_xml(std::list<Gtk::TreeRow>& rows,
                          Gtk::TreeRow& row,
                          const xmlpp::Element* parent,
                          Glib::RefPtr<Data::Store>& refStore,
                          const sigc::slot<bool,const Gnome::Vfs::FileSize&>& add_slot)
{
  if(!Data::is_tree_dir(row))
  {
    Gtk::TreeRow parent_row = *(row.parent());
    tree_branch_from_xml(rows, const_cast<Gtk::TreeNodeChildren&>(parent_row.children()), parent, refStore, add_slot);
  }
  else
  {
    tree_branch_from_xml(rows, const_cast<Gtk::TreeNodeChildren&>(row.children()), parent, refStore, add_slot);
  }
}

void tree_branch_from_xml(std::list<Gtk::TreeRow>& rows,
                          Gtk::TreeNodeChildren& children,
                          const xmlpp::Element *parent,
                          Glib::RefPtr<Data::Store>& refStore,
                          const sigc::slot<bool,const Gnome::Vfs::FileSize&>& add_slot)
{
  xmlpp::Node::NodeList node_list = parent->get_children();
  if(!(node_list.empty()))
  {
    for(xmlpp::Node::NodeList::iterator iter = node_list.begin() ; iter != node_list.end() ; ++iter)
    {
      xmlpp::Element *current_elem = static_cast<xmlpp::Element*>(*iter);

      Gtk::TreeRow new_row = *(refStore->append(children));
      rows.push_back(new_row);

      if( current_elem->get_name() == "data" )
      {
        Gnome::Vfs::FileSize size = to_rowid(current_elem->get_attribute("size")->get_value());
        Data::Row row(Gtk::TreeRowReference(refStore, refStore->get_path(new_row)),
                      current_elem->get_attribute("name")->get_value(),
                      size,
                      COASTER_ENTITY_FILE,
                      current_elem->get_attribute("path")->get_value(),
                      current_elem->get_attribute("mime")->get_value());

        /*if(save_ids && current_elem->get_attribute("id"))
          Data::new_row(row, refStore, to_rowid(current_elem->get_attribute("id")->get_value()));
        else*/
          Data::new_row(row, refStore);

        add_slot(size);
      }
      else if( current_elem->get_name() == "directory" )
      {
        Data::Row row(Gtk::TreeRowReference(refStore, refStore->get_path(new_row)),
                      current_elem->get_attribute("name")->get_value(),
                      0,
                      COASTER_ENTITY_DIRECTORY,
                      "",
                      "x-directory/normal");
        /*if(save_ids && current_elem->get_attribute("id"))
          Data::new_row(row, refStore, to_rowid(current_elem->get_attribute("id")->get_value()));
        else*/
          Data::new_row(row, refStore);
        
        Gtk::TreeNodeChildren new_children = new_row.children();
        tree_branch_from_xml(rows, new_children, current_elem, refStore, add_slot/*, save_ids*/);
      }
    }
  }
}

void add_back_from_xml(const xmlpp::Element* parent,
                       Glib::RefPtr<Data::Store>& refStore,
                       const sigc::slot<bool,const Gnome::Vfs::FileSize&>& add_slot)
{
  xmlpp::Node::NodeList node_list = parent->get_children();
  if(!node_list.empty())
  {
    for(xmlpp::Node::NodeList::iterator i = node_list.begin() ; i != node_list.end() ; ++i)
    {
      xmlpp::Element *current_elem = static_cast<xmlpp::Element*>(*i);
      
      RowId id = to_rowid(current_elem->get_attribute("id")->get_value());

      Gtk::TreeRow new_row;
      Glib::ustring temp_pid = current_elem->get_attribute("pid")->get_value();
      
      if(temp_pid == "0")
        new_row = *(refStore->append());
      else
      {
        RowId pid = to_rowid(temp_pid);
        
        refStore->foreach_iter(sigc::bind(sigc::ptr_fun(&find_row_with_pid), pid, &new_row, refStore));
      }

      if(current_elem->get_name() == "data")
      {
        Gnome::Vfs::FileSize size = to_rowid(current_elem->get_attribute("size")->get_value());
        
        Data::Row row(Gtk::TreeRowReference(refStore, refStore->get_path(new_row)),
                      current_elem->get_attribute("name")->get_value(),
                      size,
                      COASTER_ENTITY_FILE,
                      current_elem->get_attribute("path")->get_value(),
                      current_elem->get_attribute("mime")->get_value());
        Data::new_row(row, refStore, id);
                           
        add_slot(size);
      }
      else if(current_elem->get_name() == "directory")
      {
        Data::Row row(Gtk::TreeRowReference(refStore, refStore->get_path(new_row)),
                      current_elem->get_attribute("name")->get_value(),
                      0,
                      COASTER_ENTITY_DIRECTORY,
                      "",
                      "x-directory/normal");
        Data::new_row(row, refStore, id);

        Gtk::TreeNodeChildren new_children = new_row.children();
        add_back_from_xml(current_elem, new_children, refStore, add_slot);
      }
    }
  }
}

void add_back_from_xml(const xmlpp::Element* parent,
                       Gtk::TreeNodeChildren& children,
                       Glib::RefPtr<Data::Store>& refStore,
                       const sigc::slot<bool,const Gnome::Vfs::FileSize&>& add_slot)
{
  xmlpp::Node::NodeList node_list = parent->get_children();
  if(!node_list.empty())
  {
    for(xmlpp::Node::NodeList::iterator i = node_list.begin() ; i != node_list.end() ; ++i)
    {
      xmlpp::Element *current_elem = static_cast<xmlpp::Element*>(*i);
      
      RowId id = to_rowid(current_elem->get_attribute("id")->get_value());

      Gtk::TreeRow new_row = *(refStore->append(children));

      if(current_elem->get_name() == "data")
      {
        Gnome::Vfs::FileSize size = to_rowid(current_elem->get_attribute("size")->get_value());

        Data::Row row(Gtk::TreeRowReference(refStore, refStore->get_path(new_row)),
                      current_elem->get_attribute("name")->get_value(),
                      size,
                      COASTER_ENTITY_FILE,
                      current_elem->get_attribute("path")->get_value(),
                      current_elem->get_attribute("mime")->get_value());
        Data::new_row(row, refStore, id);
                           
        add_slot(size);
      }
      else if(current_elem->get_name() == "directory")
      {
        Data::Row row(Gtk::TreeRowReference(refStore, refStore->get_path(new_row)),
                      current_elem->get_attribute("name")->get_value(),
                      0,
                      COASTER_ENTITY_DIRECTORY,
                      "",
                      "x-directory/normal");
        Data::new_row(row, refStore, id);

        Gtk::TreeNodeChildren new_children = new_row.children();
        add_back_from_xml(current_elem, new_children, refStore, add_slot);
      }
    }
  }
}

std::list<Gtk::TreeRow> remove_from_xml(const xmlpp::Element* parent,
                                        Glib::RefPtr<Data::Store>& refStore)
{
  std::list<Gtk::TreeRow> rows;

  xmlpp::Node::NodeList node_list = parent->get_children();
  if(!node_list.empty())
  {
    for(xmlpp::Node::NodeList::iterator i = node_list.begin() ; i != node_list.end() ; ++i)
    {
      xmlpp::Element *current_elem = static_cast<xmlpp::Element*>(*i);
      RowId id = to_rowid(current_elem->get_attribute("id")->get_value());

      Gtk::TreeRow row;
      refStore->foreach_iter(sigc::bind(sigc::ptr_fun(&find_row_with_id), id, &row));

      rows.push_back(row);
    }
  }

  return rows;
}

void rename_from_xml(const xmlpp::Element* node,
                     Glib::RefPtr<Data::Store>& refStore,
                     bool redo)
{
  RowId id = to_rowid(node->get_attribute("id")->get_value());

  Gtk::TreeRow row;
  refStore->foreach_iter(sigc::bind(sigc::ptr_fun(&find_row_with_id), id, &row));
  
  if(!redo) // undo
  {
    Glib::ustring from = node->get_attribute("from")->get_value();
    Data::set_name(row, from);
  }
  else
  {
    Glib::ustring to   = node->get_attribute("to")->get_value();
    Data::set_name(row, to);
  }
}

RowId get_id(const xmlpp::Element* node)
{
  return to_rowid(node->get_attribute("id")->get_value());
}

RowId get_pid(const xmlpp::Element* node)
{
  return to_rowid(node->get_attribute("pid")->get_value());
}

} // namespace XmlIO

} // namespace Coaster
