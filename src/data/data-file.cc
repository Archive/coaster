/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "data/data-file.h"

#include "cstr-debug.h"
#include "cstr-enums.h"

#include <libgnomevfsmm/utils.h>

namespace Coaster
{

namespace Data
{

File::File(const Glib::ustring& uri)
: m_uri(uri)
{
  using namespace Gnome::Vfs;
  try
  {
    m_handle.open(Gnome::Vfs::escape_path_string(m_uri), OPEN_READ);

    m_refFileInfo = m_handle.get_file_info(COMMON_INFO_OPTIONS);

    m_name = m_refFileInfo->get_name();
    m_mime_type = m_refFileInfo->get_mime_type();
    m_size = m_refFileInfo->get_size();
    m_is_valid = true;
  }
  catch(...)
  {
    m_mime_type = "";
    m_name = "";
    m_size = 0;
    m_is_valid = false;
    debug("Invalid: ",m_uri);
  }
}

File::~File()
{
  if(m_is_valid)
    m_handle.close();
}

Glib::ustring File::get_uri()
{
  return m_uri;
}

Glib::ustring File::get_mime_type()
{
  return m_mime_type;
}

Glib::ustring File::get_name()
{
  return m_name;
}

Glib::RefPtr<Gnome::Vfs::FileInfo> File::get_file_info()
{
  return m_refFileInfo;
}

Gnome::Vfs::FileSize File::get_size()
{
  return m_size;
}

bool File::is_valid()
{
  return m_is_valid;
}

} // namespace Data

} // namespace Coaster
