/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_DATA_ROW_H_
#define _COASTER_DATA_ROW_H_

#include "cstr-enums.h"

#include <glibmm/ustring.h>
#include <gtkmm/treerowreference.h>

#include <libgnomevfsmm/types.h>

namespace Coaster
{

namespace Data
{

class Row
{
public:
  Row(const Gtk::TreeRowReference& rowref,
      const Glib::ustring& name,
      Gnome::Vfs::FileSize size,
      EntityType entity_type,
      const Glib::ustring& uri,
      const Glib::ustring& mime)
  : rowref(rowref), name(name), size(size), entity_type(entity_type),
    uri(uri), mime(mime)
  {}

  Row(const Gtk::TreeRowReference& rowref,
      const Glib::ustring& name)
  : rowref(rowref), name(name), size(0), entity_type(COASTER_ENTITY_DIRECTORY),
    uri(""), mime("x-directory/normal")
  {}
  
  Gtk::TreeRowReference rowref;
  Glib::ustring name;
  Gnome::Vfs::FileSize size;
  EntityType entity_type;
  Glib::ustring uri;
  Glib::ustring mime;
};

} // namespace Data
  
} // namespace Coaster

#endif // _COASTER_DATA_ROW_H_
