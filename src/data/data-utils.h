/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_DATA_UTILS_H_
#define _COASTER_DATA_UTILS_H_

#include "cstr-enums.h"
#include "cstr-types.h"

#include "data/data-row.h"

#include <glibmm/ustring.h>
#include <gtkmm/treepath.h>
#include <gtkmm/treeiter.h>

namespace Gtk
{
class TreeStore;
} // namespace Gtk

namespace Coaster
{

namespace Data
{

bool new_row(const Row& row,
             const Glib::RefPtr<Gtk::TreeStore>& treestore,
             RowId id = 0);
void new_directory_row(const Gtk::TreePath& path,
                       const Glib::RefPtr<Gtk::TreeStore>& treestore,
                       const Glib::ustring& name);
void new_file_row(const Gtk::TreePath& path,
                  const Glib::RefPtr<Gtk::TreeStore>& treestore,
                  const Glib::ustring& name,
                  int size,
                  EntityType entity_type,
                  const Glib::ustring& uri,
                  const Glib::ustring& mime_type);
void copy_row(Gtk::TreeRow& new_row,
              const Gtk::TreeRow& old_row);
bool is_tree_dir(const Gtk::TreeIter& iter);

void set_name(Gtk::TreeRow& row,
              const Glib::ustring& name);

} // namespace Data

} // namespace Coaster

#endif // _COASTER__H_
