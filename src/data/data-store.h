/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_DATA_STORE_H_
#define _COASTER_DATA_STORE_H_

#include "data/data-columns.h"

#include <gtkmm/treestore.h>
#include <gtkmm/treemodel.h>
#include <gtkmm/selectiondata.h>

namespace Coaster
{

namespace Data
{

class Store : public Gtk::TreeStore
{
public:
  virtual ~Store();
  static Glib::RefPtr<Store> create(const Columns& cols);

  Columns m_Columns;
  
protected:
  Store(const Columns& cols);

  //Overridden virtual functions:
  virtual bool row_drop_possible_vfunc(const Gtk::TreeModel::Path& dest,
                                       const Gtk::SelectionData& selection_data) const;
};

} // namespace Data

} // namespace Coaster

#endif // _COASTER_DATA_STORE_H_
