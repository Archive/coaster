/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_DATA_CD_INFO_H_
#define _COASTER_DATA_CD_INFO_H_

#include "cstr-enums.h"
#include "widgets/cd-info.h"

#include <libgnomevfsmm/types.h>

namespace Coaster
{

namespace Data
{

class CDInfo : public Widgets::CDInfo
{
public:
  CDInfo();
  virtual ~CDInfo();

  bool cd_size_add(Gnome::Vfs::FileSize size);
  void cd_size_remove(Gnome::Vfs::FileSize size);

  typedef sigc::signal<void, CDSizeType> type_signal_cd_size_changed;
  type_signal_cd_size_changed& signal_cd_size_changed();

  Gnome::Vfs::FileSize get_size_used() const;

  CDSizeType get_cd_size() const;
  void set_cd_size(CDSizeType size);

protected:
  void on_combo_box_changed();
  void update_progress_bar();

private:
  // Signals
  type_signal_cd_size_changed signal_cd_size_changed_;

  // private variables
  Gnome::Vfs::FileSize m_cd_size, m_size_used, m_size_unused;
};

} // namespace Data

} // namespace Coaster

#endif // _COASTER_DATA_CD_INFO_H_
