/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "data/data-store.h"

#include "data/data-utils.h"

namespace Coaster
{

namespace Data
{

Store::Store(const Columns& cols)
{
  set_column_types(cols);
}

Store::~Store()
{}

Glib::RefPtr<Store> Store::create(const Columns& cols)
{
  return Glib::RefPtr<Store>( new Store(cols) );
}

bool Store::row_drop_possible_vfunc(const Gtk::TreeModel::Path& dest,
                                        const Gtk::SelectionData& selection_data) const
{
  Gtk::TreeModel::Path dest_parent = dest;
  bool dest_is_not_top_level = dest_parent.up();

  if(!dest_is_not_top_level || dest_parent.empty())
  {
    //The user wants to move something to the top-level.
    //Let's always allow that.
    return true;
  }
  else
  {
    //Get an iterator for the row at this path:
    //We must unconst this. This should not be necessary with a future version of gtkmm.
    Store* unconstThis = const_cast<Store*>(this);
    const_iterator iter_dest_parent = unconstThis->get_iter(dest_parent);
    //const_iterator iter_dest_parent = get_iter(dest);
    if(iter_dest_parent)
    {
      if(Data::is_tree_dir(iter_dest_parent))
        return true;
      else
        return false;
    }
  }

  return Gtk::TreeStore::row_drop_possible_vfunc(dest, selection_data);
}

} // namespace Data

} // namespace Coaster
