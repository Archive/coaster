/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_DATA_FILE_H_
#define _COASTER_DATA_FILE_H_

#include <glibmm.h>
#include <libgnomevfsmm/types.h>
#include <libgnomevfsmm/handle.h>

namespace Coaster
{

namespace Data
{

class File
{
public:
  explicit File(const Glib::ustring& uri);
  virtual ~File();

  Glib::ustring get_uri();
  Glib::ustring get_mime_type();
  Glib::ustring get_name();
  Glib::RefPtr<Gnome::Vfs::FileInfo> get_file_info();
  Gnome::Vfs::FileSize get_size();
  bool is_valid();

protected:

private:
  Gnome::Vfs::Handle m_handle;
  Glib::RefPtr<Gnome::Vfs::FileInfo> m_refFileInfo;
  const Glib::ustring m_uri;
  Glib::ustring m_mime_type;
  Glib::ustring m_name;
  Gnome::Vfs::FileSize m_size;
  bool m_is_valid;
};

} // namespace Data
  
} // namespace Coaster

#endif // _COASTER_DATA_FILE_H_
