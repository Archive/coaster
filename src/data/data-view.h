/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_DATA_VIEW_H_
#define _COASTER_DATA_VIEW_H_

#include "widgets/view-base.h"
#include "data/data-prop.h"

#include "sharedptr.h"

#include <libgnomevfsmm/types.h>

namespace Gtk
{
class Action;
class TreeStore;
} // namespace Gtk

namespace Coaster
{

namespace Data
{

class Layout;
class CDInfo;

class View : public Widgets::View_Base
{
public:
  View();
  virtual ~View();

  typedef std::list<Glib::ustring> type_listUStrings;
  
  Glib::ustring get_ui_location() const;

  bool cd_size_add(const Gnome::Vfs::FileSize& size);
  void cd_size_remove(const Gnome::Vfs::FileSize& size);

protected:
  
  void init_properties();

  // overrides
  void save_to_document();
  void load_from_document();
  void enable_selection_actions(bool enable = true);
  void enable_layout_item_actions(bool enable = true);

  // member methods
  void on_properties_clicked();
  void on_export_iso_clicked();
  void on_cd_size_changed(CDSizeType size);
  void on_burn_clicked(); // override
  void on_layout_modified(bool modified); // override
  void on_layout_selection_changed(int number_selected); // override

  void on_undo_manager_changed();

  void on_undo();
  void on_redo();

private:
  Data::Properties m_DataProperties;

  Layout* m_pLayout;
  CDInfo *m_pCDInfo;

  // Export Actions
  Glib::RefPtr<Gtk::Action> m_refExport;

  // Undo Actions
  Glib::RefPtr<Gtk::Action> m_refUndo;
  Glib::RefPtr<Gtk::Action> m_refRedo;

  // Selection Actions
  Glib::RefPtr<Gtk::Action> m_refRename;
  Glib::RefPtr<Gtk::Action> m_refRemove;
  Glib::RefPtr<Gtk::Action> m_refCut;
  Glib::RefPtr<Gtk::Action> m_refCopy;

  // Layout Item Actions
  Glib::RefPtr<Gtk::Action> m_refClear;
  Glib::RefPtr<Gtk::Action> m_refSelectAll;
  Glib::RefPtr<Gtk::Action> m_refBurn;
};

} // namespace Data

} // namespace Coaster

#endif // _COASTER_DATA_VIEW_H_
