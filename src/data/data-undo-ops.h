/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_DATA_UNDO_OPS_H_
#define _COASTER_DATA_UNDO_OPS_H_

#include "undo-op.h"

#include <glibmm/ustring.h>
#include <gtkmm/treeiter.h>

#include <list>

namespace xmlpp
{
class Element;
} // namespace xmlpp

namespace Coaster
{

namespace Data
{

class Store;

class RemoveOp : public UndoOp
{
public:
  typedef std::list<Gtk::TreeRow>  type_list_rows;

  RemoveOp(const type_list_rows& rows,
           const Glib::ustring& label,
           const sigc::slot<void,const xmlpp::Element*,bool,bool>& add_slot,
           const sigc::slot<void,const xmlpp::Element*>& remove_slot);
  RemoveOp(const Gtk::TreeNodeChildren& children,
           const Glib::ustring& label,
           const sigc::slot<void,const xmlpp::Element*,bool,bool>& add_slot,
           const sigc::slot<void,const xmlpp::Element*>& remove_slot);

  virtual ~RemoveOp();

  void undo();
  void redo();
private:
  sigc::slot<void,const xmlpp::Element*,bool,bool> add_from_xml;
  sigc::slot<void,const xmlpp::Element*> remove_from_xml;
};

class AddOp : public UndoOp
{
public:
  typedef std::list<Gtk::TreeRow>  type_list_rows;

  AddOp(const type_list_rows& rows,
        const Glib::ustring& label,
        const sigc::slot<void,const xmlpp::Element*>& remove_slot,
        const sigc::slot<void,const xmlpp::Element*,bool,bool>& add_slot);

  virtual ~AddOp();

  void undo();
  void redo();
private:
  sigc::slot<void,const xmlpp::Element*> remove_from_xml;
  sigc::slot<void,const xmlpp::Element*,bool,bool> add_from_xml;
};

class RenameOp : public UndoOp
{
public:
  RenameOp(const Gtk::TreeRow& row,
           const Glib::ustring& from,
           const Glib::ustring& to,
           const Glib::RefPtr<Store>& refStore);
  virtual ~RenameOp();

  void undo();
  void redo();

private:
  xmlpp::Element* m_pRenameNode;
  Glib::RefPtr<Store> refStore;
};

} // namespace Data

} // namespace Coaster

#endif // _COASTER__H_
