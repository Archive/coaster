/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "data/data-undo-ops.h"

#include "cstr-intl.h"
#include "cstr-debug.h"
#include "cstr-types.h"

#include "ucompose.h"
#include "xml-io.h"

#include "data/data-store.h"
#include "data/data-utils.h"

#include <libxml++/document.h>

namespace
{

bool find_row_with_id(const Gtk::TreeIter& iter,
                      Coaster::RowId id, Gtk::TreeRow* row)
{
  using namespace Coaster::Data;
  const Columns& columns = Coaster::Data::columns();
  if((*iter).get_value(columns.m_col_id) == id)
  {
    *row = (*iter);
    return true;
  }
  else
    return false;
}

} // anonymous namespace

namespace Coaster
{

namespace Data
{

/////////////////////
//  RemoveOp
/////////////////////
RemoveOp::RemoveOp(const type_list_rows& rows,
                   const Glib::ustring& label,
                   const sigc::slot<void,const xmlpp::Element*,bool,bool>& add_slot,
                   const sigc::slot<void,const xmlpp::Element*>& remove_slot)
: UndoOp(label),
  add_from_xml(add_slot), remove_from_xml(remove_slot)
{
  m_pDocument = new xmlpp::Document();
  const Columns& columns = Data::columns();

  xmlpp::Element* rootNode = m_pDocument->create_root_node("coaster-rows");
  for(std::list<Gtk::TreeRow>::const_iterator i = rows.begin() ; i != rows.end() ; ++i)
  {
    Gtk::TreeIter iter = (*i).parent();
    if(iter)
      XmlIO::tree_branch_to_xml((*i), rootNode, true, true, (*iter).get_value(columns.m_col_id));
    else
      XmlIO::tree_branch_to_xml((*i), rootNode, true, true);
  }

  debug("Undo ",m_Label, " XML:");
  debug(m_pDocument);
}

RemoveOp::RemoveOp(const Gtk::TreeNodeChildren& children,
                   const Glib::ustring& label,
                   const sigc::slot<void,const xmlpp::Element*,bool,bool>& add_slot,
                   const sigc::slot<void,const xmlpp::Element*>& remove_slot)
: UndoOp(label),
  add_from_xml(add_slot), remove_from_xml(remove_slot)
{
  m_pDocument = new xmlpp::Document();

  xmlpp::Element* rootNode = m_pDocument->create_root_node("coaster-rows");
  XmlIO::tree_branch_to_xml(children, rootNode, true, true);

  debug("Undo ",m_Label, " XML:");
  debug(m_pDocument);
}

RemoveOp::~RemoveOp()
{}

void RemoveOp::undo()
{
  debug("Undo: ",m_Label);
  add_from_xml(m_pDocument->get_root_node(), true, false);
}

void RemoveOp::redo()
{
  debug("Redo: ",m_Label);
  remove_from_xml(m_pDocument->get_root_node());
}


////////////////
//  AddOp
////////////////
AddOp::AddOp(const type_list_rows& rows,
             const Glib::ustring& label,
             const sigc::slot<void,const xmlpp::Element*>& remove_slot,
             const sigc::slot<void,const xmlpp::Element*,bool,bool>& add_slot)
: UndoOp(label),
  remove_from_xml(remove_slot), add_from_xml(add_slot)
{
  m_pDocument = new xmlpp::Document();
  const Columns& columns = Data::columns();

  xmlpp::Element* rootNode = m_pDocument->create_root_node("coaster-rows");
  for(std::list<Gtk::TreeRow>::const_iterator i = rows.begin() ; i != rows.end() ; ++i)
  {
    Gtk::TreeIter iter = (*i).parent();
    if(iter)
      XmlIO::tree_branch_to_xml((*i), rootNode, true, true, (*iter).get_value(columns.m_col_id));
    else
      XmlIO::tree_branch_to_xml((*i), rootNode, true, true);
  }

  debug("Undo ",m_Label, " XML:");
  debug(m_pDocument);
}

AddOp::~AddOp()
{}

void AddOp::undo()
{
  debug("Undo: ",m_Label);
  remove_from_xml(m_pDocument->get_root_node());
}

void AddOp::redo()
{
  debug("Redo: ",m_Label);
  add_from_xml(m_pDocument->get_root_node(), true, false);
}


/////////////////
//  RenameOp
/////////////////
RenameOp::RenameOp(const Gtk::TreeRow& row,
                   const Glib::ustring& from,
                   const Glib::ustring& to,
                   const Glib::RefPtr<Store>& refStore)
: UndoOp(_("Rename")), refStore(refStore)
{
  m_pDocument = new xmlpp::Document();

  xmlpp::Element* rootNode = m_pDocument->create_root_node("coaster-rows");
  m_pRenameNode = rootNode->add_child("rename");
  m_pRenameNode->set_attribute("from", from);
  m_pRenameNode->set_attribute("to", to);

  const Columns& columns = Data::columns();

  std::stringstream oss;
  oss << row.get_value(columns.m_col_id);
  m_pRenameNode->set_attribute("id", oss.str());

  debug("Undo Rename XML:");
  debug(m_pDocument);
}

RenameOp::~RenameOp()
{}

void RenameOp::undo()
{
  debug("Undo Rename");
  XmlIO::rename_from_xml(m_pRenameNode, refStore, false);
}

void RenameOp::redo()
{
  debug("Redo Rename");
  XmlIO::rename_from_xml(m_pRenameNode, refStore, true);
}

} // namespace Data

} // namespace Coaster
