/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "data/data-view.h"

#include "cstr-intl.h"
#include "cstr-debug.h"
#include "ucompose.h"

#include "document.h"
#include "undo-manager.h"

#include "data/data-layout.h"
#include "data/data-cd-info.h"
#include "data/data-utils.h"

#include "backends/cd-io.h"
#include "backends/nautilus-make-iso.h"
#include "util.h"
#include "sound.h"
#include "xml-io.h"
#include "instance-manager.h"

#include "dialogs/glade-dialog.h"
#include "dialogs/properties.h"
#include "dialogs/burn.h"
#include "dialogs/dialog-util.h"

#include <gtkmm/actiongroup.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/stock.h>

#include <libxml++/document.h>

#include <bakery/Utilities/BusyCursor.h>

namespace Coaster
{

namespace Data
{

View::View()
: type_base(), m_pLayout(0), m_pCDInfo(0)
{
  using namespace Gtk;
  using namespace Gnome::Glade;

  set_border_width(8);
  set_spacing(8);

  // Layout
  ScrolledWindow* sw = new ScrolledWindow();
  sw->set_policy(POLICY_NEVER, POLICY_AUTOMATIC);
  sw->set_shadow_type(SHADOW_IN);

  m_pLayout = new Layout(sigc::mem_fun(*this, &View::popup),
                         sigc::mem_fun(*this, &View::cd_size_add),
                         sigc::mem_fun(*this, &View::cd_size_remove),
                         sigc::mem_fun(*undo_manager, &UndoManager::push));

  m_pLayout->signal_modified().connect(sigc::mem_fun(*this, &View::on_layout_modified));
  m_pLayout->signal_selection_changed().connect(sigc::mem_fun(*this, &View::on_layout_selection_changed));

  sw->add(*manage(m_pLayout));
  pack_start(*manage(sw));
  
  // CDInfo Box
  m_pCDInfo = new CDInfo();
  pack_start(*manage(m_pCDInfo), false, false);

  // Set up the View's actions
  // File:

  m_refExport = Action::create("CoasterAction_ExportISO", Gtk::Stock::CONVERT,
                               _("E_xport to ISO..."), _("Export this layout to an ISO image"));
  m_refViewActionGroup->add(m_refExport,
                            sigc::mem_fun(*this, &View::on_export_iso_clicked));

  // Edit:

  m_refUndo = Action::create("CoasterAction_Undo", Gtk::Stock::UNDO);
  m_refViewActionGroup->add(m_refUndo,
                            AccelKey("<control>Z"),
                            sigc::mem_fun(*this, &View::on_undo));

  m_refRedo = Action::create("CoasterAction_Redo", Gtk::Stock::REDO);
  m_refViewActionGroup->add(m_refRedo,
                            AccelKey("<control><shift>Z"),
                            sigc::mem_fun(*this, &View::on_redo));

  m_refCut = Action::create("CoasterAction_Cut", Gtk::Stock::CUT);
  m_refViewActionGroup->add(m_refCut,
                            sigc::mem_fun(m_pLayout, &Layout::on_cut));

  m_refCopy = Action::create("CoasterAction_Copy", Gtk::Stock::COPY);
  m_refViewActionGroup->add(m_refCopy,
                            sigc::mem_fun(m_pLayout, &Layout::on_copy));

  m_refViewActionGroup->add(Action::create("CoasterAction_Paste", Gtk::Stock::PASTE),
                            sigc::mem_fun(m_pLayout, &Layout::on_paste));
 
  m_refClear = Action::create("CoasterAction_Clear", Gtk::Stock::CLEAR,
                              _("C_lear"), _("Clear all files from the current layout"));
  m_refViewActionGroup->add(m_refClear,
                            sigc::mem_fun(m_pLayout, &Layout::on_clear));
  
  m_refSelectAll = Action::create("CoasterAction_SelectAll", 
                            _("Select _All"), _("Select all files in current layout"));
  m_refViewActionGroup->add(m_refSelectAll, 
                            AccelKey("<control><shift>A"),
                            sigc::mem_fun(m_pLayout, &Layout::on_select_all));

  // Search:
  m_refViewActionGroup->add(Action::create("CoasterAction_Search_Find", Gtk::Stock::FIND,
                              _("_Find"), _("Search for filename")),
                              sigc::mem_fun(m_pLayout, &Layout::on_search_clicked));
  m_refViewActionGroup->add(Action::create("CoasterAction_Search_FindNext", Gtk::Stock::FIND, 
                              _("Find Ne_xt"), _("Search forwards for the same filename")),
                              AccelKey("<control>G"));
  m_refViewActionGroup->add(Action::create("CoasterAction_Search_FindPrev", Gtk::Stock::FIND,
                              _("Find Ne_xt"), _("Search backwards for the same filename")),
                              AccelKey("<shift><control>G"));

  // Layout:
  Glib::RefPtr<Action> addFiles = Action::create("CoasterAction_Layout_Add_Files",
                        StockID("coaster-add-files"), 
                        _("Add _Files..."), _("Add files to the current layout"));
  addFiles->property_is_important() = true;
  m_refViewActionGroup->add(addFiles,
                        Gtk::AccelKey("<control>A"),
                        sigc::mem_fun(m_pLayout, &Layout::on_add_files));
  m_refViewActionGroup->add(Action::create("CoasterAction_Layout_Add_Folder",
                        StockID("coaster-add-folder"),
                        _("_Add F_older..."),
                        _("Add a folder from your computer to the current layout")),
                        sigc::mem_fun(m_pLayout, &Layout::on_add_folder));

  m_refViewActionGroup->add(Action::create("CoasterAction_Layout_Create_Folder",
                        _("_Create Folder"), _("Create a folder in the current layout")),
                        Gtk::AccelKey("<control><shift>N"),
                        sigc::mem_fun(m_pLayout, &Layout::on_create_directory));

  m_refRename = Action::create("CoasterAction_Layout_Rename", _("R_ename"),
                               _("Rename the selected file in the current layout"));
  m_refViewActionGroup->add(m_refRename, Gtk::AccelKey("F2"),
                        sigc::mem_fun(m_pLayout, &Layout::on_rename_selected));

  m_refRemove = Action::create("CoasterAction_Layout_Remove", Gtk::Stock::REMOVE, 
                               _("_Remove"),
                               _("Remove the selected file(s) in the current layout"));
  m_refViewActionGroup->add(m_refRemove,
                        Gtk::AccelKey("Delete"),
                        sigc::mem_fun(m_pLayout, &Layout::on_remove_selected));
  
  m_refViewActionGroup->add(Action::create("CoasterAction_Layout_Properties",
                        Gtk::Stock::PROPERTIES,
                        _("Proper_ties"), _("Change the properties of the current layout")),
                        sigc::mem_fun(*this, &View::on_properties_clicked));

  // CD Actions:
  m_refBurn = Action::create("CoasterAction_CD_Burn", Gtk::StockID("coaster-burn"),
                             _("_Burn..."), _("Burn current layout to CD"));
  m_refBurn->property_is_important() = true;
  m_refViewActionGroup->add(m_refBurn, Gtk::AccelKey("<control>B"),
                            sigc::mem_fun(*this, &View::on_burn_clicked));

  init_properties();
  enable_selection_actions(false);
  m_refUndo->set_sensitive(false);
  m_refRedo->set_sensitive(false);
  m_refRename->set_sensitive(false);

  show_all_children();
}

View::~View()
{}

void View::init_properties()
{
  m_DataProperties.size = COASTER_CD_SIZE_NONE;
  m_DataProperties.isolevel = COASTER_ISO_LEVEL2;
  m_DataProperties.joliet = true;
  m_DataProperties.rockridge = true;
  m_DataProperties.sessionformat = COASTER_SESSION_FMT_CDROM;
}

Glib::ustring View::get_ui_location() const
{
  return Glib::ustring(COASTER_UIDIR "/coaster-data-menu.ui");
}

bool View::cd_size_add(const Gnome::Vfs::FileSize& size)
{
  return m_pCDInfo->cd_size_add(size);
}

void View::cd_size_remove(const Gnome::Vfs::FileSize& size)
{
  m_pCDInfo->cd_size_remove(size);
}

void View::save_to_document()
{
  debug("View: save to doc");
  get_document()->start_save();
  static_cast<Document*>(get_document())->set_document_properties(m_DataProperties);
  static_cast<Document*>(get_document())->save_tree(m_pLayout->get_tree());
}

void View::load_from_document()
{
  debug("View: load from doc");
  static_cast<Document*>(get_document())->get_document_properties(m_DataProperties);
  m_pCDInfo->set_cd_size(m_DataProperties.size);
  m_pCDInfo->signal_cd_size_changed().connect(sigc::mem_fun(*this, &View::on_cd_size_changed));

  static_cast<Document*>(get_document())->load_tree(m_pLayout->get_tree(),
                                                    sigc::mem_fun(*this, &View::cd_size_add));

  enable_layout_item_actions(m_pLayout->has_files());
  //load_icons(true /* loading doc */);
}

void View::enable_selection_actions(bool enable)
{
  m_refRemove->set_sensitive(enable);
  m_refCut->set_sensitive(enable);
  m_refCopy->set_sensitive(enable);
}

void View::enable_layout_item_actions(bool enable)
{
  m_refClear->set_sensitive(enable);
  m_refSelectAll->set_sensitive(enable);
  m_refBurn->set_sensitive(enable);
  m_refExport->set_sensitive(enable);
}

void View::on_properties_clicked()
{
  debug("View: on properties clicked");

  using namespace Gtk;
  std::auto_ptr<Dialogs::Properties> dataprop =
    Dialogs::GladeDialog<Dialogs::Properties>::create(Util::get_win(this), "data_prop");
  dataprop->init(m_DataProperties);
  int result = dataprop->run();

  Data::Properties tempDP = dataprop->get_properties();
  if(result == Gtk::RESPONSE_OK)
  {
    if(m_DataProperties != tempDP)
    {
      m_DataProperties = tempDP;
      set_modified(true);
    }
  }
}

void View::on_export_iso_clicked()
{
  // set all instances so we can't do anything
  instmgr->set_sensitive(false);

  int response;
  std::string iso_filename;
  
  Dialogs::ui_offer_export_iso(Util::get_win(this), response, iso_filename);

  if(response == Gtk::RESPONSE_OK)
  {
    Bakery::BusyCursor cursor(Util::get_win(this));
    xmlpp::Document* layout = XmlIO::tree_to_document(m_pLayout->get_tree()->children());
    SharedPtr<IO::MakeIso> mkiso = SharedPtr<IO::MakeIso>(new IO::MakeIso(iso_filename,
                                                                          layout->get_root_node(),
                                                                          m_DataProperties));

    mkiso->make_iso();

    delete layout;
  }
  
  // return all instances to normal
  instmgr->set_sensitive(true);
}

void View::on_cd_size_changed(CDSizeType size)
{
  if(m_DataProperties.size == size)
    return;
  else
  {
    m_DataProperties.size = size;
    set_modified(true);
  } 
}

void View::on_burn_clicked()
{
  using namespace Gnome;
  debug("View: on burn clicked");
 
  if(m_DataProperties.title == "")
  {
    bool check = Util::get_bool("layouts/title_check");
    if(check)
    {
      // create and run dialog.  if they hit "No", return.  if they hit "Yes", go on.
      int response = Dialogs::ui_warn_title_check(Util::get_win(this));
      
      if(response != Gtk::RESPONSE_OK)
          return;
    } 
  }
    
  std::auto_ptr<Dialogs::Burn> burndialog = 
    Dialogs::GladeDialog<Dialogs::Burn>::create(Util::get_win(this),
                                       "diag_burn");

  // render this pixbuf before setting the interface insensitive
  // so we get a normal lookign icon
  Glib::RefPtr<Gdk::Pixbuf> pb_idle, pb_burn;
  pb_idle = render_icon(Gtk::StockID("coaster-idle"), Gtk::ICON_SIZE_MENU);
  pb_burn = render_icon(Gtk::StockID("coaster-burn"), Gtk::ICON_SIZE_MENU);

  // set all instances so we can't do anything
  instmgr->set_sensitive(false);

  instmgr->create_status_icon(pb_idle);
  instmgr->set_status_tip("Idle");

  int result = burndialog->run();

  if(result == Gtk::RESPONSE_OK)
  {
    burndialog->set_action_widgets_sensitive(false);
    burndialog->hide();
    
    instmgr->set_status_pixbuf(pb_burn);
    instmgr->set_status_tip("Burning...");
    
    xmlpp::Document* layout = XmlIO::tree_to_document(m_pLayout->get_tree()->children());
    IO::BurnDisc burn(Util::get_win(this),
                      burndialog->get_cd_drive(),
                      burndialog->get_eject(),
                      burndialog->get_speed(),
                      burndialog->get_dummy(),
                      layout,
                      m_DataProperties);
    if(burn.start_burn())
      Sound::burn_finished();
    else
      Sound::burn_error();

    delete layout;
  }

  instmgr->remove_status_icon();

  // return all instances to normal
  instmgr->set_sensitive(true);
}

void View::on_layout_modified(bool modified)
{
  enable_layout_item_actions(m_pLayout->has_files());
  get_document()->set_modified(modified);
}

void View::on_layout_selection_changed(int number_selected)
{
  enable_selection_actions(number_selected);
  if(number_selected == 1)
    m_refRename->set_sensitive(true);
  else
    m_refRename->set_sensitive(false);
}

void View::on_undo_manager_changed()
{
  m_refUndo->property_label() = String::ucompose("_Undo%1", undo_manager->undo_label());
  m_refRedo->property_label() = String::ucompose("_Redo%1", undo_manager->redo_label());
  
  m_refUndo->set_sensitive(undo_manager->undo_size());
  m_refRedo->set_sensitive(undo_manager->redo_size());
}

void View::on_undo()
{
  undo_manager->undo();
}

void View::on_redo()
{
  undo_manager->redo();
}

} // namespace Data

} // namespace Coaster
