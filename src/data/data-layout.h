/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_DATA_LAYOUT_H_
#define _COASTER_DATA_LAYOUT_H_

#include "widgets/layout-base.h"

#include "sharedptr.h"
#include "undo-op.h"

#include "data/data-store.h"

#include <gtkmm/treestore.h>
#include <gtkmm/treerowreference.h>

#include "data/data-columns.h"

#include <vector>

namespace xmlpp
{
class Element;
} // namespace xmlpp

namespace Gtk
{
class Entry;
class TreeSelection;
class Style;
} // namespace Gtk

namespace Gnome
{

namespace Vfs
{
class FileInfo;
}// namespace Vfs

namespace Glade
{
class Xml;
}// namespace Glade

}// namespace Gnome

namespace Coaster
{

class InstanceMgr;
typedef SharedPtr<InstanceMgr> InstanceMgrPtr;

namespace Data
{

class Layout : public Widgets::Layout_Base< Store, Columns >
{
public:
  explicit Layout(const sigc::slot<void,guint,guint32>& slot_popup,
                  const sigc::slot<bool,const Gnome::Vfs::FileSize&>& slot_cd_size_add,
                  const sigc::slot<void,const Gnome::Vfs::FileSize&>& slot_cd_size_remove,
                  const sigc::slot<void,const UndoOpPtr&>& slot_push_undo);
  virtual ~Layout();

  typedef std::list<Glib::ustring> type_listUStrings;

  void add_from_xml(const xmlpp::Element* rootNode,
                    bool respect_ids = false,
                    bool undo = false);
  void add_from_xml_dnd(const xmlpp::Element* rootNode,
                        const Gtk::TreePath& path);
  void remove_from_xml(const xmlpp::Element* rootNode);

  // used by View
  void on_clear();
  void on_cut();
  void on_copy();
  void on_paste();
  void on_search_clicked();
  void on_add_files();
  void on_add_folder();
  void on_create_directory();
  void on_rename_selected();
  void on_remove_selected();

protected:
  // overrides
  void init();
  void init_columns();
  void init_drag_n_drop();
  
  void search(const Glib::ustring& search_string);
  bool on_search_foreach_case(const Gtk::TreeIter& iter,
                              const Glib::ustring& search_string);
  bool on_search_foreach_nocase(const Gtk::TreeIter& iter,
                                const Glib::ustring& search_string);
  void on_tree_selection_changed();
  
  bool on_button_press_event(GdkEventButton* event);
  bool on_key_press_event(GdkEventKey* event);

  void on_name_edited(const Glib::ustring& path,
                      const Glib::ustring& new_text);
  void on_name_editing_canceled();

  // layout changed signal handlers
  void on_row_changed(const Gtk::TreePath& path,
                      const Gtk::TreeIter& iter);
  void on_row_deleted(const Gtk::TreePath& path);
  void on_row_inserted(const Gtk::TreePath& path,
                       const Gtk::TreeIter& iter);
  void on_rows_reordered(const Gtk::TreePath& path,
                         const Gtk::TreeIter& iter,
                         int* new_order);

  // layout dnd overrides
  void on_drag_begin(const Glib::RefPtr<Gdk::DragContext>& context);
  void on_drag_leave(const Glib::RefPtr<Gdk::DragContext>& context,
                     guint time);
  bool on_drag_motion(const Glib::RefPtr<Gdk::DragContext>& context,
                      int x,
                      int y,
                      guint time);
  bool on_drag_drop(const Glib::RefPtr<Gdk::DragContext>& context,
                    int x,
                    int y,
                    guint time);
  void on_drag_data_get(const Glib::RefPtr<Gdk::DragContext>& context,
                        Gtk::SelectionData& selection_data,
                        guint info,
                        guint time);
  void on_drag_data_received(const Glib::RefPtr<Gdk::DragContext>& context,
                             int x,
                             int y,
                             const Gtk::SelectionData& selection_data,
                             guint info,
                             guint time);
  void on_drag_data_delete(const Glib::RefPtr<Gdk::DragContext>& context);
  void on_drag_end(const Glib::RefPtr<Gdk::DragContext>& context);

  // header clicking signal handlers
  /*void on_name_header_clicked();
  void on_size_header_clicked();
  void on_location_header_clicked();*/

private:
  void icon_cell_data_func(Gtk::CellRenderer* cell,
                           const Gtk::TreeModel::iterator& iter);
  void size_cell_data_func(Gtk::CellRenderer* cell,
                           const Gtk::TreeModel::iterator& iter);
  void on_text_edited(const Glib::ustring& path_string,
                      const Glib::ustring& new_text);
  void on_text_editing_canceled();
  void do_nothing();
  void add_files_from_list(const type_listUStrings& files,
                           bool hidden);
  void add_to_folder_from_dir(const Gtk::TreeRowReference& parent_row,
                              const Glib::ustring& folder,
                              bool recurse,
                              bool hidden,
                              bool& changed,
                              type_listUStrings& unable_to_add);

  void copy_selected(bool cut = false);
  void select_all();

  Gtk::TreePath get_new_entity_row();
  Gtk::TreePath get_new_entity_row(const Gtk::TreePath& path);
  void expand_to_created_path(const Gtk::TreePath& path);

  std::list<Gtk::TreeRow> remove_duplicate_children(const type_listTP& paths);
  void erase_rows(const std::list<Gtk::TreeRow>& rows);

  Gtk::TreeNodeChildren get_drop_children(bool exists,
                                          const Gtk::TreePath& path,
                                          const Gtk::TreeViewDropPosition& pos);

  void on_icon_theme_changed();
  bool icon_theme_changed_foreach(const Gtk::TreeModel::iterator& iter);

  sigc::slot<void,guint,guint32> popup;
  sigc::slot<bool,const Gnome::Vfs::FileSize&> cd_size_add;
  sigc::slot<void,const Gnome::Vfs::FileSize&> cd_size_remove;
  sigc::slot<void,const UndoOpPtr&> push_undo;

  InstanceMgrPtr instmgr;

  // Columns
  Column* m_colName;
  Column* m_colSize;
  Column* m_colUri;
};

} // namespace Data

} // namespace Coaster

#endif // _COASTER_DATA_LAYOUT_H_
