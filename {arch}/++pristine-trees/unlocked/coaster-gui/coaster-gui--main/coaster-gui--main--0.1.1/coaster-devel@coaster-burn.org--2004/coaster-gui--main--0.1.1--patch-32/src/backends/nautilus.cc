/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "cd-io.h"

#include "backends/nautilus-make-iso.h"

#include "cstr-intl.h"
#include "cstr-debug.h"
#include "data/data-columns.h"
#include "dialogs/glade-dialog.h"
#include "dialogs/burn-progress.h"

#include "ucompose.h"
#include "instance-manager.h"
#include "baconmm/cd-recorder-cpp.h"
#include "cd-recorder.h"

#include <config.h>

#include <gconfmm/client.h>

#include <libxml++/document.h>

#include <libgnomevfsmm/handle.h>
#include <signal.h>

namespace Coaster
{
  
namespace IO
{

BurnDisc::BurnDisc(const Gtk::Window& parent_win,
                   const CDDrive* rec,
                   bool eject_cd,
                   int speed,
                   bool dummy,
                   const xmlpp::Document* CDStructure,
                   const Data::Properties& dp)
: instmgr(InstanceMgr::instance()), m_parent_win(const_cast<Gtk::Window*>(&parent_win)),
  m_pCDDrive(const_cast<CDDrive*>(rec)), m_bEject_CD(eject_cd),
  m_dSpeed(speed), m_bDummy(dummy), m_pDocument(const_cast<xmlpp::Document*>(CDStructure)),
  m_DataProperties(dp)
{
  m_pBurnProgress =
    Dialogs::GladeDialog<Dialogs::BurnProgress>::create(*m_parent_win, "diag_burn_progress");
  m_pBurnProgress->signal_cancel_clicked().connect(sigc::mem_fun(*this, &BurnDisc::on_cancel_clicked));
}

BurnDisc::BurnDisc(const Gtk::Window& parent_win,
                   const CDDrive* rec,
                   bool eject_cd,
                   int speed,
                   bool dummy,
                   const std::string& filename)
: instmgr(InstanceMgr::instance()), m_parent_win(const_cast<Gtk::Window*>(&parent_win)),
  m_pCDDrive(const_cast<CDDrive*>(rec)), m_bEject_CD(eject_cd),
  m_dSpeed(speed), m_bDummy(dummy), m_pDocument(0)
{
  m_filename = filename;
  m_pBurnProgress =
    Dialogs::GladeDialog<Dialogs::BurnProgress>::create(*m_parent_win, "diag_burn_progress");
  m_pBurnProgress->signal_cancel_clicked().connect(sigc::mem_fun(*this, &BurnDisc::on_cancel_clicked));
}

BurnDisc::~BurnDisc()
{}

bool BurnDisc::start_burn()
{
  using namespace Bacon;

  m_pBurnProgress->show();

  CD_RecorderWriteFlags flags;
  GList *tracks = 0;
  Glib::ustring str;
  cancel = CANCEL_MAKE_ISO;

  bool result = true;
  std::string iso_filename = "";

  if(m_filename != "" && m_pDocument != 0)
  {
    iso_filename = Glib::build_filename(Glib::get_tmp_dir(), "image.iso.XXXXXX");
    int fd = Glib::mkstemp(iso_filename);
    close(fd);

    mkiso = SharedPtr<MakeIso>(new MakeIso(iso_filename, m_pDocument->get_root_node(), m_DataProperties));

    sigc::connection set_prog_conn =
      mkiso->signal_progress_changed().connect(sigc::mem_fun(*this, &BurnDisc::on_progress_changed));
    sigc::connection set_text_conn =
      mkiso->signal_text_changed().connect(sigc::mem_fun(*this, &BurnDisc::on_text_changed));
  
    result = mkiso->make_iso();

    set_prog_conn.disconnect();
    set_text_conn.disconnect();
  }
  else
  {
    iso_filename = m_filename;
  }
  
  if(!result)
  {
    return false;
  }
  else
  {
    Track* track;

    m_refCD_Recorder = CD_Recorder::create();
    cancel = CANCEL_CD_RECORD;

    m_refCD_Recorder->signal_progress_changed().connect(sigc::mem_fun(*this, &BurnDisc::on_progress_changed));
    m_refCD_Recorder->signal_action_changed().connect(sigc::mem_fun(*this, &BurnDisc::on_action_changed));

    track = g_new0(Track, 1);
    track->type = TRACK_TYPE_DATA;
    track->contents.data.filename = g_strdup(const_cast<char*>(iso_filename.c_str()));
    tracks = g_list_prepend(tracks, track);

    flags = (CD_RecorderWriteFlags)(0);
    if(m_bEject_CD)
    {
      flags |= CD_RECORDER_EJECT;
    }
    if(m_bDummy)
    {
      flags |= CD_RECORDER_DUMMY_WRITE;
    }

    Glib::RefPtr<Gnome::Conf::Client> client = Gnome::Conf::Client::get_default_client();

    if(client->get_bool("/apps/nautilus-cd-burner/burnproof"))
    {
      flags |= CD_RECORDER_BURNPROOF;
    }
    if(client->get_bool("/apps/nautilus-cd-burner/overburn"))
    {
      flags |= CD_RECORDER_OVERBURN;
    }
    if(debug())
    {
      flags |= CD_RECORDER_DEBUG;
    }

    m_refCD_Recorder->write_tracks(m_pCDDrive, tracks, m_dSpeed, flags);

    Gnome::Vfs::Handle::unlink (iso_filename);

    cancel = CANCEL_NONE;

    g_list_foreach(tracks, (GFunc)cd_recorder_track_free, NULL);
    g_list_free(tracks);
  }
    
  return true;
}

void BurnDisc::on_cancel_clicked()
{
  if(cancel == CANCEL_NONE)
  {
    return;
  }

  if(cancel == CANCEL_MAKE_ISO)
  {
    mkiso->cancel();
    return;
  }
  
  if(m_refCD_Recorder->cancel(true) == false)
  {
    m_refCD_Recorder->cancel(false);
  }
}

void BurnDisc::on_progress_changed(double fraction)
{
  m_pBurnProgress->set_progress(fraction);
}

void BurnDisc::on_text_changed(const Glib::ustring& text)
{
  m_pBurnProgress->set_text(text);
  instmgr->set_status_tip(text);
}

void BurnDisc::on_action_changed(Bacon::CD_RecorderActions action,
                                 Bacon::CD_RecorderMedia media)
{
  using namespace Bacon;
  Glib::ustring text;
  Glib::ustring media_str;
  
  switch(action)
  {
    case CD_PREPARING_WRITE:
      text = _("Preparing to write ");
      break;
    case CD_WRITING:
      text = _("Writing ");
      break;
    case CD_FIXATING:
      text = _("Fixating ");
      break;
    case CD_BLANKING:
      text = _("Erasing ");
      break;
    default:
      g_warning ("Unhandled action in on_action_changed");
      break;
  }

  switch(media)
  {
    case CD_MEDIA_CD:
      media_str = _("CD");
      break;
    case CD_MEDIA_DVD:
      media_str = _("DVD");
      break;
    default:
      g_warning ("Unhandled media in on_action_changed");
      break;
  }

  m_pBurnProgress->set_text(text + media_str);
  instmgr->set_status_tip(text + media_str);
}

} // namespace IO

} // namespace Coaster
