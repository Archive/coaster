/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "widgets/stock-label-button.h"

#include <gtkmm/alignment.h>
#include <gtkmm/box.h>
#include <gtkmm/image.h>
#include <gtkmm/label.h>

namespace Coaster
{

namespace Widgets
{

SLButton::SLButton(const Gtk::StockID& stock_id,
                   const Glib::ustring& text)
: Gtk::Button(), m_Alignment(0), m_HBox(0), m_Image(0), m_Label(0)
{
  using namespace Gtk;
  m_Alignment = new Alignment(0.5, 0.5, 0, 0);
  m_HBox = new HBox(false, 2);
  m_Image = new Image(stock_id, ICON_SIZE_BUTTON);
  m_Label = new Label(text, true /* use mnemonic */);

  m_HBox->pack_start(*manage(m_Image), false, false, 0);
  m_HBox->pack_end(*manage(m_Label), false, false, 0);

  m_Alignment->add(*manage(m_HBox));

  add(*manage(m_Alignment));
  property_can_default() = true;
  show_all();
}

SLButton::~SLButton()
{}

} // namespace Widgets

} // namespace Coaster
