/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "dialogs/properties.h"
#include "cstr-debug.h"

#include <gtkmm/entry.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/radiobutton.h>

#include <libglademm/xml.h>

namespace Coaster
{

namespace Dialogs
{

Properties::Properties(BaseObjectType* cobject,
                       const Glib::RefPtr<Gnome::Glade::Xml>& refXml)
: Gtk::Dialog(cobject),
  m_pTitle(0), m_pAppendDate(0), m_pCatNum(0), m_pPublisher(0), m_pAuthor(0),
  m_pISOLevel1(0), m_pISOLevel2(0),
  m_pJoliet(0), m_pRockRidge(0),
  m_pCDROM(0), m_pCDI(0), m_pCDXA(0), m_pOK(0)
{
  refXml->get_widget("prop_entry_title",  m_pTitle);
  refXml->get_widget("prop_check_append_date", m_pAppendDate);
  refXml->get_widget("prop_entry_catnum", m_pCatNum);
  refXml->get_widget("prop_entry_publisher", m_pPublisher);
  refXml->get_widget("prop_entry_author", m_pAuthor);
  refXml->get_widget("prop_radio_iso_level1", m_pISOLevel1);
  refXml->get_widget("prop_radio_iso_level2", m_pISOLevel2);
  refXml->get_widget("prop_check_joliet", m_pJoliet);
  refXml->get_widget("prop_check_rr", m_pRockRidge);
  refXml->get_widget("prop_radio_session_cdrom", m_pCDROM);
  refXml->get_widget("prop_radio_session_cdi",   m_pCDI);
  refXml->get_widget("prop_radio_session_cdxa",  m_pCDXA);
  refXml->get_widget("prop_button_ok", m_pOK);

  m_pOK->set_sensitive(false);

  m_pTitle->grab_focus();

  set_default_response(Gtk::RESPONSE_OK);
}

Properties::~Properties()
{
}

void Properties::init_signals()
{
  m_pTitle->signal_changed().connect(sigc::bind(sigc::mem_fun(*this, &Properties::on_anything_changed), "title"));
  m_pAppendDate->signal_toggled().connect(sigc::bind(sigc::mem_fun(*this, &Properties::on_anything_changed), "append_date"));
  m_pCatNum->signal_changed().connect(sigc::bind(sigc::mem_fun(*this, &Properties::on_anything_changed), "catnum"));
  m_pPublisher->signal_changed().connect(sigc::bind(sigc::mem_fun(*this, &Properties::on_anything_changed), "pub"));
  m_pAuthor->signal_changed().connect(sigc::bind(sigc::mem_fun(*this, &Properties::on_anything_changed), "author"));
  m_pISOLevel1->signal_toggled().connect(sigc::bind(sigc::mem_fun(*this, &Properties::on_anything_changed), "iso1"));
  m_pISOLevel2->signal_toggled().connect(sigc::bind(sigc::mem_fun(*this, &Properties::on_anything_changed), "iso2"));
  m_pJoliet->signal_toggled().connect(sigc::bind(sigc::mem_fun(*this, &Properties::on_anything_changed), "joliet"));
  m_pRockRidge->signal_toggled().connect(sigc::bind(sigc::mem_fun(*this, &Properties::on_anything_changed), "rr"));
  m_pCDROM->signal_toggled().connect(sigc::bind(sigc::mem_fun(*this, &Properties::on_anything_changed), "cdrom"));
  m_pCDI->signal_toggled().connect(sigc::bind(sigc::mem_fun(*this, &Properties::on_anything_changed), "cdi"));
  m_pCDXA->signal_toggled().connect(sigc::bind(sigc::mem_fun(*this, &Properties::on_anything_changed), "cdxa"));
}

void Properties::init(const Data::Properties& dp)
{
  m_pTitle->set_text(dp.title);
  if(dp.append_date)
    m_pAppendDate->set_active();
  m_pCatNum->set_text(dp.catnum);
  m_pPublisher->set_text(dp.publisher);
  m_pAuthor->set_text(dp.author);

  switch(dp.isolevel)
  {
    case COASTER_ISO_LEVEL1:
      m_pISOLevel1->set_active();
      break;
    case COASTER_ISO_LEVEL2:
      m_pISOLevel2->set_active();
      break;
  }

  if(dp.joliet)
    m_pJoliet->set_active();

  if(dp.rockridge)
    m_pRockRidge->set_active();

  switch(dp.sessionformat)
  {
    case COASTER_SESSION_FMT_CDROM:
      m_pCDROM->set_active();
      break;
    case COASTER_SESSION_FMT_CDI:
      m_pCDI->set_active();
      break;
    case COASTER_SESSION_FMT_CDXA:
      m_pCDXA->set_active();
      break;
  }

  init_signals();
}

void Properties::on_anything_changed(const Glib::ustring& str)
{
  debug(str + "changed");
  m_pOK->set_sensitive(true);
}

Data::Properties Properties::get_properties() const
{
  Data::Properties dp;

  dp.title = m_pTitle->get_text();
  dp.append_date = m_pAppendDate->get_active();
  dp.catnum = m_pCatNum->get_text();
  dp.publisher = m_pPublisher->get_text();
  dp.author = m_pAuthor->get_text();

  if(m_pISOLevel1->get_active())
    dp.isolevel = COASTER_ISO_LEVEL1;
  else
    dp.isolevel = COASTER_ISO_LEVEL2;

  dp.joliet = m_pJoliet->get_active();
  dp.rockridge = m_pRockRidge->get_active();

  if(m_pCDXA->get_active())
    dp.sessionformat = COASTER_SESSION_FMT_CDXA;
  else if(m_pCDI->get_active())
    dp.sessionformat = COASTER_SESSION_FMT_CDI;
  else
    dp.sessionformat = COASTER_SESSION_FMT_CDROM;

  return dp;
}

} // namespace Dialogs

} // namespace Coaster
