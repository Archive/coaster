/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "widgets/combo-speed.h"
#include "ucompose.h"

#include <gtkmm/liststore.h>
#include <gtkmm/cellrenderertext.h>

#include <gconfmm/client.h>

namespace Coaster
{

namespace Widgets
{

ComboSpeed::ComboSpeed()
: Gtk::ComboBox()
{
  ComboColumns columns;
  m_refListModel = Gtk::ListStore::create(columns);

  set_model(m_refListModel);

  Gtk::CellRendererText *const cell_name = new Gtk::CellRendererText();
  pack_start(*manage(cell_name), true);
  add_attribute(cell_name->property_text(), columns.m_col_speed);

  Gtk::TreeIter iter = m_refListModel->append();
  (*iter).set_value(columns.m_col_speed, Glib::ustring("Maximum"));
  (*iter).set_value(columns.m_col_real_speed, 0);
}

ComboSpeed::~ComboSpeed()
{}

int ComboSpeed::get_selected_real_speed() const
{
  ComboColumns columns;
  return (*(get_active())).get_value(columns.m_col_real_speed);
}

void ComboSpeed::populate_speed(int max_speed)
{
  ComboColumns columns;

  m_refListModel->clear();

  Gtk::TreeIter iter = m_refListModel->append();
  (*iter).set_value(columns.m_col_speed, Glib::ustring("Maximum"));
  (*iter).set_value(columns.m_col_real_speed, 0);

  for(int i = 1 ; i <= max_speed ; i++)
  {
    iter = m_refListModel->append();
    (*iter).set_value(columns.m_col_speed, String::ucompose("%1X", i));
    (*iter).set_value(columns.m_col_real_speed, (i * 150));
  }

  Gtk::TreeIter max_iter = m_refListModel->children().begin();
  set_active(max_iter);
}

} // namespace Widgets

} // namespace Coaster
