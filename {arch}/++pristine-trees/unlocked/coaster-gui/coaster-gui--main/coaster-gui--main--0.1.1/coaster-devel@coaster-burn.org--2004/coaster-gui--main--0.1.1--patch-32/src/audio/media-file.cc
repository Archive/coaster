#include "media-file.h"
#include "ogg-helper.h"

#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>

#include <id3/tag.h>

#include <sstream>

namespace // Anonymous namespace
{

ov_callbacks file_info_callbacks =
{
	ogg_helper_read,
	ogg_helper_seek,
	ogg_helper_close_dummy,
	ogg_helper_tell
};

void get_vorbis_info(Gnome::Vfs::Handle &handle, Glib::ustring &title, Glib::ustring &duration, double &duration_secs)
{
  OggVorbis_File vf;
  vorbis_comment *c;
  int ov_callback_return_value, total_time;
  std::vector<Glib::ustring> returnValues;
  Glib::ustring mime_type = handle.get_file_info(COMMON_INFO_OPTIONS)->get_mime_type();

  if(mime_type == "application/ogg" || mime_type == "application/x-ogg")
  {
    ov_callback_return_value = ov_open_callbacks(handle.gobj(), &vf, NULL, 0, file_info_callbacks);
    total_time = ov_time_total(&vf, -1);

    if(total_time)
    {
      int seconds = (int)(total_time % 60);
      std::ostringstream output;
      output.imbue(std::locale(""));
      output << (int)(total_time / 60) << ":" << ((seconds < 10) ? "0" : "") << seconds;

      c = ov_comment(&vf, -1);
      char *cc = vorbis_comment_query(c, "title", 0);
      if(cc)
        title = Glib::ustring(cc);
      else
        title = "";
      duration = Glib::locale_to_utf8(output.str());
      duration_secs = total_time;
    }
    // Deallocate the memory used. (Stupid C ;)
    ov_clear(&vf);
  }
}

void get_mp3_info(Gnome::Vfs::Handle &handle, Glib::ustring uri, Glib::ustring &title, Glib::ustring &duration, double &duration_secs)
{
  ID3_Tag myTag(uri.c_str());
  ID3_Frame *myFrame = myTag.Find(ID3FID_TITLE);
  if(myFrame)
  {
    char title_temp[4000];
    ID3_Field *myField = myFrame->GetField(ID3FN_TEXT);
    myField->Get(title_temp, 4000);
    title = Glib::ustring(title_temp);
  }
  duration_secs = 0.0;
  duration = "???";
}

}

namespace Coaster
{

MediaFile::MediaFile(const Glib::ustring& uri) : m_uri(uri)
{
  using namespace Gnome::Vfs;

  m_handle.open(m_uri, OPEN_READ);
  
  Glib::RefPtr< FileInfo > info = m_handle.get_file_info(COMMON_INFO_OPTIONS);

  m_mime_type = info->get_mime_type();
  m_info_name = info->get_name();

  if(m_mime_type == "application/ogg" || m_mime_type == "application/x-ogg")
  {
    m_MediaType = COASTER_MEDIA_OGG;
    get_vorbis_info(m_handle, m_title, m_duration_string, m_duration_secs);
  }
  else if(m_mime_type == "audio/mpeg" || m_mime_type == "audio/x-mp3")
  {
    m_MediaType = COASTER_MEDIA_MP3;
    get_mp3_info(m_handle, m_uri, m_title, m_duration_string, m_duration_secs);
  }
  else if(m_mime_type == "application/x-flac")
    m_MediaType = COASTER_MEDIA_FLAC;
  else if(m_mime_type == "application/x-wav")
    m_MediaType = COASTER_MEDIA_WAV;
  else
    m_MediaType = COASTER_MEDIA_NOT_ADDABLE;
}

MediaFile::~MediaFile()
{
}

bool MediaFile::is_valid()
{
  bool result = false;
 
  switch(m_MediaType)
  {
    case COASTER_MEDIA_OGG:
    case COASTER_MEDIA_MP3:
      result = true;
      break;
    case COASTER_MEDIA_FLAC:
    case COASTER_MEDIA_WAV:
    default:
      break;
  }

  return result;
}

Glib::ustring MediaFile::get_info_name()
{
  return m_info_name;
}

Glib::ustring MediaFile::get_title()
{
  return m_title;
}

Glib::ustring MediaFile::get_duration_string()
{
  return m_duration_string;
}

double MediaFile::get_duration_secs()
{
  return m_duration_secs;
}
  
} // namespace Coaster
