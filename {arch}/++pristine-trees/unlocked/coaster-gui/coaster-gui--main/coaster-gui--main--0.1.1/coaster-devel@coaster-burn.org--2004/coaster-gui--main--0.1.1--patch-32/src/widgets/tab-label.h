/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_TAB_LABEL_
#define _COASTER_TAB_LABEL_

#include <gtkmm/box.h>
#include <gtkmm/tooltips.h>

namespace Gtk
{
class Image;
class Button;
}

namespace Coaster
{

class TabLabel : public Gtk::HBox
{
public:
  explicit TabLabel(const Gtk::StockID& stock_id);
  virtual ~TabLabel();

  Glib::SignalProxy0<void> signal_button_clicked();
  Gtk::Widget* get_button();

  static TabLabel* create(const Gtk::StockID& stock_id, sigc::slot<void> clicked_slot);
  
protected:
  virtual void on_button_state_changed(Gtk::StateType type);

private:
  //Gtk::Label *m_pLabel;
  Gtk::Image  *m_pImage;
  Gtk::Button *m_pButton;
};

} // namespace Coaster

#endif // _COASTER_TAB_LABEL_
