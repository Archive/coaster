/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "nautilus-make-iso.h"

#include "cstr-debug.h"
#include "cstr-intl.h"
#include "ucompose.h"

#include "data/data-columns.h"

#include <glibmm/iochannel.h>
#include <glibmm/date.h>
#include <glibmm/spawn.h>
#include <glibmm/exception.h>

#include <gtkmm/treestore.h>

#include <libgnomevfsmm/handle.h>

#include <libxml++/nodes/element.h>

#include <iostream>
#include <vector>
#include <ext/stdio_filebuf.h>

#include <signal.h>

namespace Coaster
{

namespace IO
{

MakeIso::MakeIso(const std::string& iso_filename,
                 const xmlpp::Element* rootNode,
                 const Data::Properties& dp)
: m_pRootNode(const_cast<xmlpp::Element*>(rootNode)),
  m_DataProperties(dp), pid(0), tmp_dir(Glib::get_tmp_dir()),
  iso_filename(iso_filename)
{
  graft_filename = Glib::build_filename(tmp_dir, "image.graft_file.XXXXXX");
  fd_graft = Glib::mkstemp(graft_filename);
  
  __gnu_cxx::stdio_filebuf<char> buffer(fd_graft,
                                        std::ios::out,
#ifndef COASTER_NEW_STDIO_FILEBUF
                                        false,
#endif
                                        static_cast<size_t>(BUFSIZ));
  std::ostream graft_file_stream(&buffer);

  if(!graft_file_stream)
  {
    std::cerr << "can't open output file \"" << graft_filename << "\"" << std::endl;
  }

  parse_layout_to_graft_file(graft_file_stream);

  graft_file_stream.flush();
  buffer.close();
  close(fd_graft);
}

MakeIso::~MakeIso()
{}

MakeIso::type_signal_progress_changed& MakeIso::signal_progress_changed()
{
  return m_signal_progress_changed_;
}

MakeIso::type_signal_text_changed& MakeIso::signal_text_changed()
{
  return m_signal_text_changed_;
}

unsigned long MakeIso::get_iso_filesize(int& exit_status)
{
  std::string stdout_data, stderr_data;
  std::vector<Glib::ustring> argv;
  GError* error;

  error = NULL;

  argv.push_back("mkisofs");
  if (m_DataProperties.rockridge)
	  argv.push_back("-r");
	if (m_DataProperties.joliet)
		argv.push_back("-J");
	/* Undocumented -input-charset option */
	argv.push_back("-input-charset");
	argv.push_back("utf8");
	argv.push_back("-q");
	argv.push_back("-graft-points");
	argv.push_back("-path-list");
	argv.push_back(graft_filename);
	argv.push_back("-print-size");

  if(Coaster::debug())
  {
    std::cout << "launching command: ";
    for(std::vector<Glib::ustring>::iterator iter = argv.begin() ; iter != argv.end() ; ++iter)
    {
      std::cout << String::ucompose("%1 ", *iter);
    }
    std::cout << std::endl;
  }

  try
  {
    Glib::spawn_sync(tmp_dir,
                     argv,
                     Glib::SPAWN_SEARCH_PATH, 
                     sigc::slot<void>(), 
                     &stdout_data, 
                     &stderr_data, 
                     &exit_status);
  }
  catch(Glib::Exception& e)
  {
    g_warning("mkisofs command failed: %s\n", e.what().c_str());
    return false;
  }

  return atol (stdout_data.c_str()); /* blocks of 2048 bytes */
}

bool MakeIso::make_iso()
{
  int stdout_pipe, stderr_pipe;
  int exit_status;
  unsigned long iso_size;

  bool use_joliet = m_DataProperties.joliet;

	iso_size = get_iso_filesize(exit_status);

	/*if (exit_status != 0 && use_joliet) {
		if (strstr (stderr_data.c_str(), "Joliet tree sort failed.") != NULL) {
			if (ask_disable_joliet (cd_progress_get_window ())) {
				use_joliet = false;
				goto retry;
			} else {
				mkisofs_result = RESULT_ERROR;
				goto cleanup;
			}
		}
	}*/


  // Check for free space
  Gnome::Vfs::FileSize vol_free_space = get_free_space_on_tmp();
  if(vol_free_space < iso_size)
  {
    // code to ask for a different temporary filesystem
    return false;
  }

  // code to actually make the ISO
  std::vector<Glib::ustring> argv;
	argv.push_back("mkisofs");
  if(m_DataProperties.rockridge)
  	argv.push_back("-r");
	if (use_joliet)
		argv.push_back("-J");
	argv.push_back("-graft-points");
	argv.push_back("-path-list");
	argv.push_back(graft_filename);
  
  
  if(m_DataProperties.append_date)
  {
    Glib::Date now;
    now.set_time(time(NULL));

		argv.push_back("-V");
    if(m_DataProperties.title == "")
      argv.push_back(now.format_string("%b %d, %Y"));
    else
      argv.push_back(m_DataProperties.title + now.format_string(" - %b %d, %Y"));
  }
  else
  {
    if(m_DataProperties.title != "")
    {
      argv.push_back("-V");
      argv.push_back(m_DataProperties.title);
    }
  }

	argv.push_back("-o");
	argv.push_back(iso_filename);

  if(Coaster::debug())
  {
    std::cout << "launching command: ";
    for(std::vector<Glib::ustring>::iterator iter = argv.begin() ; iter != argv.end() ; ++iter)
    {
      std::cout << String::ucompose("%1 ", *iter);
    }
    std::cout << std::endl;
  }

	m_signal_text_changed_.emit(_("Creating CD image"));
	m_signal_progress_changed_.emit(0.0);

	//cd_progress_set_image_spinning (TRUE);
  GError* error;
	error = NULL;
  try
  {
    Glib::spawn_async_with_pipes(Glib::get_tmp_dir(),
                                 argv,
                                 Glib::SPAWN_SEARCH_PATH,
                                 sigc::slot<void>(),
                                 &pid, 0, 
                                 &stdout_pipe, &stderr_pipe);
  }
  catch(Glib::Exception& e)
  {
    g_warning ("mkisofs command failed: %s\n", e.what().c_str());
    return false;
  }
	main_loop = Glib::MainLoop::create(false);

  stdout_channel = Glib::IOChannel::create_from_fd(stdout_pipe);
  stdout_channel->set_encoding();

  sigc::connection stdout_conn = Glib::signal_io().connect(sigc::mem_fun(*this, &MakeIso::stdout_read), stdout_channel, Glib::IO_IN | Glib::IO_HUP | Glib::IO_ERR);

  stderr_channel = Glib::IOChannel::create_from_fd(stderr_pipe);
  stderr_channel->set_encoding();

  sigc::connection stderr_conn = Glib::signal_io().connect(sigc::mem_fun(*this, &MakeIso::stderr_read), stderr_channel, Glib::IO_IN | Glib::IO_HUP | Glib::IO_ERR);

  main_loop->run();

  stderr_conn.disconnect();
  stdout_conn.disconnect();

  Gnome::Vfs::Handle::unlink(graft_filename);

  return mkisofs_result;
}

void MakeIso::cancel()
{
  if(main_loop)
  {
    kill(pid, SIGINT);
    Gnome::Vfs::Handle::unlink(iso_filename);
    main_loop->quit();
    mkisofs_result = false;
  }
}

void MakeIso::parse_layout_to_graft_file(std::ostream& filestream,
                                         const xmlpp::Element* parent,
                                         const Glib::ustring& prefix)
{
  using namespace Gtk;

  xmlpp::Node::NodeList children;
  if(parent == 0)
    children = m_pRootNode->get_children();
  else
    children = parent->get_children();

  if(!children.empty())
  {
    for(xmlpp::Node::NodeList::iterator i = children.begin() ; i != children.end() ; ++i)
    {
      xmlpp::Element* current_elem = static_cast<xmlpp::Element*>(*i);

      Glib::ustring name = current_elem->get_attribute("name")->get_value();

      if(current_elem->get_name() == "data")
      {
        Glib::ustring uri = current_elem->get_attribute("path")->get_value();

        if(prefix.empty())
        {
          // output "name=uri" in file
          filestream << String::ucompose("%1=%2", name, uri) << std::endl;
          Coaster::debug(String::ucompose("%1=%2", name, uri));
        }
        else
        {
          // output "prefix/name=uri" in file
          filestream << String::ucompose("%1/%2=%3", prefix, name, uri) << std::endl;
          Coaster::debug(String::ucompose("%1/%2=%3", prefix, name, uri));
        }
      }
      else
      {
        if(!prefix.empty())
        {
          Glib::ustring new_prefix = String::ucompose("%1/%2", prefix, name);
          parse_layout_to_graft_file(filestream, current_elem, new_prefix);
        }
        else
        {
          parse_layout_to_graft_file(filestream, current_elem, name);
        }
      }
    }
  }
}

Gnome::Vfs::FileSize MakeIso::get_free_space_on_tmp()
{
  Glib::RefPtr<Gnome::Vfs::Uri> uri = Gnome::Vfs::Uri::create(tmp_dir);
  return uri->get_volume_free_space();
}

bool MakeIso::stdout_read(Glib::IOCondition condition)
{
  Glib::ustring line;
  Glib::IOStatus status;

  status = stdout_channel->read_line(line);

  if(line != "")
  {
    debug("make_iso stdout: ",line);
  }

  return true;
}

bool MakeIso::stderr_read(Glib::IOCondition condition)
{
  Glib::ustring line;
  char fraction_str[7];
  double fraction;
  Glib::IOStatus status;

  status = stderr_channel->read_line(line);

  if(line != "")
  {
    debug("make_iso stderr: ", line);
  }

  if(status == Glib::IO_STATUS_NORMAL)
  {
    if(strncmp(line.c_str(), "Total translation table size", 28) == 0)
    {
      m_signal_progress_changed_.emit(1.0);
      main_loop->quit();
      mkisofs_result = true;
    }

    if(strstr(line.c_str(), "estimate finish"))
    {
      if(sscanf(line.c_str(), "%6c%%done, estimate finish", fraction_str) == 1)
      {
        fraction_str[6] = 0;
        fraction = g_strtod(fraction_str, NULL);
        m_signal_progress_changed_.emit(fraction/100.0);
      }
    }
  }

  return true;
}

} // namespace IO

} // namespace Coaster
