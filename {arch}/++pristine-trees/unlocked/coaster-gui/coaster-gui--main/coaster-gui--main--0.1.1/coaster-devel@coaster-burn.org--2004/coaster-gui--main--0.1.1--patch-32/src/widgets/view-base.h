/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_VIEW_BASE_H_
#define _COASTER_VIEW_BASE_H_

#include <gtkmm/box.h>

#include "document.h"
#include "sharedptr.h"

namespace Gtk
{
class Menu;
class ActionGroup;
} // namespace Gtk

namespace Coaster
{

class InstanceMgr;
class UndoManager;

namespace Widgets
{

class View_Base : public Gtk::VBox,
                  public Bakery::View<Document>
{
public:
  View_Base();
  virtual ~View_Base();

  typedef View_Base type_base;;

  // Member Methods
  virtual Glib::ustring get_ui_location() const;
  virtual Glib::RefPtr<Gtk::ActionGroup> get_ui_action_group() const;
  
  virtual void set_popup_menu(Gtk::Menu* popup_menu);
  virtual void popup(guint button,
                     guint32 activate_time);

  // overrides
  /*virtual void set_document(Document* pDocument);
  virtual void set_document(AudioDoc* pDocument);
  virtual void set_document(VideoDoc* pDocument);
  virtual void load_from_document();*/

protected:
  // Override these:
  virtual void on_burn_clicked();
  virtual void on_layout_modified(bool modified);
  virtual void on_layout_selection_changed(int number_selected);

  virtual void on_undo_manager_changed();

  virtual void enable_selection_actions(bool enable = true);
  virtual void enable_layout_item_actions(bool enable = true);

  SharedPtr<InstanceMgr> instmgr;
  SharedPtr<UndoManager> undo_manager;
  
  Gtk::Menu* m_pMenu_Popup;
  Glib::RefPtr<Gtk::ActionGroup> m_refViewActionGroup;
};

} // namespace Widgets

} // namespace Coaster

#endif // _COASTER_VIEW_BASE_H_
