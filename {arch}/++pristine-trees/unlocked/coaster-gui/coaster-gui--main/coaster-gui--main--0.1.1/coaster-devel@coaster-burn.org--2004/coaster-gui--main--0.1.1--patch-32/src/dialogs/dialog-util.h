/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_DIALOG_UTIL_H_
#define _COASTER_DIALOG_UTIL_H_

#include <glibmm/ustring.h>
#include <gtkmm/window.h>
#include <bakery/App/App_WithDoc.h>

namespace Coaster
{

typedef enum
{
  COASTER_RESPONSE_CANCEL,
  COASTER_RESPONSE_AUDIO,
  COASTER_RESPONSE_DATA,
  COASTER_RESPONSE_VIDEO,
  COASTER_RESPONSE_YES,
  COASTER_RESPONSE_NO,
  COASTER_RESPONSE_OK
} DiagResponse;

namespace Dialogs
{

typedef std::list<Glib::ustring> type_listUStrings;

type_listUStrings offer_add_files(Gtk::Window& parent,
                                  bool& hidden,
                                  int& response);

Glib::ustring offer_add_folder(Gtk::Window& parent,
                               bool& hidden,
                               bool& recursive,
                               int& response);

Bakery::App_WithDoc::enumSaveChanges ui_offer_to_save_changes(Gtk::Window& parent,
                                                              const std::string& filepath);

Glib::ustring ui_offer_open_files(Gtk::Window& parent);
void ui_offer_open_iso(Gtk::Window& parent, int& response, std::string& filename);
void ui_offer_export_iso(Gtk::Window& parent, int& response, std::string& filename);

Glib::ustring ui_find(Gtk::Window& parent);

int ui_warn_title_check(Gtk::Window& parent);
int ui_warn_remove_files(Gtk::Window& parent);

void ui_error_add_files(Gtk::Window& parent,
                        const std::list<Glib::ustring>& files);

} // namespace Dialogs

} // namespace Coaster

#endif // _COASTER_DIALOG_UTIL_H_
