/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_INSTANCE_MANAGER_H_
#define _COASTER_INSTANCE_MANAGER_H_

#include "sharedptr.h"
#include "client.h"

#include <bakery/App/App_WithDoc.h>

#include <gtkmm/treerowreference.h>
#include <gconfmm/entry.h>

#ifdef COASTER_HAS_HASH_MAP
#include <ext/hash_map>
#else
#include <map>
#endif

namespace Gdk
{
class Pixbuf;
} // namespace Gdk

namespace Gtk
{
class Window;
class IconTheme;
class Clipboard;
class TreeStore;
} // namespace Gtk

namespace Gnome
{

namespace Vfs
{
class VolumeMonitor;
} // namespace Vfs

} // namespace Gnome

#ifdef COASTER_HAS_HASH_MAP
namespace __gnu_cxx
{

using std::size_t;
template<> struct hash<const Glib::ustring>
{
  size_t operator()(const Glib::ustring& __s) const { return __stl_hash_string(__s.c_str()); }
};

} // namespace __gnu_cxx
#endif

namespace Coaster
{

extern const Glib::ustring GCONF_READ_SPEED;
extern const Glib::ustring GCONF_WRITE_SPEED;
extern const Glib::ustring GCONF_OPTICAL_READER_DEV;
extern const Glib::ustring GCONF_OPTICAL_WRITER_DEV;
extern const Glib::ustring GCONF_EJECT_ON_FINISH;

#ifdef COASTER_HAS_HASH_MAP
struct equstr
{
  bool operator()(const Glib::ustring& a,
                  const Glib::ustring& b) const
  {
    return a.collate_key() == b.collate_key();
  }
};
#endif

namespace Widgets
{
class CDTrayIcon;
}

class InstanceMgr : public SharedObject
{
public:
  InstanceMgr();
  ~InstanceMgr();

#ifdef COASTER_HAS_HASH_MAP
  typedef __gnu_cxx::hash_map<const Glib::ustring,
                              Glib::RefPtr<Gdk::Pixbuf>,
                              __gnu_cxx::hash<const Glib::ustring>,
                              equstr> type_str_pix_hash;
#else
  typedef std::map<const Glib::ustring,
                   Glib::RefPtr<Gdk::Pixbuf> > type_str_pix_hash;
#endif

  // Static methods
  static SharedPtr<InstanceMgr> instance();

  // Initialization
  void init();

  // instance manager signals
  typedef sigc::signal<void,bool> type_signal_sensitive;
  typedef sigc::signal<void>      type_signal_icon_theme_changed;

  type_signal_sensitive& signal_sensitive();
  type_signal_icon_theme_changed& signal_icon_theme_changed();

  void set_sensitive(bool sensitive = true);

  // Icon Theme
  Glib::RefPtr<Gtk::IconTheme> m_refIconTheme;
  bool lookup_icon(const Glib::ustring& mime_type, 
                   const Gtk::TreeRowReference& rowref,
                   const Glib::RefPtr<Gtk::TreeStore>& treestore);
  Glib::RefPtr<Gdk::Pixbuf> lookup_icon(const Glib::ustring& mime_type);
  void on_icon_theme_changed();

  // Clipboard
  Glib::RefPtr<Gtk::Clipboard> m_refClipboard;

  // Volume Monitor
  Glib::RefPtr<Gnome::Vfs::VolumeMonitor> m_refVolumeMonitor;

  // GConf
  Client m_Client;

  // StatusIcon
  void create_status_icon(const Glib::RefPtr<Gdk::Pixbuf>& pixbuf);
  void set_status_pixbuf(const Glib::RefPtr<Gdk::Pixbuf>& pixbuf);
  void set_status_tip(const Glib::ustring& tip,
                      const Glib::ustring& private_tip = Glib::ustring());
  void remove_status_icon();

private:

  // Icon cache
  type_str_pix_hash m_hash_StrPixbuf;

  // sensitive signal
  type_signal_sensitive m_signal_sensitive_;
  type_signal_icon_theme_changed m_signal_icon_theme_changed_;

  // Status Icon
  Widgets::CDTrayIcon* m_status_icon;

  // Internal lookup function
  Glib::RefPtr<Gdk::Pixbuf> gtk_lookup_icon(const Glib::ustring& mime_type,
                                            bool check_map = true);
};

typedef SharedPtr<InstanceMgr> InstanceMgrPtr;

} // namespace Coaster

#endif // _COASTER_INSTANCE_MANAGER_H_
