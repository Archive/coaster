/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_ERROR_ADDING_FILES_H_
#define _COASTER_ERROR_ADDING_FILES_H_

#include <gtkmm/dialog.h>
#include <list>

namespace Gtk
{
class Label;
} // namespace Gtk

namespace Gnome
{
namespace Glade
{
class Xml;
} // namespace Glade
} // namespace Gnome

namespace Coaster
{

namespace Dialogs
{

class ErrorAddingFiles : public Gtk::Dialog
{
public:
  ErrorAddingFiles(BaseObjectType* cobject,
                   const Glib::RefPtr<Gnome::Glade::Xml>& refXml);
  virtual ~ErrorAddingFiles();

  typedef std::list<Glib::ustring> type_listUStrings;

  void set_files(const type_listUStrings& files);
private:
  Gtk::Label* m_pErrAdd_Label;
};

} // namespace Dialogs

} // namespace Coaster

#endif // _COASTER_ERROR_ADDING_FILES_H_
