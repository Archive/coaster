## $Id$
##
## Copyright (c) 2002  Daniel Elstner  <daniel.elstner@gmx.net>
## Modified to fit Coaster by Bryan Forbes  <bryan@reigndropsfall.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License VERSION 2 as
## published by the Free Software Foundation.  You are not allowed to
## use any other version of the license; unless you got the explicit
## permission from the author to do so.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

SUBDIRS = 

if NAUTILUS_BACKEND
SUBDIRS += baconmm
endif

SUBDIRS += eggmm widgets dialogs backends data

DIST_SUBDIRS = baconmm eggmm widgets dialogs backends data

bin_PROGRAMS = coaster

sources_h  = \
	client.h							\
	cstr-debug.h					\
	cstr-strings.h				\
	cstr-types.h					\
	document.h						\
	edit-ops.h						\
	instance-manager.h		\
	mainwindow.h					\
	sharedptr.h						\
	sound.h								\
	stock-registration.h	\
	undo-manager.h				\
	undo-op.h							\
	util.h								\
	xml-io.h							\
	cstr-enums.h					\
	cstr-intl.h						\
	ucompose.h

sources_cc = \
	client.cc							\
	cstr-debug.cc					\
	cstr-strings.cc				\
	document.cc						\
	edit-ops.cc						\
	instance-manager.cc		\
	main.cc								\
	mainwindow.cc					\
	sharedptr.cc					\
	sound.cc							\
	stock-registration.cc	\
	undo-manager.cc				\
	undo-op.cc						\
	util.cc								\
	xml-io.cc

coaster_SOURCES = $(sources_h) $(sources_cc)

DEFS = @DEFS@ \
	$(coaster_backend_defines) \
	$(coaster_hash_map_define) \
	-DG_DISABLE_DEPRECATED \
	-DGDK_DISABLE_DEPRECATED \
	-DGDK_PIXBUF_DISABLE_DEPRECATED \
	-DGTK_DISABLE_DEPRECATED

gladedir = $(datadir)/coaster/glade
dtddir   = $(datadir)/coaster/dtd
uidir		 = $(datadir)/coaster/ui

INCLUDES = \
	-I$(srcdir) \
	-I$(top_srcdir) \
	-I$(top_builddir) \
	$(COASTER_PKG_CFLAGS) \
	-DCOASTER_GLADEDIR=\""$(gladedir)"\" \
	-DCOASTER_DTDDIR=\""$(dtddir)"\" \
	-DCOASTER_UIDIR=\""$(uidir)"\" \
	-DCOASTER_ICONDIR=\""$(coaster_icondir)"\" \
	-DLOCALEDIR=\""$(datadir)/locale"\" \
	-DPREFIX=\""$(prefix)"\" \
	-DSYSCONFDIR=\""$(sysconfdir)"\" \
	-DLIBDIR=\""$(libdir)"\" \
	-DDATADIR=\""$(datadir)"\"

bacon_libs = 
if NAUTILUS_BACKEND
bacon_libs += baconmm/libcoasterbaconmm.la
endif

coaster_LDADD = \
	data/libcoasterdata.la \
	backends/libcoasterbackend.la \
	dialogs/libcoasterdialogs.la \
	widgets/libcoasterwidgets.la \
	eggmm/libcoastereggmm.la \
	eggmm/egg/libcoasteregg.la \
	$(bacon_libs) $(COASTER_PKG_LIBS)
