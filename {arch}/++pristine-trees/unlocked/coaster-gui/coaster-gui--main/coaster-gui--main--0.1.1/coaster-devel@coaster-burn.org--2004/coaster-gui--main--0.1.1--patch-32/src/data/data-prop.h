/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_DATA_PROP_H_
#define _COASTER_DATA_PROP_H_

#include "cstr-enums.h"
#include <glibmm/ustring.h>

namespace Coaster
{

namespace Data
{

struct Properties
{
  CDSizeType size;

  Glib::ustring title;
  bool append_date;
  Glib::ustring catnum;
  Glib::ustring publisher;
  Glib::ustring author;

  // Advanced Options
  IsoLevel isolevel;

  bool joliet;
  bool rockridge;

  SessionFormat sessionformat;

  inline bool operator==(const Properties& comp) const;
  inline bool operator!=(const Properties& comp) const;
};

inline bool Properties::operator==(const Properties& comp) const
{
  return ( size == comp.size && title == comp.title && append_date == comp.append_date &&
           catnum == comp.catnum && publisher == comp.publisher &&
           author == comp.author && isolevel == comp.isolevel && joliet == comp.joliet &&
           rockridge == comp.rockridge && sessionformat == comp.sessionformat );
}

inline bool Properties::operator!=(const Properties& comp) const
{
  return ( size != comp.size || title != comp.title || append_date == comp.append_date ||
           catnum != comp.catnum || publisher != comp.publisher ||
           author != comp.author || isolevel != comp.isolevel || joliet != comp.joliet ||
           rockridge != comp.rockridge || sessionformat != comp.sessionformat );
}

} // namespace Data
  
} // namespace Coaster

#endif // _COASTER_DATA_PROP_H_
