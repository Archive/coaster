/* Copyright (C) 2002-2004  The Coaster Development Team
 * Adapted from Rhythmbox's Ellipsized Label
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "widgets/cellrenderer-ellipsize.h"
#include "cstr-debug.h"

#include <gtk/gtkcellrenderertext.h>
#include <glib.h>

#include <glibmm/markup.h>

#include <iostream>

#define ELLIPSIS "..."

namespace
{
  
void add_attr(PangoAttrList* attr_list,
              PangoAttribute* attr)
{
  attr->start_index = 0;
  attr->end_index = G_MAXINT;
  
  pango_attr_list_insert (attr_list, attr);
}



int measure_string_width(const Glib::ustring& string,
                         Glib::RefPtr<Pango::Layout>& layout,
                         bool markup)
{
	int width, height;

	if (markup)
	{
		layout->set_markup(string);
	}
	else
	{
		layout->set_text(string);
	}
	
	layout->get_pixel_size(width, height);

	return width;
}

void compute_character_widths(const char* string,
                              PangoLayout* layout,
                              int* char_len_return,
                              int** widths_return,
                              int** cuts_return,
                              gboolean markup)
{
	int* widths;
	int* offsets;
	int* cuts;
	int char_len;
	int byte_len;
	const char* p;
	const char* nm_string;
	int i;
	PangoLayoutIter* iter;
	PangoLogAttr* attrs;
	
#define BEGINS_UTF8_CHAR(x) (((x) & 0xc0) != 0x80)

	if (markup)
	{
		pango_layout_set_markup (layout, string, -1);
	}
	else
	{
		pango_layout_set_text (layout, string, -1);
	}

	nm_string = pango_layout_get_text (layout);
	
	char_len = g_utf8_strlen (nm_string, -1);
	byte_len = strlen (nm_string);
	
	widths = g_new (int, char_len);
	offsets = g_new (int, byte_len);
	
	// Create a translation table from byte index to char offset
	p = nm_string;
	i = 0;
	while (*p) {
		int byte_index = p - nm_string;
		
		if (BEGINS_UTF8_CHAR (*p)) {
			offsets[byte_index] = i;
			++i;
		} else {
			offsets[byte_index] = G_MAXINT; // segv if we try to use this
		}
		
		++p;
	}

	// Now fill in the widths array
	iter = pango_layout_get_iter (layout);

	do {
		PangoRectangle extents;
		int byte_index;

		byte_index = pango_layout_iter_get_index (iter);

		if (byte_index < byte_len) {
			pango_layout_iter_get_char_extents (iter, &extents);
			
			g_assert (BEGINS_UTF8_CHAR (nm_string[byte_index]));
			g_assert (offsets[byte_index] < char_len);
			
			widths[offsets[byte_index]] = PANGO_PIXELS (extents.width);
		}
		
	} while (pango_layout_iter_next_char (iter));

	pango_layout_iter_free (iter);

	g_free (offsets);
	
	*widths_return = widths;

	// Now compute character offsets that are legitimate places to
	// chop the string
	attrs = g_new (PangoLogAttr, char_len + 1);
	
	pango_get_log_attrs (nm_string, byte_len, -1,
			     pango_context_get_language (
				     pango_layout_get_context (layout)),
			     attrs,
			     char_len + 1);

	cuts = g_new (int, char_len);
	i = 0;
	while (i < char_len) {
		cuts[i] = attrs[i].is_cursor_position;

		++i;
	}

	g_free (attrs);

	*cuts_return = cuts;

	*char_len_return = char_len;
}

typedef struct
{
  GString* string;
	int start_offset;
	int end_offset;
	int position;
} EllipsizeStringData;

void start_element_handler(GMarkupParseContext* context,
                           const gchar* element_name,
                           const gchar** attribute_names,
                           const gchar** attribute_values,
                           gpointer user_data,
                           GError** error)
{
	EllipsizeStringData* data = (EllipsizeStringData*)user_data;
	int i;
	
	g_string_append_c (data->string, '<');
	g_string_append (data->string, element_name);

	for (i = 0; attribute_names[i] != NULL; i++)
	{
		g_string_append_c (data->string, ' ');
		g_string_append (data->string, attribute_names[i]);
		g_string_append (data->string, "=\"");
		g_string_append (data->string, attribute_values[i]);
		g_string_append_c (data->string, '"');
	}

	g_string_append_c (data->string, '>');
}

void end_element_handler(GMarkupParseContext* context,
                         const gchar* element_name,
                         gpointer user_data,
                         GError** error)
{
	EllipsizeStringData* data = (EllipsizeStringData*)user_data;

	g_string_append (data->string, "</");
	g_string_append (data->string, element_name);
	g_string_append_c (data->string, '>');
}

void append_ellipsized_text(const char* text,
                            EllipsizeStringData* data,
                            int text_len,
                            gboolean markup)
{
	int position;
	int new_position;
	char* escaped;
	
	position = data->position;
	new_position = data->position + text_len;
	
	if (position > data->start_offset &&
	    new_position < data->end_offset)
	{
		return;
	}
	else if ((position < data->start_offset &&
	          new_position < data->start_offset) ||
	         (position > data->end_offset &&
	          new_position > data->end_offset))
	{
		if (markup)
		{
			escaped = g_markup_escape_text (text, strlen (text));
			g_string_append (data->string, escaped);
			g_free (escaped);
		}
		else
			g_string_append (data->string, text);
	}
	else if (position <= data->start_offset &&
		 new_position >= data->end_offset)
	{
		if (position < data->start_offset)
		{
			if (markup)
			{
				escaped = g_markup_escape_text (text, data->start_offset - position);
				g_string_append (data->string, escaped);
				g_free (escaped);
			}
			else
				g_string_append_len (data->string, text, data->start_offset - position);
		}

		g_string_append (data->string, ELLIPSIS);
		
		if (new_position > data->end_offset)
		{
			if (markup)
			{
				escaped = g_markup_escape_text (text + data->end_offset - position,
								position + text_len - data->end_offset);
				g_string_append (data->string, escaped);
				g_free (escaped);
			}
			else
				g_string_append_len (data->string, text + data->end_offset - position,
						     position + text_len - data->end_offset);
		}
	}

	data->position = new_position;
}

void text_handler(GMarkupParseContext* context,
                  const gchar* text,
                  gsize text_len,
                  gpointer user_data,
                  GError** error)
{
	EllipsizeStringData* data = (EllipsizeStringData*)user_data;

	append_ellipsized_text(text, data, text_len, TRUE);
}

static GMarkupParser pango_markup_parser = {
  start_element_handler,
  end_element_handler,
  text_handler,
  NULL,
  NULL
};

char* ellipsize_string (const char* string, 
                        int start_offset,
                        int end_offset,
                        gboolean markup)
{
	GString* str;
	EllipsizeStringData data;
	char* result;
	GMarkupParseContext* c;
	
	str = g_string_new (NULL);
	data.string = str;
	data.start_offset = start_offset;
	data.end_offset = end_offset;
	data.position = 0;

	if (markup)
	{
		c = g_markup_parse_context_new (&pango_markup_parser, 
					        GMarkupParseFlags(0), &data, NULL);
		g_markup_parse_context_parse (c, string, -1, NULL);
		g_markup_parse_context_free (c);
	}
	else
	{
		append_ellipsized_text (string, &data,
					g_utf8_strlen (string, -1), FALSE);
	}
	
	result = str->str;
	g_string_free (str, FALSE);
	return result;
}

Glib::ustring rb_string_ellipsize_start (const Glib::ustring& string,
                                         Glib::RefPtr<Pango::Layout>& layout,
                                         int width,
                                         int& resulting_width,
                                         bool markup)
{
	int* cuts;
	int* widths;
	int char_len;
	int truncate_offset;
	int bytes_end;
	
	// Zero-length string can't get shorter - catch this here to
	// avoid expensive calculations
	if (string == "")
		return Glib::ustring("");

	// I'm not sure if this short-circuit is a net win; it might be better
	// to just dump this, and always do the compute_character_widths() etc.
	// down below.

	resulting_width = measure_string_width (string, layout, markup);

	if (resulting_width <= width) {
		// String is already short enough.
		return string;
	}

	// Remove width of an ellipsis
	width -= measure_string_width (ELLIPSIS, layout, markup);

	if (width < 0) {
		// No room even for an ellipsis.
		return Glib::ustring("");
	}

	// Our algorithm involves removing enough chars from the string to bring
	// the width to the required small size. However, due to ligatures,
	// combining characters, etc., it's not guaranteed that the algorithm
	// always works 100%. It's sort of a heuristic thing. It should work
	// nearly all the time... but I wouldn't put in
	// g_assert (width of resulting string < width).
	//
	// Hmm, another thing that this breaks with is explicit line breaks
	// in "string"

	compute_character_widths (string.c_str(), layout->gobj(), &char_len, &widths, &cuts, markup);

        for (truncate_offset = 1; truncate_offset < char_len; truncate_offset++) {

        	resulting_width -= widths[truncate_offset];

        	if (resulting_width <= width &&
		    cuts[truncate_offset]) {
			break;
        	}
        }

	g_free (cuts);
	g_free (widths);
	
	bytes_end = g_utf8_offset_to_pointer (string.c_str(), truncate_offset) - string.c_str();
	
	return ellipsize_string (string.c_str(), 0, bytes_end, markup);
}

bool set_text_ellipsized(Glib::RefPtr<Pango::Layout>& layout,
                         const Glib::ustring& string,
                         Glib::ustring& resulting_string,
                         int width,
                         int&	resulting_width,
                         bool markup)
{
	bool ret;

	resulting_string = rb_string_ellipsize_start (string, layout, width,
					       resulting_width, markup);

	ret = (resulting_string.compare(string) != 0);

	return ret;
}

} // anonymous namespace

namespace Coaster
{

namespace Widgets
{

CellRendererEllipsize::CellRendererEllipsize()
: Glib::ObjectBase(typeid(CellRendererEllipsize)),
  Gtk::CellRendererText(),
  property_ellipsized_text_(*this, "ellipstext", "")
{
  property_mode() = Gtk::CELL_RENDERER_MODE_EDITABLE;
}

CellRendererEllipsize::~CellRendererEllipsize()
{}

Glib::PropertyProxy<Glib::ustring> CellRendererEllipsize::property_ellipsized_text()
{
  return property_ellipsized_text_.get_proxy();
}

void CellRendererEllipsize::get_size_vfunc(Gtk::Widget& widget,
                                           const Gdk::Rectangle* cell_area,
                                           int* x_offset,
                                           int* y_offset,
                                           int* width,
                                           int* height) const
{
  if(cell_area)
  {
    if(width)
      *width = cell_area->get_width() - (property_xpad() * 2);
    
    if(height)
      *height = cell_area->get_height() - (property_ypad() * 2);
    
    if(x_offset)
    {
      *x_offset = int(((widget.get_direction() == Gtk::TEXT_DIR_RTL) ?
          (1.0 - property_xalign()) : property_xalign()) * (cell_area->get_width() - *width));
      *x_offset = std::max(0, *x_offset);
    }
    
    if(y_offset)
    {
      *y_offset = int(property_yalign() * (cell_area->get_height() - *height));
      *y_offset = std::max(0, *y_offset);
    }
  }
}

void CellRendererEllipsize::render_vfunc(const Glib::RefPtr<Gdk::Drawable>& window,
                                         Gtk::Widget& widget,
                                         const Gdk::Rectangle& background_area,
                                         const Gdk::Rectangle& cell_area,
                                         const Gdk::Rectangle& expose_area,
                                         Gtk::CellRendererState flags)
{
  if(property_text() == "")
    return;

  int x_offset = 0, y_offset = 0, width = 0, height = 0;
  get_size(widget, cell_area, x_offset, y_offset, width, height);

  const Glib::ustring full_text = property_text();
  int len_str = full_text.size();

  if( len_str >= 6 )
  {

    Glib::RefPtr<Pango::Layout> layout = widget.create_pango_layout("");
    int resulting_width;
    Glib::ustring resulting_string;
      
    set_text_ellipsized(layout, full_text, resulting_string, width, resulting_width, false);
    property_text() = resulting_string;
  }

  Gtk::CellRendererText::render_vfunc(window,
                                      widget,
                                      background_area,
                                      cell_area,
                                      expose_area,
                                      flags);
}

} // namespace Widgets

} // namespace Coaster
