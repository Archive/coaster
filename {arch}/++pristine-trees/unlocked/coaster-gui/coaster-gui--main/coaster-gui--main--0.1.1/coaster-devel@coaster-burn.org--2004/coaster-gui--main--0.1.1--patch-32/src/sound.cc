/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "sound.h"

#include "util.h"

#include <libgnome/gnome-sound.h>

namespace Coaster
{

namespace Sound
{

void burn_finished()
{
  if(!Util::get_bool("notifications/sound_enable"))
    return;
  
  if(Util::get_bool("notifications/burn_finished"))
  {
    gnome_sound_play(Util::get_string("notifications/burn_finished_file").c_str());
  }
}

void burn_error()
{
  if(!Util::get_bool("notifications/sound_enable"))
    return;

  if(Util::get_bool("notifications/burn_error"))
  {
    gnome_sound_play(Util::get_string("notifications/burn_error_file").c_str());
  }
}
  
} // namespace Sound

} // namespace Coaster
