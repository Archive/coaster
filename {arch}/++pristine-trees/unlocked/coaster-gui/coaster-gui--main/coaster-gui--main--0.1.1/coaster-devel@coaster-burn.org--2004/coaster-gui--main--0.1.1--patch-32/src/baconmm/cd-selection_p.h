#ifndef _BACONMM_CD_SELECTION_P_H
#define _BACONMM_CD_SELECTION_P_H
#include <gtkmm/private/combobox_p.h>

#include <glibmm/class.h>

namespace Bacon
{

class CdSelection_Class : public Glib::Class
{
public:
#ifndef DOXYGEN_SHOULD_SKIP_THIS
  typedef CdSelection CppObjectType;
  typedef BaconCdSelection BaseObjectType;
  typedef BaconCdSelectionClass BaseClassType;
  typedef Gtk::ComboBox_Class CppClassParent;
  typedef GtkComboBoxClass BaseClassParent;

  friend class CdSelection;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

  const Glib::Class& init();

  static void class_init_function(void* g_class, void* class_data);

  static Glib::ObjectBase* wrap_new(GObject*);

protected:

  //Callbacks (default signal handlers):
  //These will call the *_impl member methods, which will then call the existing default signal callbacks, if any.
  //You could prevent the original default signal handlers being called by overriding the *_impl method.
  static void device_changed_callback(GtkWidget* self, const char* device_path);

  //Callbacks (virtual functions):
};


} // namespace Bacon

#endif /* _BACONMM_CD_SELECTION_P_H */
