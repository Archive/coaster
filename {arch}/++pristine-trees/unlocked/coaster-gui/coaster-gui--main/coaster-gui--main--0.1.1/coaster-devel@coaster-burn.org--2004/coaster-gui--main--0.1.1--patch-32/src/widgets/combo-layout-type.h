/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_COMBO_LAYOUT_TYPE_H_
#define _COASTER_COMBO_LAYOUT_TYPE_H_

#include "cstr-enums.h"

#include <gtkmm/combobox.h>
#include <gtkmm/treemodel.h>

namespace Gdk
{
class Pixbuf;
} // namespace Gdk

namespace Gtk
{
class ListStore;
} // namespace Gtk

namespace Coaster
{

class ComboLayoutType : public Gtk::ComboBox
{
public:
  ComboLayoutType();
  ~ComboLayoutType();

  LayoutType get_selected_layout_type() const;
  int get_active_row_number() const;

  class ComboColumns : public Gtk::TreeModel::ColumnRecord
  {
  public:
    ComboColumns()
    {
      add(m_col_pixbuf);
      add(m_col_type);
      add(m_col_layout_type);
    }

    Gtk::TreeModelColumn<Glib::RefPtr<Gdk::Pixbuf> > m_col_pixbuf;
    Gtk::TreeModelColumn<Glib::ustring> m_col_type;
    Gtk::TreeModelColumn<LayoutType> m_col_layout_type;
  };

private:
  Glib::RefPtr<Gtk::ListStore> m_refListModel;
};
  
} // namespace Coaster

#endif // _COASTER_COMBO_LAYOUT_TYPE_H_
