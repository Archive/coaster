/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_ERASE_DIALOG_
#define _COASTER_ERASE_DIALOG_

#include "sharedptr.h"

#include <gtkmm/dialog.h>
#include <cd-drive.h>

namespace Gtk
{
class CheckButton;
class HBox;
class Image;
class Button;
class Label;
class Expander;
class ProgressBar;
} // namespace Gtk

namespace Bacon
{
class CdSelection;
} // namespace Bacon

namespace Gnome
{
namespace Glade
{
class Xml;
} // namespace Glade
} // namespace Gnome

namespace Coaster
{

namespace Dialogs
{

class Erase : public Gtk::Dialog
{
public:
  Erase(BaseObjectType* cobject,
        const Glib::RefPtr<Gnome::Glade::Xml>& refXml);
  virtual ~Erase();

  bool get_fast() const;
  CDDrive* get_cd_drive() const;

  void set_action_widgets_sensitive(bool sensitive = true);

protected:
  void on_combo_changed();
  void on_finished_erasing();
  void on_ok_clicked();
  
private:
  Gtk::Image* m_pImage;
  
  Gtk::Label* m_pLabel_Drive;
  Gtk::HBox* m_pHBox_Drive;

  Gtk::CheckButton* m_pFast;

  Gtk::Button* m_pOK;

  Bacon::CdSelection* m_pComboDrives;
};

} // namespace Dialogs
  
} // namespace Coaster

#endif // _COASTER_ERASE_DIALOG_
