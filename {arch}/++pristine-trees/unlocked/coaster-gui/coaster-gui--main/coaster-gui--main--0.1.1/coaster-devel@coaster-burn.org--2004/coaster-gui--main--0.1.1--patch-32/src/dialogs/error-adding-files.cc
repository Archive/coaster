/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "dialogs/error-adding-files.h"

#include <gtkmm/label.h>
#include <libglademm/xml.h>

namespace Coaster
{

namespace Dialogs
{

ErrorAddingFiles::ErrorAddingFiles(BaseObjectType* cobject,
                                   const Glib::RefPtr<Gnome::Glade::Xml>& refXml)
: Gtk::Dialog(cobject), m_pErrAdd_Label(0)
{
  refXml->get_widget("err_add_label", m_pErrAdd_Label);
}

ErrorAddingFiles::~ErrorAddingFiles()
{}

void ErrorAddingFiles::set_files(const type_listUStrings& files)
{
  Glib::ustring unadded_files;
  
  type_listUStrings::const_iterator start = files.begin();
  type_listUStrings::const_iterator end = files.end();

  unadded_files += *start;
  ++start;

  while(start != end)
  {
    unadded_files += "\n" + *start;
    ++start;
  }

  m_pErrAdd_Label->set_text(unadded_files);
}

} // namespace Dialogs

} // namespace Coaster
