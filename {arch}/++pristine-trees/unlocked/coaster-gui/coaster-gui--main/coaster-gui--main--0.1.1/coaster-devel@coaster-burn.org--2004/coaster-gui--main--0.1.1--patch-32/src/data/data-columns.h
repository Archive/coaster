/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_DATA_COLUMNS_H_
#define _COASTER_DATA_COLUMNS_H_

#include "cstr-enums.h"
#include "cstr-types.h"

#include <glibmm/ustring.h>
#include <gdkmm/pixbuf.h>
#include <gtkmm/treemodelcolumn.h>
#include <libgnomevfsmm/types.h>

namespace Coaster
{

namespace Data
{

class Columns : public Gtk::TreeModelColumnRecord
{
public:
  Columns()
  {
    add(m_col_name);
    add(m_col_ref_pixbuf);
    add(m_col_size);
    add(m_col_uri);
    add(m_col_entity_type);
    add(m_col_mime_type);
    add(m_col_id);
  }

  // visible columns
  Gtk::TreeModelColumn<Glib::ustring> m_col_name;
  Gtk::TreeModelColumn<Glib::RefPtr<Gdk::Pixbuf> > m_col_ref_pixbuf;
  Gtk::TreeModelColumn<Gnome::Vfs::FileSize> m_col_size;
  Gtk::TreeModelColumn<Glib::ustring> m_col_uri;

  // invisible columns
  Gtk::TreeModelColumn<EntityType> m_col_entity_type;
  Gtk::TreeModelColumn<Glib::ustring> m_col_mime_type;
  Gtk::TreeModelColumn<RowId> m_col_id;
};

const Columns& columns() G_GNUC_CONST;

} // namespace Data

} // namespace Coaster

#endif // _COASTER_DATA_COLUMNS_H_
