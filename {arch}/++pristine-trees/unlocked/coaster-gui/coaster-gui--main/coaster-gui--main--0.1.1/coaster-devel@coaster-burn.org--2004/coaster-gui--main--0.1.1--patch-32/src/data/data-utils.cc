/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "data/data-utils.h"

#include "cstr-debug.h"
#include "instance-manager.h"
#include "data/data-columns.h"

#include <gtkmm/treestore.h>

namespace
{

Coaster::RowId col_index = 0;

} // anonymous namespace

namespace Coaster
{

namespace Data
{

bool new_row(const Row& row,
             const Glib::RefPtr<Gtk::TreeStore>& treestore,
             RowId id)
{
  const Columns& modelColumns = columns();

  InstanceMgr::instance()->lookup_icon(row.mime, row.rowref, treestore);

  Gtk::TreeRow trow = *(treestore->get_iter(row.rowref.get_path()));

  trow[modelColumns.m_col_name] = row.name;
  trow[modelColumns.m_col_mime_type] = row.mime;
  trow[modelColumns.m_col_size] = row.size;
  trow[modelColumns.m_col_entity_type] = row.entity_type;
  trow[modelColumns.m_col_uri] = row.uri;
  if(id == 0)
    trow[modelColumns.m_col_id] = ++col_index;
  else
    trow[modelColumns.m_col_id] = id;

  return false;
}

void new_directory_row(const Gtk::TreePath& path,
                       const Glib::RefPtr<Gtk::TreeStore>& treestore,
                       const Glib::ustring& name)
{
  const Columns& modelColumns = columns();
  
  Gtk::TreeRow row = *(treestore->get_iter(path));
  Gtk::TreeRowReference rowref(treestore, path);

  row[modelColumns.m_col_name] = name;
  row[modelColumns.m_col_mime_type] = "x-directory/normal";
  row[modelColumns.m_col_size] = 0;
  row[modelColumns.m_col_entity_type] = COASTER_ENTITY_DIRECTORY;
  row[modelColumns.m_col_uri] = "";
  row[modelColumns.m_col_id] = ++col_index;
}

void new_file_row(const Gtk::TreePath& path,
                  const Glib::RefPtr<Gtk::TreeStore>& treestore,
                  const Glib::ustring& name,
                  int size,
                  EntityType entity_type,
                  const Glib::ustring& uri,
                  const Glib::ustring& mime_type)
{
  const Columns& modelColumns = columns();

  Gtk::TreeRow row = *(treestore->get_iter(path));
  Gtk::TreeRowReference rowref(treestore, path);

  row[modelColumns.m_col_name] = name;
  row[modelColumns.m_col_size] = size;
  row[modelColumns.m_col_entity_type] = entity_type;
  row[modelColumns.m_col_uri] = uri;
  row[modelColumns.m_col_mime_type] = mime_type;
  row[modelColumns.m_col_id] = ++col_index;
  debug("TreePath new_file_row: ",path.to_string());
}

void copy_row(Gtk::TreeRow& new_row,
              const Gtk::TreeRow& old_row)
{
  const Columns& modelColumns = columns();

  new_row[modelColumns.m_col_name] = old_row.get_value(modelColumns.m_col_name);
  new_row[modelColumns.m_col_ref_pixbuf] = old_row.get_value(modelColumns.m_col_ref_pixbuf);
  new_row[modelColumns.m_col_size] = old_row.get_value(modelColumns.m_col_size);
  new_row[modelColumns.m_col_entity_type] = old_row.get_value(modelColumns.m_col_entity_type);
  new_row[modelColumns.m_col_uri] = old_row.get_value(modelColumns.m_col_uri);
  new_row[modelColumns.m_col_mime_type] = old_row.get_value(modelColumns.m_col_mime_type);
  new_row[modelColumns.m_col_id] = old_row.get_value(modelColumns.m_col_id);
}

bool is_tree_dir(const Gtk::TreeIter& iter)
{
  const Columns& model_columns = columns();
  
  if((*iter).get_value(model_columns.m_col_entity_type) == COASTER_ENTITY_DIRECTORY)
    return true;
  else
    return false;
}

void set_name(Gtk::TreeRow& row,
              const Glib::ustring& name)
{
  const Columns& model_columns = columns();

  row[model_columns.m_col_name] = name;
}

} // namespace Data

} // namespace Coaster
