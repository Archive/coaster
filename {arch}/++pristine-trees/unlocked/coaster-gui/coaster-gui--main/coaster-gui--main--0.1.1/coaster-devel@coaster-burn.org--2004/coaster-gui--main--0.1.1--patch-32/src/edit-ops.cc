/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "edit-ops.h"

#include "cstr-debug.h"
#include "ucompose.h"
#include "xml-io.h"

#include <libxml/xmlmemory.h>

#include <libxml++/document.h>
#include <libxml++/nodes/element.h>
#include <libxml++/parsers/domparser.h>

#include <gtkmm/clipboard.h>

#include <libgnomevfsmm/types.h>

#include <iostream>

namespace
{

const char *TARGET_DATA_ROW = "application/x-coaster/data-row";

struct
{
  std::string           target;
  char                 *buffer;
  Gnome::Vfs::FileSize  length;

  void reset()
  {
    xmlFree(buffer);
    target = "";
    buffer = 0;
    length = 0;
  }

  void set_data(Gtk::SelectionData& data)
  {
    if(data.get_target() != target)
      return;
     data.set(target, 8, (guchar*)buffer, length);
  }
} clipboard_contents = { "", 0, 0 };

sigc::slot<void,const xmlpp::Element*,bool,bool> add_from_xml;

void paste_data(const Gtk::SelectionData& data)
{
  xmlpp::DomParser parser;

  try
  {
    parser.parse_memory(data.get_data_as_string());
  }
  catch(const xmlpp::exception& e)
  {
    Coaster::debug(String::ucompose("Edit::paste_data: exception: %1", e.what()));
    return;
  }

  xmlpp::Document* doc = parser.get_document();
  add_from_xml(doc->get_root_node(), false, true);
}

void selection_requested_cb(Gtk::SelectionData& data,
                            guint info)
{
  clipboard_contents.set_data (data);
}

void selection_received_cb(const Gtk::SelectionData& data)
{
  if(!data.get_length())
    return;

  Coaster::debug("Paste target: ",data.get_target());
  if(data.get_target() == TARGET_DATA_ROW)
    paste_data(data);
  
  /*else if (data.get_target () == TARGET_WIDGET)
    paste_widgets_impl (data);*/
}

void selection_cleared_cb ()
{
  // Do nothing
}

} // anonymous namespace

namespace Coaster
{

namespace Edit
{

void copy_data_row(const Gtk::TreeRow& row,
                   bool cut)
{
  xmlpp::Document storage;
  xmlpp::Element* rootNode = storage.create_root_node("coaster-rows");

  XmlIO::tree_branch_to_xml(row, rootNode, true, cut);

  debug(&storage);
  
  clipboard_contents.reset();
  xmlChar **buffer_addr = &reinterpret_cast<xmlChar*&>(clipboard_contents.buffer);
  int buffer_size_tmp;
  
  xmlDocDumpMemory(storage.cobj(), buffer_addr, &buffer_size_tmp);
  clipboard_contents.length = buffer_size_tmp;

  std::list<Gtk::TargetEntry> targets;
  targets.push_back(Gtk::TargetEntry(TARGET_DATA_ROW));
  clipboard_contents.target = TARGET_DATA_ROW;

  Glib::RefPtr<Gtk::Clipboard> clipboard = Gtk::Clipboard::get();
  clipboard->set(targets,
                 sigc::ptr_fun(selection_requested_cb),
                 sigc::ptr_fun(selection_cleared_cb));
}

void copy_data_rows(const std::list<Gtk::TreeRow>& rows,
                    bool cut)
{
  xmlpp::Document storage;

  xmlpp::Element* rootNode = storage.create_root_node("coaster-rows");
  for(std::list<Gtk::TreeRow>::const_iterator i = rows.begin() ; i != rows.end() ; ++i)
  {
    XmlIO::tree_branch_to_xml((*i), rootNode, true, cut);
  }

  debug(&storage);

  xmlChar **buffer_addr = &reinterpret_cast<xmlChar*&>(clipboard_contents.buffer);
  int buffer_size_tmp;
  
  xmlDocDumpMemory(storage.cobj(), buffer_addr, &buffer_size_tmp);
  clipboard_contents.length = buffer_size_tmp;

  std::list<Gtk::TargetEntry> targets;
  targets.push_back(Gtk::TargetEntry(TARGET_DATA_ROW));
  clipboard_contents.target = TARGET_DATA_ROW;

  Glib::RefPtr<Gtk::Clipboard> clipboard = Gtk::Clipboard::get();
  clipboard->set(targets,
                 sigc::ptr_fun(selection_requested_cb),
                 sigc::ptr_fun(selection_cleared_cb));
}

void paste_data_rows(const sigc::slot<void,const xmlpp::Element*,bool,bool>& add_slot)
{
  add_from_xml = add_slot;
  
  Glib::RefPtr<Gtk::Clipboard> clipboard = Gtk::Clipboard::get();
  clipboard->request_contents(TARGET_DATA_ROW, sigc::ptr_fun(selection_received_cb));
}

} // namespace Edit

} // namespace Coaster
