/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "mainwindow.h"

#include "cstr-debug.h"
#include "cstr-intl.h"
#include "document.h"
#include "instance-manager.h"
#include "sound.h"
#include "ucompose.h"
#include "util.h"

#include "widgets/view-base.h"

#include "dialogs/about.h"
#include "dialogs/glade-dialog.h"
#include "dialogs/burn.h"
#include "dialogs/erase.h"
#include "dialogs/copy.h"
#include "dialogs/preferences.h"
#include "dialogs/dialog-util.h"

#include "backends/cd-io.h"

#include <gtkmm/statusbar.h>
#include <gtkmm/stock.h>
#include <gtkmm/messagedialog.h>

#include <libgnome/gnome-help.h>

#include <bakery/App/AppInstanceManager.h>

#include <iostream>
#include <unistd.h>
#include <sys/types.h>

#define COASTER_FILE_MENU_UI COASTER_UIDIR G_DIR_SEPARATOR_S "coaster-file-menu.ui"
#define COASTER_EDIT_MENU_UI COASTER_UIDIR G_DIR_SEPARATOR_S "coaster-edit-menu.ui"
#define COASTER_OTHER_MENU_UI COASTER_UIDIR G_DIR_SEPARATOR_S "coaster-other-menu.ui"
#define COASTER_TOOLBAR_UI COASTER_UIDIR G_DIR_SEPARATOR_S "coaster-toolbar.ui"

namespace // Anonymous Namespace for this file only (kinda like C's static functions)
{

void on_preferences_closed_clicked(GtkWidget* widget,
                                   gpointer data)
{
  if(data != NULL)
    gtk_widget_hide(GTK_WIDGET(data));
}

//////////////////////////////////////////////
// Function: can_read()                     //
// Purpose: Check if a file's permissions   //
//          and owner id match this         //
//          program's user id.              //
// Input: An integer for the UID and an     //
//        enumeration for the permissions.  //
// Output: A bool representing whether the  //
//         file is readable (true) or not.  //
//////////////////////////////////////////////
bool can_read (unsigned int uid,
               Gnome::Vfs::FilePermissions perms)
{
  bool canRead = false;
  uid_t tempUid = getuid();

  if ((uid == tempUid) && (perms & Gnome::Vfs::PERM_USER_READ))
    canRead = true;
  else if (perms & Gnome::Vfs::PERM_OTHER_READ)
    canRead = true;

  return canRead;
}

Coaster::Document* get_document_from_uri(const Glib::ustring& uri)
{
  using namespace Coaster;
  Glib::ustring mime_type = Glib::convert_return_gchar_ptr_to_ustring(gnome_vfs_get_mime_type(uri.c_str()));

  if(mime_type == "application/x-blf")
  {
    debug("Document type: application/x-blf");
    return new Document(COASTER_LAYOUT_DATA);
  }
/*  else if(mime_type == "application/x-bla")
    return new AudioDoc();
  else if(mime_type == "application/x-blv")
    return new VideoDoc();*/
  else
    return 0;
}



const char *const coaster_icon_filename =
  COASTER_ICONDIR G_DIR_SEPARATOR_S "coaster.png";

} // anonymous namespace


namespace Coaster
{

Gtk::MessageDialog* MainWindow::m_pErrorHelp = 0;
bool MainWindow::m_bErrorHelp_Shown = false;
Dialogs::About* MainWindow::m_pAbout = 0;
Glib::ustring MainWindow::m_about_icon = "icon.png";

//////////////////////////////////////////////////////////////////////////////////////////
//  Coaster::MainWindow                                                                 //
//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////
// Function: MainWindow() (constructor)     //
// Purpose: Initialize our window.          //
// Input: none                              //
// Output: none                             //
//////////////////////////////////////////////
MainWindow::MainWindow(BaseObjectType* cobject,
                       const Glib::RefPtr<Gnome::Glade::Xml>& refXml)
: Gtk::Window(cobject), // It's a virtual base class, so we have to call the 
                        // specific constructor to prevent the default constructor
                        // from being called.
  type_base(cobject, _("Coaster")),
  instmgr(InstanceMgr::instance()),
  m_pMainVBox(0), m_pView(0), m_pStatus(0)
{
  refXml->get_widget("bakery_vbox", m_pVBox);
  refXml->get_widget("main_vbox", m_pMainVBox);

  // Derive the Info Box and grab the status bar
  refXml->get_widget("status_bar", m_pStatus);

  add_mime_type("application/x-blf");
  add_mime_type("application/x-bla");
  add_mime_type("application/x-blv");

  show_all_children();
}

//////////////////////////////////////////////
// Function: ~MainWindow() (destructor)     //
// Purpose: Delete anything we need to.     //
// Input: none                              //
// Output: none                             //
//////////////////////////////////////////////
MainWindow::~MainWindow()
{
  delete m_pView;

  if(m_AppInstanceManager.get_app_count() == 0)
  {
    if(m_pErrorHelp)
    {
      delete m_pErrorHelp;
      m_pErrorHelp = 0;
    }
  }
}

//////////////////////////////////////////////
// Function: init()                         //
// Purpose: Initialize the interface.       //
// Input: none                              //
// Output: none                             //
//////////////////////////////////////////////
bool MainWindow::init(const Glib::ustring& document_uri)
{
  init_about_information();

  using namespace Gtk;

  Bakery::App_WithDoc_Gtk::init();
  
  // connect signal handlers to signals
  //std::cout << "1" << std::endl;
  //instmgr->signal_sensitive().connect(sigc::mem_fun(*this, &MainWindow::on_instmgr_sensitive));
  //std::cout << "2" << std::endl;
  //std::cout << "3" << std::endl;

  if(!document_uri.empty())
    open_document(Util::create_full_uri(document_uri));

  instmgr->signal_sensitive().connect(sigc::mem_fun(*this, &MainWindow::on_instmgr_sensitive));
  
  return true;
}

void MainWindow::init_about_information()
{
  type_vecStrings authors, documenters;

  authors.push_back("Coaster UI:");
  authors.push_back("Bryan Forbes <bryan@reigndropsfall.net>");
  authors.push_back("");
  authors.push_back("libcoaster:");
  authors.push_back("Sean Harshbarger <harshy@dersoldat.org>");
  authors.push_back("");
  authors.push_back("libburn:");
  authors.push_back("Derek Foreman <manmower@signalmarketing.com>");
  authors.push_back("Ben Jansens <xor@orodu.net>");

  documenters.push_back("Bryan Forbes <bryan@reigndropsfall.net>");

  set_about_icon(coaster_icon_filename);
  set_about_information(PACKAGE_VERSION, authors, 
                        _("Copyright (C) 2003-2004 The Coaster Development Team"),
                        _("A disc burning program"), documenters, "");
}

void MainWindow::set_about_icon(const Glib::ustring& icon)
{
  m_about_icon = icon;
}

//////////////////////////////////////////////////////////////////////////////////////////
//  Coaster::MainWindow -- protected                                                          //
//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////
// Function: new_instance()                 //
// Purpose: Create a new instance of the    //
//          interface.                      //
// Input: none                              //
// Output: A pointer to the new instance of //
//         the application                  //
//////////////////////////////////////////////
Bakery::App* MainWindow::new_instance()
{
  Glib::RefPtr<Gnome::Glade::Xml> refXml = Gnome::Glade::Xml::create(glade_main_filename, "window_main");
  MainWindow* pMainWindow = 0;
  refXml->get_widget_derived("window_main", pMainWindow);

  return pMainWindow;
}

//////////////////////////////////////////////
// Function: do_nothing()                   //
// Purpose: Do nothing when it is called.   //
// Input: none                              //
// Output: none                             //
//////////////////////////////////////////////
void MainWindow::do_nothing()
{
  debug("This does nothing!");
}

int MainWindow::add_ui_from_file(const Glib::ustring& ui_filename)
{
  int result;
  try
  {
    result = m_refUIManager->add_ui_from_file(ui_filename);
    return result;
  }
  catch(const Glib::Error& ex)
  {
    std::cerr << "building menus failed: " <<  ex.what();
  }

  return 0;
}

Glib::ustring MainWindow::ui_file_select_open()
{
  return Dialogs::ui_offer_open_files(*this);
}

bool MainWindow::open_document(const Glib::ustring& file_uri)
{
  //Check whether the file exists:
  debug("Opening: ",file_uri);
  if(Util::is_uri(file_uri))
  {
    if(!Glib::file_test(Glib::filename_from_uri(file_uri), Glib::FILE_TEST_EXISTS))
    {
      ui_warning(_("Open failed: File does not exist."));
      return false;
    }
  }
  else
  {
    if(!Glib::file_test(Util::create_full_path(file_uri), Glib::FILE_TEST_EXISTS))
    {
      ui_warning(_("Open failed: File does not exist."));
      return false;
    }
  }

  //Check whether it's already open:
  //It could even be open in this instance.
  bool bAlreadyOpen = false;
  MainWindow* pAppAlreadyOpen = 0;

  Bakery::AppInstanceManager::type_listAppInstances apps =  m_AppInstanceManager.get_instances();
  for(Bakery::AppInstanceManager::type_listAppInstances::iterator iter = apps.begin(); iter != apps.end(); iter++)
  {
    MainWindow* pApp = dynamic_cast<MainWindow*>(*iter);
    if(pApp)
    {
      Bakery::Document* pDoc = pApp->get_document();
      if(pDoc->get_file_uri() == file_uri)
      {
        bAlreadyOpen = true;
        pAppAlreadyOpen = pApp;
      }
    }
  }

  if(bAlreadyOpen)
  {
    //Bring it to the front:
    if(pAppAlreadyOpen)
    {
      pAppAlreadyOpen->ui_bring_to_front();
    }

    //Tell user that it's already open:
    ui_warning(gettext("This document is already open."));

    return true; //success.
  }
  else
  {
    //Open it:
        
    //Load it into a new instance unless the current document is just a default new.
    MainWindow* pApp = 0;
    bool bUsingNewInstance = false;
    if(!(get_document()->get_is_new())) //if it's not new.
    {
      //New instance:
      pApp = dynamic_cast<MainWindow*>(new_instance());
      pApp->init(); //It's shown too.
      bUsingNewInstance = true;
    }
    else
    {
      pApp = this; //Replace the default new document in this instance.
    }

    delete pApp->m_pDocument;
    pApp->m_pDocument = 0;

    pApp->m_pDocument = get_document_from_uri(file_uri);

    //Open it.
    pApp->m_pDocument->set_file_uri(file_uri);
    bool bTest = pApp->m_pDocument->load();

    if(!bTest) //if open failed.
    {
      ui_warning(gettext("Open failed."));

      if(bUsingNewInstance)
      {
        //Remove new instance:
        pApp->get_document()->set_modified(false); //avoid 'do you want to save?' dialog.
        pApp->on_menu_file_close();
      }
      else
      {
        //re-initialize document.
        pre_init_view_actions(pApp);
        delete pApp->m_pDocument;
        delete pApp->m_pView;
        pApp->m_pDocument = 0;
        pApp->m_pView = 0;

        pApp->init_create_document();
        pApp->init_view_actions();
      }

      return false; //failed.
    }
    else
    {
      //if open succeeded:
      pre_init_view_actions(pApp);

      delete pApp->m_pView;
      pApp->m_pView = 0;
      
      pApp->m_pView = dynamic_cast<Widgets::View_Base*>(pApp->m_pDocument->get_view());
      pApp->m_pView->set_document(static_cast<Document*>(pApp->m_pDocument));
      pApp->m_pMainVBox->pack_start(*manage(pApp->m_pView));
      pApp->m_pView->show();

      pApp->init_view_actions();

      pApp->m_pDocument->set_is_new(true);
      pApp->m_pDocument->signal_modified().connect(sigc::mem_fun(*pApp, &MainWindow::on_document_modified));

      pApp->on_document_load();
      pApp->update_window_title();
      pApp->set_document_modified(false); //disables menu and toolbar Save items.

      //Update document history list:
      document_history_add(file_uri);

      return true; //success.
    }
  } //if already open.

  return false; //failed.
}

//////////////////////////////////////////////
// Function: init_create_document()         //
// Purpose: Initialize the view when the    //
//          program starts.                 //
// Input: none                              //
// Output: none                             //
//////////////////////////////////////////////
void MainWindow::init_create_document()
{
  init_create_document(COASTER_LAYOUT_DATA);
}

void MainWindow::init_create_document(LayoutType type)
{
  if(m_pDocument == NULL)
  {
    m_pDocument = new Document(type);
    if(m_pView)
      delete m_pView;

    m_pView = dynamic_cast<Widgets::View_Base*>(m_pDocument->get_view());
    m_pView->set_document(static_cast<Document*>(m_pDocument));

    m_pMainVBox->pack_start(*manage(m_pView));
  }

  //m_pView->show();
  show_all_children();

  Bakery::App_WithDoc_Gtk::init_create_document();
}

//////////////////////////////////////////////
// Function: init_ui_manager()              //
// Purpose: //
// Input: none                              //
// Output: none                             //
//////////////////////////////////////////////
void MainWindow::init_ui_manager()
{
  // set up bakery's stuff, since I like it (thanks murray!)
  Bakery::App_WithDoc_Gtk::init_ui_manager();
  
  // connect signals for menu tooltips
  m_refUIManager->signal_connect_proxy().connect(sigc::mem_fun(*this, &MainWindow::on_ui_connect));
}

//////////////////////////////////////////////
// Function: init_menus_file()              //
// Purpose: //
// Input: none                              //
// Output: none                             //
//////////////////////////////////////////////
void MainWindow::init_menus_file()
{
  using namespace Gtk;

  // File menu

  //Build actions:
  m_refFileActionGroup = ActionGroup::create("CoasterFileActions");

  m_refFileActionGroup->add(Action::create("Coaster_Menu_File", _("_File")));
  m_refFileActionGroup->add(Action::create("Coaster_Menu_File_RecentFiles",
                                           _("_Recent Files")));

  //File actions
  Glib::RefPtr<Action> actionNew = Action::create("CoasterAction_File_New", Gtk::Stock::NEW,
                                                  _("_New Layout"), _("Create a new layout"));
  actionNew->property_is_important() = true;
  m_refFileActionGroup->add(actionNew,
                            sigc::mem_fun(*this, &MainWindow::on_menu_file_new));
  m_refFileActionGroup->add(Action::create("CoasterAction_File_Open", Gtk::Stock::OPEN,
                                           _("_Open..."), _("Open a layout")),
                            sigc::mem_fun(*this, &MainWindow::on_menu_file_open));

  //Remember thes ones for later, so we can disable Save menu and toolbar items:
  m_action_save = Action::create("CoasterAction_File_Save", Gtk::Stock::SAVE,
                                 _("_Save"), _("Save the current layout"));
  m_refFileActionGroup->add(m_action_save,
                            sigc::mem_fun(*this, &MainWindow::on_menu_file_save));

  m_action_saveas = Action::create("CoasterAction_File_SaveAs", Gtk::Stock::SAVE_AS,
                                   _("Save _As..."), 
                                   _("Save the current layout with a different name"));                   
  m_refFileActionGroup->add(m_action_saveas,
                            sigc::mem_fun(*this, &MainWindow::on_menu_file_saveas));
                        
  m_refFileActionGroup->add(Action::create("CoasterAction_File_Close", Gtk::Stock::CLOSE,
                                           _("_Close"), _("Close the current layout")),
                            sigc::mem_fun(*this, &MainWindow::on_menu_file_close));
  m_refFileActionGroup->add(Action::create("CoasterAction_File_Exit", Gtk::Stock::QUIT,
                                           _("_Quit"), _("Quit the program")),
                            sigc::mem_fun(*this, &MainWindow::on_menu_file_exit));
                        
  m_refUIManager->insert_action_group(m_refFileActionGroup);

  //Add menu:
  add_ui_from_file(COASTER_FILE_MENU_UI);
 
  //Add recent-files submenu:
  Bakery::App_WithDoc_Gtk::init_menus_file_recentfiles("/Bakery_MainMenu/Bakery_MenuPH_File/Coaster_Menu_File/Coaster_Menu_File_RecentFiles");
}

//////////////////////////////////////////////
// Function: init_menus_edit()              //
// Purpose: //
// Input: none                              //
// Output: none                             //
//////////////////////////////////////////////
void MainWindow::init_menus_edit()
{
  using namespace Gtk;

  m_refEditActionGroup = ActionGroup::create("CoasterEditActions");

  m_refEditActionGroup->add(Action::create("Coaster_Menu_Edit", _("_Edit")));

  m_refEditActionGroup->add(Action::create("CoasterAction_Preferences", Gtk::Stock::PREFERENCES,
                                           _("Pr_eferences"), _("Configure Coaster")),
                      sigc::mem_fun(*this, &MainWindow::on_preferences_clicked));

  m_refUIManager->insert_action_group(m_refEditActionGroup);

  add_ui_from_file(COASTER_EDIT_MENU_UI);
}

//////////////////////////////////////////////
// Function: init_menus_other()             //
// Purpose: //
// Input: none                              //
// Output: none                             //
//////////////////////////////////////////////
void MainWindow::init_menus_other()
{
  using namespace Gtk;

  m_refOtherActionGroup = ActionGroup::create("CoasterOtherActions");

  // Popup Menu
  m_refOtherActionGroup->add(Action::create("CoasterPopup"));
  //m_refOtherActionGroup->add(Action::create("CoasterAction_Popup_Menu_Add", Gtk::Stock::ADD));

  //Extra Menus:
  m_refOtherActionGroup->add(Action::create("Coaster_Menu_Search", _("_Search")));
  m_refOtherActionGroup->add(Action::create("Coaster_Menu_Layout", _("_Layout")));
  m_refOtherActionGroup->add(Action::create("Coaster_Menu_CD", _("_CD")));
  
  //"Add" submenu:
  //m_refOtherActionGroup->add(Action::create("CoasterAction_Layout_Menu_Add", Gtk::Stock::ADD));
 
  //"CD" actions:
  m_refOtherActionGroup->add(Action::create("CoasterAction_CD_BurnISO",
                                            Gtk::StockID("coaster-burn"),
                    _("Burn _ISO..."), _("Burn an ISO image to disc")),
                    sigc::mem_fun(*this, &MainWindow::on_burn_iso_clicked));
  m_refOtherActionGroup->add(Action::create("CoasterAction_CD_Copy", Gtk::StockID("coaster-copy"),
                    _("_Copy..."), _("Copy a CD to another")),
                    sigc::mem_fun(*this, &MainWindow::on_copy_cd_clicked));

  m_refEraseAction = Action::create("CoasterAction_CD_Erase", Gtk::StockID("coaster-erase"),
                    _("Erase..."), _("Erase a CD/RW"));
  m_refOtherActionGroup->add(m_refEraseAction, sigc::mem_fun(*this, &MainWindow::on_erase_clicked));

  m_refEjectAction = Action::create("CoasterAction_CD_Eject", 
                    _("Eject"), _("Eject the CD in CD/DVD drive"));
  m_refOtherActionGroup->add(m_refEjectAction);

  m_refUIManager->insert_action_group(m_refOtherActionGroup);

  add_ui_from_file(COASTER_OTHER_MENU_UI);
 
  //m_refEraseAction->set_sensitive(false);
  m_refEjectAction->set_sensitive(false);
}

void MainWindow::init_menus_help()
{
  using namespace Gtk;
  //Help menu

  //Build actions:
  m_refHelpActionGroup = ActionGroup::create("BakeryHelpActions");
  m_refHelpActionGroup->add(Action::create("BakeryAction_Menu_Help","_Help"));

  m_refHelpActionGroup->add(Action::create("CoasterAction_Help_Contents",
                                           Gtk::Stock::HELP),
                                           sigc::mem_fun(*this, &MainWindow::on_menu_help_contents));
  m_refHelpActionGroup->add(Action::create("BakeryAction_Help_About",
                                           Gtk::StockID("gnome-stock-about")),
                                           sigc::mem_fun(*this, &MainWindow::on_menu_help_about));

  m_refUIManager->insert_action_group(m_refHelpActionGroup);

  //Build part of the menu structure, to be merged in by using the "PH" plaeholders:
  static const Glib::ustring ui_description =
    "<ui>"
    "  <menubar name='Bakery_MainMenu'>"
    "    <placeholder name='Bakery_MenuPH_Help'>"
    "      <menu action='BakeryAction_Menu_Help'>"
    "        <menuitem action='CoasterAction_Help_Contents' />"
    "        <menuitem action='BakeryAction_Help_About' />"
    "      </menu>"
    "    </placeholder>"
    "  </menubar>"
    "</ui>";

  //Add menu:
  add_ui_from_string(ui_description);
}

void MainWindow::pre_init_view_actions(MainWindow* pApp)
{
  pApp->m_refUIManager->remove_ui(pApp->last_view_merge_ui);
  pApp->m_refUIManager->remove_action_group(pApp->m_pView->get_ui_action_group());
}

void MainWindow::init_view_actions()
{
  m_refUIManager->insert_action_group(m_pView->get_ui_action_group());

  last_view_merge_ui = add_ui_from_file(m_pView->get_ui_location());
  m_pView->set_popup_menu(static_cast<Gtk::Menu*>(m_refUIManager->get_widget("/CoasterPopup")));
}

void MainWindow::init_unfinished_actions()
{
  // Actions that won't be implemented until later
  //m_refOtherActionGroup->get_action("CoasterAction_Layout_Add_Session")->set_sensitive(false);

  // Unfinished Other actions
  //m_refOtherActionGroup->get_action("CoasterAction_Search_Find")->set_sensitive(false);
  //m_refOtherActionGroup->get_action("CoasterAction_Search_FindNext")->set_sensitive(false);
  //m_refOtherActionGroup->get_action("CoasterAction_Search_FindPrev")->set_sensitive(false);
  m_refOtherActionGroup->get_action("CoasterAction_CD_Copy")->set_sensitive(false);
  m_refOtherActionGroup->get_action("CoasterAction_CD_Erase")->set_sensitive(false);
}

//////////////////////////////////////////////
// Function: init_menus()                   //
// Purpose: //
// Input: none                              //
// Output: none                             //
//////////////////////////////////////////////
void MainWindow::init_menus()
{
  init_menus_file();
  m_refFileActionGroup->get_action("CoasterAction_File_New")->property_is_important() = true;
  init_menus_edit();
  init_menus_other();
  init_menus_help();

  add_ui_from_file(COASTER_TOOLBAR_UI); // add this before we add
                                        // the view menus in order
                                        // to get the toolbar in
                                        // the correct order
  init_view_actions();

  init_unfinished_actions();
}

//////////////////////////////////////////////
// Function: init_toolbars()                //
// Purpose: //
// Input: none                              //
// Output: none                             //
//////////////////////////////////////////////
void MainWindow::init_toolbars()
{
  using namespace Gtk;

}

void MainWindow::init_layout()
{
  //We override this method so that we can put everything in the vbox from the glade file, instead of the vbox from App_Gtk and leave out the placeholder vbox.

  //Add menu bar at the top:
  //These were defined in init_uimanager().
  Gtk::MenuBar* pMenuBar = static_cast<Gtk::MenuBar*>(m_refUIManager->get_widget("/Bakery_MainMenu"));
  m_pVBox->pack_start(*pMenuBar, Gtk::PACK_SHRINK);

  Gtk::Toolbar* pToolBar = static_cast<Gtk::Toolbar*>(m_refUIManager->get_widget("/Bakery_ToolBar"));
  m_HandleBox_Toolbar.add(*pToolBar);
  m_HandleBox_Toolbar.show();

  add_accel_group(m_refUIManager->get_accel_group());

  m_pVBox->pack_start(m_HandleBox_Toolbar, Gtk::PACK_SHRINK);
}

void MainWindow::on_menu_help_contents()
{
  GError* error = 0;

  gnome_help_display("coaster", 0, &error);
  std::cout << error << std::endl;
  if(error)
  {
    if(m_pErrorHelp && m_bErrorHelp_Shown) // Error box hasn't been closed, so just raise it
    {
      m_pErrorHelp->set_transient_for(*this);
      m_pErrorHelp->set_markup(String::ucompose("<b>%1</b>\n\n%2", _("Could not display help for Coaster"), error->message));

      Glib::RefPtr<Gdk::Window> error_win = m_pErrorHelp->get_window();
      error_win->show();
      error_win->raise();
    }
    else
    {
      //Re-create About box:
      if(m_pErrorHelp)
      {
        delete m_pErrorHelp;
        m_pErrorHelp = 0;
      }

      m_pErrorHelp = new Gtk::MessageDialog(*this, String::ucompose("<b><span size='larger'>%1</span></b>\n\n%2", _("Could not display help for Coaster"), error->message), true, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_CLOSE);

      m_pErrorHelp->set_default_response(Gtk::RESPONSE_CLOSE);

      m_pErrorHelp->set_transient_for(*this);
      m_pErrorHelp->signal_response().connect(sigc::mem_fun(*this, &MainWindow::on_help_error_close));
      m_bErrorHelp_Shown = true;
      m_pErrorHelp->run();
    }

    g_error_free(error);
  }
}

void MainWindow::on_help_error_close(int response)
{
  m_pErrorHelp->hide();
  m_bErrorHelp_Shown = false;
}

void MainWindow::on_menu_help_about()
{
  if(m_pAbout && m_bAboutShown) // "About" box hasn't been closed, so just raise it
  {
    m_pAbout->set_transient_for(*this);

    Glib::RefPtr<Gdk::Window> about_win = m_pAbout->get_window();
    about_win->show();
    about_win->raise();
  }
  else
  {
    // not yet wrapped:
    Glib::RefPtr<Gdk::Pixbuf> refLogo = Gdk::Pixbuf::create_from_file(m_about_icon);

    //Re-create About box:
    if(m_pAbout)
    {
      delete m_pAbout;
      m_pAbout = 0;
    }
    m_pAbout = new Dialogs::About(m_strAppName, m_HelpInfo.m_strVersion,
                                  m_HelpInfo.m_strCopyright, m_HelpInfo.m_strDescription,
                                  "http://www.coaster-burn.org",
                                  m_HelpInfo.m_vecAuthors, m_HelpInfo.m_vecDocumenters,
                                  m_HelpInfo.m_strTranslatorCredits, refLogo);

    m_pAbout->set_transient_for(*this);
    m_pAbout->signal_hide().connect(sigc::mem_fun(*this, &MainWindow::on_about_close));
    m_bAboutShown = true;
    m_pAbout->show();
  }
}

Bakery::App_WithDoc::enumSaveChanges MainWindow::ui_offer_to_save_changes()
{
  return Dialogs::ui_offer_to_save_changes(*this, m_pDocument->get_name());
}

//////////////////////////////////////////////
// Function: on_menu_file_exit()            //
// Purpose: Quit the program when called.   //
// Input: none                              //
// Output: none                             //
//////////////////////////////////////////////
void MainWindow::on_menu_file_exit()
{
  m_AppInstanceManager.close_all();
}

//////////////////////////////////////////////
// Function: on_menu_file_new()             //
// Purpose: //
// Input: none                              //
// Output: none                             //
//////////////////////////////////////////////
void MainWindow::on_menu_file_new()
{
  Bakery::App* pApp = new_instance();
  dynamic_cast<MainWindow*>(pApp)->init();
}

//////////////////////////////////////////////
// Function: on_ui_connect()                //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void MainWindow::on_ui_connect(const Glib::RefPtr<Gtk::Action>& action,
                               Gtk::Widget* widget)
{
  if(dynamic_cast<Gtk::MenuItem*>(widget))
  {
    (static_cast<Gtk::MenuItem*>(widget))->signal_select().connect(sigc::bind(sigc::mem_fun(*this, &MainWindow::on_show_menu_tip), action));
    (static_cast<Gtk::MenuItem*>(widget))->signal_deselect().connect(sigc::mem_fun(*this, &MainWindow::on_clear_menu_tip));
  }
}

//////////////////////////////////////////////
// Function: on_show_menu_tip()             //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void MainWindow::on_show_menu_tip(const Glib::RefPtr<Gtk::Action>& action)
{
  Glib::ustring tip = action->property_tooltip();
  if (tip == "") tip = " ";
  m_pStatus->push(" " + tip);
}

//////////////////////////////////////////////
// Function: on_clear_menu_tip()            //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void MainWindow::on_clear_menu_tip()
{
  m_pStatus->pop();
}

void MainWindow::on_preferences_clicked()
{
  debug("preferences clicked");
  //std::auto_ptr<Preferences> prefsdialog = GladeDialog<Preferences>::create(*this, "diag_prefs");
  Dialogs::Preferences prefsdialog(*this);

  prefsdialog.run();

  //instmgr->refresh_drives();
  //std::auto_ptr<Gtk::Dialog> diag(Glib::wrap((GtkDialog*)(coaster_preferences_window(instmgr->get_drives(), instmgr->get_n_drives(), (GCallback*)(on_preferences_closed_clicked)))));
  //diag->run();
}

void MainWindow::on_burn_iso_clicked()
{
  debug("Burn ISO clicked");

  // render these pixbufs before setting the interface insensitive
  // so we get a normal lookign icon
  Glib::RefPtr<Gdk::Pixbuf> pb_idle, pb_burn;
  pb_idle = render_icon(Gtk::StockID("coaster-idle"), Gtk::ICON_SIZE_MENU);
  pb_burn = render_icon(Gtk::StockID("coaster-burn"), Gtk::ICON_SIZE_MENU);

  int response_file_chooser;
  int response_burn_dialog;
  std::string iso_filename;

  instmgr->set_sensitive(false);

  Dialogs::ui_offer_open_iso(*this, response_file_chooser, iso_filename);

  if(response_file_chooser == Gtk::RESPONSE_OK)
  {
    instmgr->create_status_icon(pb_idle);
    instmgr->set_status_tip("Idle");

    std::auto_ptr<Dialogs::Burn> burndialog = 
      Dialogs::GladeDialog<Dialogs::Burn>::create(Util::get_win(this),
                                                  "diag_burn");

    response_burn_dialog = burndialog->run();
    
    if(response_burn_dialog == Gtk::RESPONSE_OK)
    {
      burndialog->hide();

      instmgr->set_status_pixbuf(pb_burn);
      instmgr->set_status_tip("Burning...");

      IO::BurnDisc burn(Util::get_win(this),
                        burndialog->get_cd_drive(),
                        burndialog->get_eject(),
                        burndialog->get_speed(),
                        burndialog->get_dummy(),
                        iso_filename);

      if(burn.start_burn())
        Sound::burn_finished();
      else
        Sound::burn_error();
    }

    instmgr->remove_status_icon();
  }


  instmgr->set_sensitive(true);
  return;
}

void MainWindow::on_copy_cd_clicked()
{
  debug("Copy CD clicked");
  std::auto_ptr<Dialogs::Copy> copydialog =
    Dialogs::GladeDialog<Dialogs::Copy>::create(*this, "diag_copy");

  // set all instances so we can't do anything
  instmgr->set_sensitive(false);

  int response = copydialog->run();

  switch(response)
  {
    case Gtk::RESPONSE_OK:
      //copy cd
      debug("Copy: OK");
      break;
    case Gtk::RESPONSE_HELP:
      //help
      debug("Copy: Help");
      break;
    case Gtk::RESPONSE_CANCEL:
    default:
      debug("Copy: Cancel");
      break;
  }

  // return all instances to normal
  instmgr->set_sensitive(true);
}

void MainWindow::on_erase_clicked()
{
  debug("Erase CD clicked");
  std::auto_ptr<Dialogs::Erase> erasedialog =
    Dialogs::GladeDialog<Dialogs::Erase>::create(*this, "diag_erase");

  // render this pixbuf before setting the interface insensitive
  // so we get a normal lookign icon
  Glib::RefPtr<Gdk::Pixbuf> pb_idle, pb_erase;
  pb_idle = render_icon(Gtk::StockID("coaster-idle"), Gtk::ICON_SIZE_MENU);
  pb_erase = render_icon(Gtk::StockID("coaster-erase"), Gtk::ICON_SIZE_MENU);

  // set all instances so we can't do anything
  instmgr->set_sensitive(false);

  instmgr->set_status_pixbuf(pb_idle);
  instmgr->set_status_tip("Idle");
  
  int response = erasedialog->run();

  if(response == Gtk::RESPONSE_OK)
  {
    //erase cd
    debug("Erase: OK");

    erasedialog->hide();

    instmgr->set_status_pixbuf(pb_erase);
    instmgr->set_status_tip("Erasing...");

    //Erase operation
  }

  instmgr->remove_status_icon();
  // return all instances to normal
  instmgr->set_sensitive(true);
}

void MainWindow::on_instmgr_sensitive(bool sensitive)
{
  set_sensitive(sensitive);
}

} // namespace Coaster
