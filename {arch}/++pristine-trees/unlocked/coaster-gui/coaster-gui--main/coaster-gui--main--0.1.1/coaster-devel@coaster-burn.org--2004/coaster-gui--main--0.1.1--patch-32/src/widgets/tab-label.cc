/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "widgets/tab-label.h"

#include <gtkmm/label.h>
#include <gtkmm/button.h>
#include <gtkmm/image.h>
#include <gtkmm/stock.h>

namespace Coaster
{

TabLabel::TabLabel(const Gtk::StockID& stock_id)
: Gtk::HBox(false, 6), m_pImage(0), m_pButton(0)
{
  using namespace Gtk;
  
  m_pImage = new Image(stock_id, ICON_SIZE_MENU);
  m_pButton = new Button();

  m_pButton->set_size_request(16, 16);
  m_pButton->set_relief(RELIEF_NONE);
  m_pButton->add(*manage(new Image(Stock::CLOSE, ICON_SIZE_MENU)));
  m_pButton->signal_state_changed().connect(sigc::mem_fun(*this, &TabLabel::on_button_state_changed));

  pack_start(*manage(m_pImage), PACK_EXPAND_WIDGET);
  pack_start(*manage(m_pButton), PACK_SHRINK);
  show_all();
}

TabLabel* TabLabel::create(const Gtk::StockID& stock_id, sigc::slot<void> clicked_slot)
{
  TabLabel* new_tl = new TabLabel(stock_id);

  new_tl->signal_button_clicked().connect(clicked_slot);
  
  return new_tl;
}

TabLabel::~TabLabel()
{}

Glib::SignalProxy0<void> TabLabel::signal_button_clicked()
{
  return m_pButton->signal_clicked();
}

Gtk::Widget* TabLabel::get_button()
{
  return m_pButton;
}

void TabLabel::on_button_state_changed(Gtk::StateType type)
{
  using namespace Gtk;
  
  if(m_pButton->get_state() == STATE_ACTIVE)
    m_pButton->set_state(STATE_NORMAL);
}

} // namespace Coaster
