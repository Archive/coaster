/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "dialogs/chooser.h"
#include "cstr-intl.h"

#include <gtkmm/button.h>
#include <gtkmm/stock.h>
#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/alignment.h>
#include <gtkmm/checkbutton.h>

namespace
{

class ChooserException : public Glib::Exception
{
public:
  ChooserException(const Glib::ustring& description);
  virtual ~ChooserException() throw();
  
  Glib::ustring what() const;
private:
  Glib::ustring desc;
};

ChooserException::ChooserException(const Glib::ustring& description)
: desc(description)
{}

ChooserException::~ChooserException() throw()
{}

Glib::ustring ChooserException::what() const
{
  return desc;
}

} // anonymous namespace

namespace Coaster
{

namespace Dialogs
{

Chooser::Chooser(Gtk::Window& parent,
                 ChooserType type,
                 bool audio)
: Gtk::FileChooserDialog(parent, N_("Temporary Title")),
  m_pAllFiles(0), m_pAudioFiles(0),
  m_pAllLayoutFiles(0), m_pDataLayoutFiles(0),
  m_pAudioLayoutFiles(0), m_pVideoLayoutFiles(0),
  m_pHiddenFiles(0), m_pRecursive(0),
  m_chooser_type_(type), m_audio_filters_(audio)
{
  using namespace Gtk;

  if(m_chooser_type_ == CHOOSER_TYPE_FILES)
  {
    set_action(Gtk::FILE_CHOOSER_ACTION_OPEN);
    set_title(_("Add Files"));

    set_select_multiple();

    if(m_audio_filters_)
    {
      m_pAudioFiles = new FileFilter();
      m_pAudioFiles->set_name(_("Audio Files"));
      m_pAudioFiles->add_mime_type("application/ogg");
      m_pAudioFiles->add_mime_type("application/x-ogg");
      m_pAudioFiles->add_mime_type("audio/mpeg");
      m_pAudioFiles->add_mime_type("audio/x-mp3");
      m_pAudioFiles->add_mime_type("application/x-flac");
      m_pAudioFiles->add_mime_type("audio/x-flac");
      m_pAudioFiles->add_mime_type("application/x-wav");
      m_pAudioFiles->add_mime_type("audio/x-wav");

      m_pAllFiles = new FileFilter();
      m_pAllFiles->set_name(_("All Files"));
      m_pAllFiles->add_pattern("*");

      add_filter(*manage(m_pAllFiles));
      add_filter(*manage(m_pAudioFiles));
    }
  }
  else if(m_chooser_type_ == CHOOSER_TYPE_OPEN)
  {
    set_action(Gtk::FILE_CHOOSER_ACTION_OPEN);
    set_title(_("Open Layout"));

    m_pAllLayoutFiles = new FileFilter();
    m_pAllLayoutFiles->set_name(_("All Layout Files"));
    m_pAllLayoutFiles->add_mime_type("application/x-blf");
    m_pAllLayoutFiles->add_mime_type("application/x-bla");
    m_pAllLayoutFiles->add_mime_type("application/x-blv");

    m_pDataLayoutFiles = new FileFilter();
    m_pDataLayoutFiles->set_name(_("Data Layouts"));
    m_pDataLayoutFiles->add_mime_type("application/x-blf");

    m_pAudioLayoutFiles = new FileFilter();
    m_pAudioLayoutFiles->set_name(_("Audio Layouts"));
    m_pAudioLayoutFiles->add_mime_type("application/x-bla");

    m_pVideoLayoutFiles = new FileFilter();
    m_pVideoLayoutFiles->set_name(_("Video Layouts"));
    m_pVideoLayoutFiles->add_mime_type("application/x-blv");

    m_pAllFiles = new FileFilter();
    m_pAllFiles->set_name(_("All Files"));
    m_pAllFiles->add_pattern("*");

    add_filter(*manage(m_pAllLayoutFiles));
    add_filter(*manage(m_pDataLayoutFiles));
    add_filter(*manage(m_pAudioLayoutFiles));
    add_filter(*manage(m_pVideoLayoutFiles));
    add_filter(*manage(m_pAllFiles));
  }  
  else if(m_chooser_type_ == CHOOSER_TYPE_FOLDER)
  {
    set_action(Gtk::FILE_CHOOSER_ACTION_SELECT_FOLDER);
    set_title(_("Add Folder"));
  }

  if(m_chooser_type_ != CHOOSER_TYPE_OPEN)
  {
    HBox *hBox = new HBox();

    if(m_chooser_type_ == CHOOSER_TYPE_FOLDER)
    {
      m_pRecursive = new CheckButton(_("R_ecursive"), true);
      m_pRecursive->set_active();
      hBox->pack_end(*manage(m_pRecursive), PACK_SHRINK);
    }

    m_pHiddenFiles = new CheckButton(_("Hi_dden Files"), true);
    m_pHiddenFiles->signal_toggled().connect(sigc::mem_fun(*this, &Chooser::on_hidden_files));
    hBox->pack_end(*manage(m_pHiddenFiles), PACK_SHRINK);

    hBox->show_all();

    set_extra_widget(*manage(hBox));
  }

  add_button(Stock::CANCEL, RESPONSE_CANCEL);

  if(m_chooser_type_ != CHOOSER_TYPE_OPEN)
    add_button(Stock::ADD, RESPONSE_OK);
  else
    add_button(Stock::OPEN, RESPONSE_OK);

  set_default_response(RESPONSE_OK);
}

Chooser::~Chooser()
{}

bool Chooser::get_hidden_files() const
{
  return m_pHiddenFiles->get_active();
}

bool Chooser::get_recursive() const
{
  if(m_chooser_type_ != CHOOSER_TYPE_FOLDER)
    throw ChooserException(_("Cannot access recursive functions in a File Chooser"));
  else
    return m_pRecursive->get_active();
}

void Chooser::set_recursive(bool recursive)
{
  if(m_chooser_type_ != CHOOSER_TYPE_FOLDER)
    throw ChooserException(_("Cannot access recursive functions in a File Chooser"));
  else
    m_pRecursive->set_active(recursive);
}

void Chooser::on_hidden_files()
{
  property_show_hidden() = m_pHiddenFiles->get_active();
}

} // namespace Dialogs

} // namespace Coaster
