/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_CELL_RENDERER_ELLIPSIZE_H_
#define _COASTER_CELL_RENDERER_ELLIPSIZE_H_

#include <gtkmm/cellrenderertext.h>

namespace Coaster
{

namespace Widgets
{

class CellRendererEllipsize : public Gtk::CellRendererText
{
public:
  CellRendererEllipsize();
  ~CellRendererEllipsize();

  Glib::PropertyProxy<Glib::ustring> property_ellipsized_text();
  Glib::PropertyProxy<int> property_last_cell_width();

protected:
  void get_size_vfunc(Gtk::Widget& widget,
                      const Gdk::Rectangle* cell_area,
                      int* x_offset,
                      int* y_offset,
                      int* width,
                      int* height) const;

  void render_vfunc(const Glib::RefPtr<Gdk::Drawable>& window,
                    Gtk::Widget& widget,
                    const Gdk::Rectangle& background_area,
                    const Gdk::Rectangle& cell_area,
                    const Gdk::Rectangle& expose_area,
                    Gtk::CellRendererState flags);

private:
  Glib::Property<Glib::ustring> property_ellipsized_text_;
};

} // namespace Widgets

} // namespace Coaster

#endif // _COASTER_CELL_RENDERER_ELLIPSIZE_H_
