/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "client.h"

#include "cstr-strings.h"

#include <gtkmm/combobox.h>
#include <gtkmm/spinbutton.h>

#include "baconmm/cd-selection.h"

#include <iostream>

namespace Bakery
{
namespace Conf
{

void Association<Gtk::SpinButton>::connect_widget(Callback widget_changed)
{
  m_widget.signal_value_changed().connect(widget_changed);
}

void Association<Gtk::SpinButton>::load_widget()
{
  int val = get_conf_client()->get_int(get_key());
  if (m_widget.get_value() != val)
    m_widget.set_value(val);
}

void Association<Gtk::SpinButton>::save_widget()
{
  int val = static_cast<int>(m_widget.get_value());
  if (get_conf_client()->get_int(get_key()) != val)
    get_conf_client()->set(get_key(), val);
}

// ---------------------------------------------------------
// ComboBox Association
void Association<Gtk::ComboBox>::connect_widget(Callback widget_changed)
{
  m_widget.signal_changed().connect(widget_changed);
}

void Association<Gtk::ComboBox>::load_widget()
{
  // Only set it if it has changed (avoids excess notifications).
  int val = get_conf_client()->get_int(get_key());
  if (m_widget.get_active_row_number() != val)
    m_widget.set_active(val);
}

void Association<Gtk::ComboBox>::save_widget()
{
  // Only set it if it has changed (avoids excess notifications).
  int val = m_widget.get_active_row_number();
  if (get_conf_client()->get_int(get_key()) != val)
    get_conf_client()->set(get_key(), val);
}

// ---------------------------------------------------------
// Bacon::CdSelection Association
void Association<Bacon::CdSelection>::connect_widget(Callback widget_changed)
{
  m_widget.signal_device_changed().connect(sigc::hide(widget_changed));
}

void Association<Bacon::CdSelection>::load_widget()
{
  // Only set it if it has changed (avoids excess notifications).
  Glib::ustring val = get_conf_client()->get_string(get_key());
  if (m_widget.get_device() != val)
    m_widget.set_device(val);
}

void Association<Bacon::CdSelection>::save_widget()
{
  // Only set it if it has changed (avoids excess notifications).
  Glib::ustring val = m_widget.get_device();
  if (get_conf_client()->get_string(get_key()) != val)
    get_conf_client()->set(get_key(), val);
}

} // namespace Conf
} // namespace Bakery

namespace Coaster
{

Client::Client()
: Bakery::Conf::Client(COASTER_GCONFDIR)
{}

Client::~Client()
{}

void Client::add_implementation(const Glib::ustring& key,
                                Gtk::Widget& widget,
                                bool instant)
{
  if(dynamic_cast<Bacon::CdSelection*>(&widget))
    add_association(key, static_cast<Bacon::CdSelection&>(widget), instant);
  else if (dynamic_cast<Gtk::ComboBox*>(&widget))
    add_association(key, static_cast<Gtk::ComboBox&>(widget), instant);
  else
    Bakery::Conf::Client::add_implementation(key, widget, instant);
}

} // namespace Coaster
