/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_UNDO_MANAGER_H_
#define _COASTER_UNDO_MANAGER_H_

#include "sharedptr.h"
#include "undo-op.h"

#include <sigc++/signal.h>
#include <glibmm/ustring.h>
#include <deque>
#include <list>

namespace Coaster
{

class UndoManager : public SharedObject
{
public:
  UndoManager();
  virtual ~UndoManager();

  void push(const UndoOpPtr& op);
  void undo();
  void redo();

  void set_origin();

  typedef sigc::signal<void> type_signal_changed;
  typedef sigc::signal<void> type_signal_origin_reached;

  type_signal_changed&        signal_changed();
  type_signal_origin_reached& signal_origin_reached();

protected:
  class UndoStack
  {
  public:
    typedef UndoOpPtr               value_t;
    typedef std::deque<value_t>     impl_stack_t;
    typedef impl_stack_t::size_type size_type;

  private:
    impl_stack_t impl_stack;

  public:
    ~UndoStack();
    
    void      clear();
    size_type truncate(size_type max_size);

    value_t   top() const;
    void      pop();
    void      push(const value_t& value);

    bool      empty() const;
    size_type size() const;
  };

  UndoStack undo_stack;
  UndoStack redo_stack;

public:
  UndoStack::size_type undo_size() const;
  UndoStack::size_type redo_size() const;

  Glib::ustring        undo_label() const;
  Glib::ustring        redo_label() const;

protected:

  UndoStack::size_type origin;
  bool                 origin_valid;

  void push_undo(const UndoOpPtr& op);
  void push_redo(const UndoOpPtr& op);

  void truncate_undo_stack();

private:
  type_signal_changed        m_signal_changed_;
  type_signal_origin_reached m_signal_origin_reached_;
};

typedef SharedPtr<UndoManager> UndoManagerPtr;

} // namespace Coaster

#endif // _COASTER_UNDO_MANAGER_H_
