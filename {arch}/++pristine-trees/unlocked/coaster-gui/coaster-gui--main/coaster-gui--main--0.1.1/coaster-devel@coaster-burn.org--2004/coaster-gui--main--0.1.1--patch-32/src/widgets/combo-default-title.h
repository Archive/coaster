/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_COMBO_DEFAULT_TITLE_H_
#define _COASTER_COMBO_DEFAULT_TITLE_H_

#include <gtkmm/combobox.h>
#include <gtkmm/treemodel.h>

namespace Gtk
{
class ListStore;
} // namespace Gtk

namespace Gnome
{
namespace Glade
{
class Xml;
} // namespace Glade
} // namespace Gnome

namespace Coaster
{

class ComboDefaultTitle : public Gtk::ComboBox
{
public:
  ComboDefaultTitle();
  virtual ~ComboDefaultTitle();

  class ComboColumns : public Gtk::TreeModel::ColumnRecord
  {
  public:
    ComboColumns()
    {
      add(m_col_name);
      add(m_col_gconf_value);
    }

    Gtk::TreeModelColumn<Glib::ustring> m_col_name;
    Gtk::TreeModelColumn<Glib::ustring> m_col_gconf_value;
  };

  Glib::ustring get_gconf_value() const;
  void set_from_gconf_value(const Glib::ustring& value);

protected:
private:
  Glib::RefPtr<Gtk::ListStore> m_refStore;
};
  
} // namespace Coaster

#endif // _COASTER_COMBO_DEFAULT_TITLE_H_
