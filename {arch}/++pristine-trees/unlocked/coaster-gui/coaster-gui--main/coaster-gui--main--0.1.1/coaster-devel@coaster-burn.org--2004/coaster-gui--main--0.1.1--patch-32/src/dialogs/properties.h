/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_DIALOG_PROPERTIES_H_
#define _COASTER_DIALOG_PROPERTIES_H_

#include "data/data-prop.h"

#include <gtkmm/dialog.h>

namespace Gtk
{
class Entry;
class CheckButton;
class RadioButton;
class Button;
} // namespace Gtk

namespace Gnome
{
namespace Glade
{
class Xml;
} // namespace Glade
} // namespace Gnome

namespace Coaster
{

namespace Dialogs
{

class Properties : public Gtk::Dialog
{
public:
  Properties(BaseObjectType* cobject,
             const Glib::RefPtr<Gnome::Glade::Xml>& refXml);
  virtual ~Properties();

  Data::Properties get_properties() const;
  void init(const Data::Properties& dp);
  void init_signals();
  
protected:
  void on_anything_changed(const Glib::ustring& str);

private:
  Gtk::Entry* m_pTitle;
  Gtk::CheckButton* m_pAppendDate;
  Gtk::Entry* m_pCatNum;
  Gtk::Entry* m_pPublisher;
  Gtk::Entry* m_pAuthor;

  Gtk::RadioButton* m_pISOLevel1;
  Gtk::RadioButton* m_pISOLevel2;

  Gtk::CheckButton* m_pJoliet;
  Gtk::CheckButton* m_pRockRidge;
  
  Gtk::RadioButton* m_pCDROM;
  Gtk::RadioButton* m_pCDI;
  Gtk::RadioButton* m_pCDXA;

  Gtk::Button*      m_pOK;
};

} // namespace Dialogs

} // namespace Coaster

#endif // _COASTER_DIALOG_PROPERTIES_H_
