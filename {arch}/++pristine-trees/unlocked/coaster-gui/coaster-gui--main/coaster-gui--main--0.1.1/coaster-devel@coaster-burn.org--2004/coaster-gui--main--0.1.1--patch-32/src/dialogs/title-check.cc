/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "dialogs/title-check.h"
#include "client.h"

#include <gtkmm/image.h>
#include <libglademm/xml.h>

namespace Coaster
{

namespace Dialogs
{

TitleCheck::TitleCheck(BaseObjectType* cobject,
                       const Glib::RefPtr<Gnome::Glade::Xml>& refXml)
: ClientDialog(cobject, refXml), m_pBurn_Image(0)
{
  refXml->get_widget("title_burn_image", m_pBurn_Image);
  connect_widget("layouts/title_check", "titlechk_check");

  m_pBurn_Image->set(Gtk::StockID("coaster-burn"), Gtk::ICON_SIZE_BUTTON);
}

TitleCheck::~TitleCheck()
{}

} // namespace Dialogs

} // namespace Coaster
