/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "dialogs/copy.h"
#include "cstr-debug.h"
#include "ucompose.h"
#include "baconmm/cd-selection.h"

#include <gtkmm/checkbutton.h>
#include <gtkmm/image.h>
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/label.h>

#include <libglademm/xml.h>

namespace Coaster
{

namespace Dialogs
{

Copy::Copy(BaseObjectType* cobject,
           const Glib::RefPtr<Gnome::Glade::Xml>& refXml)
: Gtk::Dialog(cobject),
  m_pImage(0), m_pHBox_Read(0), m_pHBox_Write(0), m_pOK(0), m_pCancel(0),
  m_pLabel_Read(0), m_pLabel_Write(0), m_pComboDrives_Read(0), 
  m_pComboDrives_Write(0)
{
  refXml->get_widget("copy_image", m_pImage);
  refXml->get_widget("copy_hbox_read", m_pHBox_Read);
  refXml->get_widget("copy_hbox_write", m_pHBox_Write);
  refXml->get_widget("copy_label_read", m_pLabel_Read);
  refXml->get_widget("copy_label_write", m_pLabel_Write);
  refXml->get_widget("copy_ok", m_pOK);
  refXml->get_widget("copy_cancel", m_pCancel);

  m_pOK->set_sensitive(false);
  m_pOK->signal_clicked().connect(sigc::mem_fun(*this, &Copy::on_ok_clicked));
  
  m_pImage->set(Gtk::StockID("coaster-copy"), Gtk::ICON_SIZE_DIALOG);

  m_pComboDrives_Read = new Bacon::CdSelection();
  m_pComboDrives_Read->property_file_image() = false;
  m_pComboDrives_Read->property_show_recorders_only() = false;
  m_pLabel_Read->set_mnemonic_widget(*m_pComboDrives_Read);
  m_pComboDrives_Read->signal_device_changed().connect(sigc::mem_fun(*this,
                                                                     &Copy::on_combo_changed));

  m_pHBox_Read->pack_start(*Gtk::manage(m_pComboDrives_Read));
  m_pHBox_Read->show_all();
  
  m_pComboDrives_Write = new Bacon::CdSelection();
  m_pComboDrives_Write->property_file_image() = false;
  m_pComboDrives_Write->property_show_recorders_only() = true;
  m_pLabel_Write->set_mnemonic_widget(*m_pComboDrives_Write);
  /*if(m_pComboDrives_Write->is_selected_burnable())
    m_pOK->set_sensitive(true);*/
  m_pComboDrives_Write->signal_device_changed().connect(sigc::mem_fun(*this,
                                                                      &Copy::on_combo_changed));

  m_pHBox_Write->pack_start(*Gtk::manage(m_pComboDrives_Write));
  m_pHBox_Write->show_all();

  set_default_response(Gtk::RESPONSE_OK);
}

Copy::~Copy()
{
  debug("Copy: destroyed");
}

CDDrive* Copy::get_read_drive() const
{
  return const_cast<CDDrive*>(m_pComboDrives_Read->get_cdrom());
}

CDDrive* Copy::get_write_drive() const
{
  return const_cast<CDDrive*>(m_pComboDrives_Write->get_cdrom());
}

void Copy::set_action_widgets_sensitive(bool sensitive)
{
  m_pComboDrives_Read->set_sensitive(sensitive);
  m_pComboDrives_Write->set_sensitive(sensitive);
  get_action_area()->set_sensitive(sensitive);
  m_pCancel->set_sensitive(true);
}

void Copy::on_combo_changed(const Glib::ustring& device)
{
  /*if(m_pComboDrives_Write->is_selected_burnable())
    m_pOK->set_sensitive();
  else
    m_pOK->set_sensitive(false);*/
}

void Copy::on_ok_clicked()
{
  set_action_widgets_sensitive(false);

  //IO::erasedisc(get_drive_info(), get_fast(), sigc::mem_fun(*this, &Copy::pulse));
 
  set_action_widgets_sensitive(true);
  response(Gtk::RESPONSE_OK);
}

} // namespace Dialogs

} // namespace Coaster
