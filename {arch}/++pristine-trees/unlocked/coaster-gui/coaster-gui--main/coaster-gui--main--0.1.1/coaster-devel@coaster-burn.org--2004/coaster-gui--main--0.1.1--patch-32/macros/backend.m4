AC_DEFUN([COASTER_BACKEND_CHECK],
[
  AC_ARG_ENABLE([burn-backend], AC_HELP_STRING(
    [--burn-backend=@<:@nautilus|libburn@:>@],
    [Specify which burning backend to use. @<:@default=nautilus@:>@]),
    [coaster_burn_backend=$enableval],
    [coaster_burn_backend=nautilus])

AC_MSG_CHECKING([which burning backend to use])

backend_pkgconfig_checks=
coaster_backend_defines=

case $coaster_burn_backend in
  nautilus) backend_pkgconfig_checks="libnautilus-burn >= $NAUTILUS_BURN_MIN_VERSION"
            coaster_backend_defines="-DCOASTER_BACKEND_NAUTILUS" ;;
  libburn)  backend_pkgconfig_checks="libburn-1 >= $LIBBURN_MIN_VERSION libisofs-1 >= $LIBBURN_MIN_VERSION libcoaster >= $LIBCOASTER_MIN_VERSION"
            coaster_backend_defines="-DCOASTER_BACKEND_LIBBURN" ;;
esac

AM_CONDITIONAL([NAUTILUS_BACKEND], [test x$coaster_burn_backend = xnautilus])
AM_CONDITIONAL([LIBBURN_BACKEND], [test x$coaster_burn_backend = xlibburn])
AC_SUBST([coaster_backend_defines])

AC_MSG_RESULT([${coaster_burn_backend}])
])
