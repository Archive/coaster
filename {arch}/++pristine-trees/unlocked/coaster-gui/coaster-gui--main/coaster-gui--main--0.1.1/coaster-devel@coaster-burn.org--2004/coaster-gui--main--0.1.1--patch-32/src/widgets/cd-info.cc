/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "widgets/cd-info.h"
#include "cstr-intl.h"

#include <gtkmm/label.h>
#include <gtkmm/progressbar.h>
#include <gtkmm/comboboxtext.h>

namespace Coaster
{

namespace Widgets
{

CDInfo::CDInfo()
: Gtk::HBox(false, 12), m_pProgLabel(0),
  m_pComboLabel(0), m_pProgress(0), m_pComboBox(0)
{
  using namespace Gtk;

  m_pComboLabel = new Label(_("<b>Size of CD:</b>"), 0.0, 0.5, false);
  m_pComboLabel->set_use_markup();
  pack_start(*manage(m_pComboLabel), false, false);

  m_pComboBox = new ComboBoxText();
  pack_start(*manage(m_pComboBox), false, false);

  m_pProgress = new ProgressBar();
  pack_end(*manage(m_pProgress), false, false);

  m_pProgLabel = new Label(_("<b>Space Used on CD:</b>"), 0.0, 0.5, false);
  m_pProgLabel->set_use_markup();
  pack_end(*manage(m_pProgLabel), false, false);
}

CDInfo::~CDInfo()
{}

void CDInfo::init(const std::vector<Glib::ustring>& sizes)
{
  for(std::vector<Glib::ustring>::const_iterator iter = sizes.begin() ; iter != sizes.end() ; ++iter)
  {
    m_pComboBox->append_text(_((*iter).c_str()));
  }
}

} // namespace Widgets

} // namespace Coaster
