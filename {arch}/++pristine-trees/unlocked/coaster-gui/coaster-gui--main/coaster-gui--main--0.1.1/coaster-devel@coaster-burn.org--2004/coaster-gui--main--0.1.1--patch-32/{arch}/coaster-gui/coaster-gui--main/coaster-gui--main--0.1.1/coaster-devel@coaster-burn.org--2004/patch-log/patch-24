Revision: coaster-gui--main--0.1.1--patch-24
Archive: coaster-devel@coaster-burn.org--2004
Creator: Bryan Forbes <bryan@reigndropsfall.net>
Date: Fri Nov  5 15:34:47 CST 2004
Standard-date: 2004-11-05 21:34:47 GMT
New-files: src/dialogs/.arch-ids/error-adding-files.cc.id
    src/dialogs/.arch-ids/error-adding-files.h.id
    src/dialogs/error-adding-files.cc
    src/dialogs/error-adding-files.h
Removed-files: src/dialogs/.arch-ids/layout-file-check.cc.id
    src/dialogs/.arch-ids/layout-file-check.h.id
    src/dialogs/layout-file-check.cc
    src/dialogs/layout-file-check.h
Modified-files: AUTHORS ChangeLog NEWS TODO configure.in
    data/Makefile.am data/coaster.schemas.in
    data/dtd/coaster-data.dtd
    data/glade/coaster-dialogs.glade
    data/glade/coaster-preferences.glade
    data/ui/coaster-data-menu.ui
    data/ui/coaster-edit-menu.ui data/ui/coaster-toolbar.ui
    pixmaps/Makefile.am po/POTFILES.in po/POTFILES.skip
    po/nl.po src/Makefile.am src/backends/cd-io.h
    src/backends/nautilus.cc src/data/data-cd-info.cc
    src/data/data-cd-info.h src/data/data-layout.cc
    src/data/data-layout.h src/data/data-view.cc
    src/data/data-view.h src/dialogs/Makefile.am
    src/dialogs/client-dialog.cc
    src/dialogs/client-dialog.h src/dialogs/dialog-util.cc
    src/dialogs/dialog-util.h src/dialogs/find.cc
    src/dialogs/glade-dialog.h src/dialogs/offer-save.cc
    src/dialogs/offer-save.h src/dialogs/preferences.cc
    src/dialogs/preferences.h src/dialogs/title-check.cc
    src/dialogs/title-check.h src/document.cc src/document.h
    src/instance-manager.cc src/instance-manager.h
    src/interface.cc src/interface.h src/layout.h
    src/sound.cc src/util.h src/view.cc src/view.h
    src/widgets/cd-info.cc
New-patches: bryan@reigndropsfall.net--2004/coaster-gui--bryan--0.1.1--patch-40
    bryan@reigndropsfall.net--2004/coaster-gui--bryan--0.1.1--patch-41
    bryan@reigndropsfall.net--2004/coaster-gui--bryan--0.1.1--patch-42
    bryan@reigndropsfall.net--2004/coaster-gui--bryan--0.1.1--patch-43
    coaster-devel@coaster-burn.org--2004/coaster-gui--main--0.1.1--patch-24
Summary: HIG love; changes for release 0.1.0.

2004-11-05  Bryan Forbes  <bryan@reigndropsfall.net>

	* NEWS:
	* TODO: updated.
	* configure.in: bumped version to 0.1.0.
	* data/glade/coaster-dialogs.glade: chaned to give focus to correct buttons
	in dialogs; added HIG compliant save changes dialog; changed Document to
	Layout in save changes dialog.
	* data/glade/coaster-preferences.glade: removed date format combo box.
	* data/ui/coaster-data-menu.ui: commented out cut and copy.
	* src/dialogs/dialog-util.[h|cc]: modified ui_offer_to_save_changes to use
	the new glade HIG dialog.
	* src/dialogs/offer-save.[h|cc]: modified to use the new glade dialog.
	* src/instance-manager.cc: removed the inclusion of offer-save.h.
	* src/interface.cc: made the open button important; changed to pass *this
	as a parameter to ui_offer_to_save_changes.

2004-11-05  Bryan Forbes  <bryan@reigndropsfall.net>

	* AUTHORS: changed my email address and added Andrew Johnson to list.
	* NEWS: updated for release 0.0.95.
	* TODO: updated to remove added features and add new TODOs.
	* configure.in: bumped version and version of n-c-b we depend on.
	* data/coaster.schemas.in: added sound_enable and match_case.
	* data/dtd/coaster-data.dtd: removed session, cdtext, and audio elements.
	* data/glade/coaster-dialogs.glade: removed :'s at the end of bold labels;
	changed labels and buttons to be HIG compliant; removed
	diag_layout_file_check; added a match case checkbutton on the find dialog;
	added a dialog to warn about deleting.
	* data/glade/coaster-preferences.glade: removed :'s at the end of bold
	labels; removed layout file check checkbutton; added sound enable
	checkbutton.
	* data/ui/coaster-edit-menu.ui:
	* data/ui/coaster-data-menu.ui: moved cut, copy, paste, undo, redo, clear,
	and select all to data menu; changed Add_Directory to Create_Directory;
	added a rename item; removed all mention of sessions.
	* po/nl.po: updated.
	* src/backends/cd-io.h:
	* src/backends/nautilus.cc:
	* src/document.[h|cc]: removed all references to sessions.
	* src/layout.h: added selection_changed signal; return & from get_tree();
	added support to match case in searching.
	* src/data/data-layout.[h|cc]: removed inclusion of dialogs/session-rem.h;
	added get_number_selected(); implemented searching with case matching; added
	rename support; added warning on delete; used new Util::get_win() to get
	toplevel widget.
	* src/view.[h|cc]: moved selection_changed signal to layout; removed all
	references to sessions.
	* src/data/data-view.[h|cc]: added DataLayout directly to this; added undo,
	redo, cut, copy, paste, clear, anded select all to here; removed all
	references to sessions; use new Util::get_win() to get toplevel widget;
	added accelerator to "Burn" action.
	* src/dialogs/Makefile.am: removed layout-file-check.[h|cc] and
	session-rem.[h|cc] from build.
	* src/dialogs/client-dialog.[h|cc]: added on_response override.
	* src/dialogs/dialog-util.[h|cc]: removed references to sessions.
	* src/dialogs/find.cc: connect match_case widget and load settings.
	* src/dialogs/glade-dialog.h: added specialization for Gtk::Dialog.
	* src/dialogs/preferences.[h|cc]: added sound_enable support.
	* src/dialogs/title-check.[h|cc]: added an image on the burn button.
	* src/instance-manager.h: removed inclusion of dialogs/session-rem.h.
	* src/interface.[h|cc]: removed init_view_signals and pre_init_view_signals
	since this is now handled from within the view; removed selection actions
	and all edit actions except preferences; removed on_layout_selection_changed
	and on_view_switch_page.
	* src/sound.cc: added check to see if the user wants sounds played.
	* src/util.h: added inline function to get the toplevel window of a widget.

2004-11-03  Bryan Forbes  <bryan@reigndropsfall.net>

	* src/widgets/cd-info.cc: string change.

2004-11-03  Bryan Forbes  <bryan@reigndropsfall.net>

	* NEWS:
	* configure.in: bumped version to 0.0.94.
	* po/POTFILES.in: added src/dialogs/preferences.cc and
	src/widgets/combo-cd-size.cc to the list.
	* data/glade/coaster-dialogs.glade:
	* src/dialogs/Makefile.am:
	* src/dialogs/error-adding-files.[h|cc]: added new dialog for errors adding
	files when there isn't enough space.
	* src/data/data-cd-info.[h|cc]: changed m_size_avail to m_cd_size; return a
	bool if we can't add the file.
	* src/data/data-layout.[h|cc]: moved get_selected_row_bools() functionality
	into get_new_entity_row(); removed get_selected_row_bools(); added
	functionality to check if we can add a file.
	* src/data/data-view.[h|cc]: return a bool from cd_size_add(); added
	function to check size of children; added removing size from m_pCDInfo when
	clearing a view.
	* src/dialogs/preferences.cc: set the title to "Coaster Preferences" to be
	HIG compliant.
