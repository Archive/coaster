/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_PREFS_LIST_H_
#define _COASTER_PREFS_LIST_H_

#include <gdkmm/pixbuf.h>
#include <gtkmm/treeview.h>
#include <gtkmm/treemodelcolumn.h>

namespace Gtk
{
class ListStore;
}

namespace Gnome
{
namespace Glade
{
class Xml;
}
}

namespace Coaster
{

namespace Widgets
{

class PrefsList : public Gtk::TreeView
{
public:
  PrefsList(BaseObjectType* cobject,
            const Glib::RefPtr<Gnome::Glade::Xml>& refGlade);
  virtual ~PrefsList();
  
  typedef sigc::signal<void, int> type_signal_category_changed;
  type_signal_category_changed& signal_category_changed();
  
  class PrefsColumns : public Gtk::TreeModelColumnRecord
  {
  public:
    PrefsColumns()
    {
      add(m_col_icon);
      add(m_col_name);
      add(m_col_id);
    }

    Gtk::TreeModelColumn<Glib::RefPtr<Gdk::Pixbuf> > m_col_icon;
    Gtk::TreeModelColumn<Glib::ustring> m_col_name;
    Gtk::TreeModelColumn<int> m_col_id;
  };

protected:
  // Signal Handlers
  void on_selection_changed();

  // Data funcs
  void icon_data_func(Gtk::CellRenderer* cell,
                      const Gtk::TreeModel::iterator& iter);

private:
  // Signals
  type_signal_category_changed signal_category_changed_;

  // Widgets
  PrefsColumns m_model_columns;
  Glib::RefPtr<Gtk::ListStore> m_refStore;
  Glib::RefPtr<Gtk::TreeSelection> m_refSelection;

  Column* m_colCategory;
};

} // namespace Widgets

} // namespace Coaster

#endif // _COASTER__H_
