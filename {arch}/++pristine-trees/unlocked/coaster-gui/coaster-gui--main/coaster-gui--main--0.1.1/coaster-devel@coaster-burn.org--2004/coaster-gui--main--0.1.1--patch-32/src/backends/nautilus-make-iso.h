/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_MAKE_ISO_NAUTILUS_H_
#define _COASTER_MAKE_ISO_NAUTILUS_H_

#include "sharedptr.h"

#include "data/data-prop.h"

#include <glibmm/main.h>
#include <glibmm/ustring.h>
#include <gtkmm/treemodel.h>

#include <libgnomevfsmm/types.h>

#include <fstream>

namespace Glib
{
class IOChannel;
} // namespace Glib

namespace Gtk
{
class TreeStore;
} // namespace Gtk;

namespace xmlpp
{
class Element;
} // namespace xmlpp

namespace Coaster
{

namespace IO
{

class MakeIso : public SharedObject
{
public:
  MakeIso(const std::string& iso_filename,
          const xmlpp::Element* rootNode,
          const Data::Properties& dp);
  virtual ~MakeIso();

  typedef sigc::signal<void,double> type_signal_progress_changed;
  typedef sigc::signal<void,const Glib::ustring&> type_signal_text_changed;

  type_signal_progress_changed& signal_progress_changed();
  type_signal_text_changed& signal_text_changed();

  unsigned long get_iso_filesize(int& exit_status);
  bool make_iso();
  void cancel();

protected:
  void parse_layout_to_graft_file(std::ostream& filestream,
                                  const xmlpp::Element* parent = 0,
                                  const Glib::ustring& prefix = Glib::ustring());
  Gnome::Vfs::FileSize get_free_space_on_tmp();
  
  bool stdout_read(Glib::IOCondition condition);
  bool stderr_read(Glib::IOCondition condition);
private:
  xmlpp::Element* m_pRootNode;
  Data::Properties m_DataProperties;

  type_signal_progress_changed m_signal_progress_changed_;
  type_signal_text_changed     m_signal_text_changed_;

  Glib::RefPtr<Glib::IOChannel> stdout_channel;
  Glib::RefPtr<Glib::IOChannel> stderr_channel;
  Glib::RefPtr<Glib::MainLoop> main_loop;

  std::string graft_filename;
  int fd_graft;
  int pid;

  std::string tmp_dir;

  std::string iso_filename;

  bool mkisofs_result;
};

} // namespace IO

} // namespace Coaster

#endif // _COASTER__H_
