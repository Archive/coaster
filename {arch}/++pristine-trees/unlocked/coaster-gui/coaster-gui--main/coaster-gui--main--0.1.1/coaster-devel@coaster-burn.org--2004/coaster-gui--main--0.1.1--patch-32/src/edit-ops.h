/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_EDIT_OPS_H_
#define _COASTER_EDIT_OPS_H_

#include "undo-op.h"

#include <gtkmm/treeiter.h>
#include <gtkmm/treestore.h>

#include <libgnomevfsmm/types.h>

#include <list>

namespace xmlpp
{
class Element;
} // namespace xmlpp

namespace Coaster
{

namespace Edit
{

void copy_data_row(const Gtk::TreeRow& row,
                   bool cut = false);
void copy_data_rows(const std::list<Gtk::TreeRow>& rows,
                    bool cut = false);

void paste_data_rows(const sigc::slot<void,const xmlpp::Element*,bool,bool>& add_slot);

} // namespace Edit
  
} // namespace Coaster

#endif // _COASTER_EDIT_OPS_H_
