/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "dialogs/dialog-util.h"

#include "cstr-intl.h"

#include "dialogs/chooser.h"
#include "dialogs/glade-dialog.h"
#include "dialogs/offer-save.h"
#include "dialogs/title-check.h"
#include "dialogs/remove-warn.h"
#include "dialogs/error-adding-files.h"
#include "dialogs/find.h"

#include <gtkmm/stock.h>

namespace Coaster
{

namespace Dialogs
{

std::string m_last_dir_ = "";
bool m_recursive_ = true;
  
type_listUStrings offer_add_files(Gtk::Window& parent,
                                  bool& hidden,
                                  int& response)
{
  std::auto_ptr<Chooser> chooser(new Chooser(parent, Dialogs::CHOOSER_TYPE_FILES));
  
  if(m_last_dir_ != "")
    chooser->set_current_folder(m_last_dir_);
  
  response = chooser->run();
  m_last_dir_ = chooser->get_current_folder();
  hidden = chooser->get_hidden_files();

  return chooser->get_filenames();
}

Glib::ustring offer_add_folder(Gtk::Window& parent,
                               bool& hidden,
                               bool& recursive,
                               int& response)
{
  Glib::ustring directory;
  std::auto_ptr<Chooser> chooser(new Chooser(parent, Dialogs::CHOOSER_TYPE_FOLDER));

  if(m_last_dir_ != "")
    chooser->set_current_folder(m_last_dir_);
  if(m_recursive_)
    chooser->set_recursive(m_recursive_);

  response = chooser->run();
  m_last_dir_ = chooser->get_current_folder();
  m_recursive_ = chooser->get_recursive();
  
  hidden = chooser->get_hidden_files();
  recursive = chooser->get_recursive();

  return Glib::filename_to_utf8(chooser->get_filename());
}

Bakery::App_WithDoc::enumSaveChanges ui_offer_to_save_changes(Gtk::Window& parent,
                                                              const std::string& filepath)
{
  std::auto_ptr<OfferSave> offer = GladeDialog<OfferSave>::create(parent, "diag_offer_save");
  offer->init(filepath);
  int int_result = offer->run();

  if(int_result == Gtk::RESPONSE_REJECT)
     return Bakery::App_WithDoc::SAVECHANGES_Discard;
  else if(int_result == Gtk::RESPONSE_ACCEPT)
     return Bakery::App_WithDoc::SAVECHANGES_Save;
  else
     return Bakery::App_WithDoc::SAVECHANGES_Cancel;
}

Glib::ustring ui_offer_open_files(Gtk::Window& parent)
{
  std::auto_ptr<Chooser> chooser(new Chooser(parent, Dialogs::CHOOSER_TYPE_OPEN));

  int response = chooser->run();

  if(response != Gtk::RESPONSE_CANCEL)
    return chooser->get_uri();
  else
    return Glib::ustring();
}

void ui_offer_open_iso(Gtk::Window& parent, int& response, std::string& filename)
{
  Gtk::FileChooserDialog fc(parent, _("Open an ISO"));
  
  fc.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
  fc.add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);
  fc.set_default_response(Gtk::RESPONSE_OK);

  Gtk::FileFilter* pIsoFiles = new Gtk::FileFilter();
  pIsoFiles->set_name(_("ISO Images"));
  pIsoFiles->add_mime_type("application/x-cd-image");

  Gtk::FileFilter* pAllFiles = new Gtk::FileFilter();
  pAllFiles->set_name(_("All Files"));
  pAllFiles->add_pattern("*");

  fc.add_filter(*Gtk::manage(pIsoFiles));
  fc.add_filter(*Gtk::manage(pAllFiles));

  response = fc.run();
  filename = fc.get_filename();
}

void ui_offer_export_iso(Gtk::Window& parent, int& response, std::string& filename)
{
  Gtk::FileChooserDialog fc(parent, _("Export to..."), Gtk::FILE_CHOOSER_ACTION_SAVE);
  
  fc.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
  fc.add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);
  fc.set_default_response(Gtk::RESPONSE_OK);

  Gtk::FileFilter* pIsoFiles = new Gtk::FileFilter();
  pIsoFiles->set_name(_("ISO Images"));
  pIsoFiles->add_mime_type("application/x-cd-image");

  Gtk::FileFilter* pAllFiles = new Gtk::FileFilter();
  pAllFiles->set_name(_("All Files"));
  pAllFiles->add_pattern("*");

  fc.add_filter(*Gtk::manage(pIsoFiles));
  fc.add_filter(*Gtk::manage(pAllFiles));

  response = fc.run();
  filename = fc.get_filename();
}

Glib::ustring ui_find(Gtk::Window& parent)
{
  Glib::ustring search_string = "";

  std::auto_ptr<Find> find = GladeDialog<Find>::create(parent, "diag_find");
  int result = find->run();

  switch(result)
  {
    case Gtk::RESPONSE_OK:
      search_string = find->get_search_string();
      break;
    case Gtk::RESPONSE_CLOSE:
    default:
      break;
  }

  return search_string;
}

int ui_warn_title_check(Gtk::Window& parent)
{
  std::auto_ptr<TitleCheck> titlecheck = 
    GladeDialog<TitleCheck>::create(parent, "diag_title_check");
  return titlecheck->run();
}

int ui_warn_remove_files(Gtk::Window& parent)
{
  std::auto_ptr<RemoveWarn> remove_warn = 
    GladeDialog<RemoveWarn>::create(parent, "diag_remove_warn");
  remove_warn->set_default_response(Gtk::RESPONSE_OK);
  return remove_warn->run();
}

void ui_error_add_files(Gtk::Window& parent,
                        const std::list<Glib::ustring>& files)
{
  std::auto_ptr<ErrorAddingFiles> erraddfiles = 
    GladeDialog<ErrorAddingFiles>::create(parent, "diag_err_add_files");
  erraddfiles->set_files(files);
  erraddfiles->run();
}

} // namespace Dialogs

} // namespace Coaster
