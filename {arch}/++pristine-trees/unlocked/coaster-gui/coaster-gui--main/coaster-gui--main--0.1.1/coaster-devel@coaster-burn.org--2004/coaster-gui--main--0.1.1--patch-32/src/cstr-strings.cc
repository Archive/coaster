/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "cstr-strings.h"

#include <glib/gutils.h>

namespace Coaster
{

const char *const glade_main_filename = COASTER_GLADEDIR G_DIR_SEPARATOR_S "coaster.glade";
const char *const glade_dialogs_filename = COASTER_GLADEDIR G_DIR_SEPARATOR_S "coaster-dialogs.glade";
const char *const glade_prefs_filename = COASTER_GLADEDIR G_DIR_SEPARATOR_S "coaster-preferences.glade";
  
const Glib::ustring COASTER_GCONFDIR = "/apps/coaster";

} // namespace Coaster
