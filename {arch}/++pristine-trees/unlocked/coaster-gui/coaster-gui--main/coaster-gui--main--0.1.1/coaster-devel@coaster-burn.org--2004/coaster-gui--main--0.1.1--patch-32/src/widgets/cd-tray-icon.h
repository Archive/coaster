/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_TRAY_ICON_H_
#define _COASTER_TRAY_ICON_H_

#include "eggmm/tray-icon.h"

#include <gtkmm/tooltips.h>

namespace Gdk
{
class Pixbuf;
} // namespace Gdk

namespace Gtk
{
class EventBox;
class Image;
} // namespace Gtk

namespace Coaster
{

namespace Widgets
{

class CDTrayIcon : public Egg::TrayIcon
{
public:
  explicit CDTrayIcon(const Glib::RefPtr<Gdk::Pixbuf>& pixbuf);
  virtual ~CDTrayIcon();

  void set_pixbuf(const Glib::RefPtr<Gdk::Pixbuf>& pixbuf);
  void set_tip(const Glib::ustring& tip, const Glib::ustring& private_tip = Glib::ustring());

protected:
private:
  Gtk::EventBox* m_pEventBox;
  Gtk::Image*    m_pImage;
  Gtk::Tooltips  m_Tooltips;
};

} // namespace Widgets

} // namespace Coaster

#endif // _COASTER__H_
