/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "data/data-cd-info.h"

#include "cstr-intl.h"
#include "cstr-debug.h"

#include "util.h"
#include "ucompose.h"

#include <gtkmm/label.h>
#include <gtkmm/progressbar.h>
#include <gtkmm/comboboxtext.h>

#include <libglademm/xml.h>
#include <libgnomevfsmm/utils.h>

#include <iostream>
#include <sstream>


namespace
{

void change_prog_text(Gtk::ProgressBar* prog,
                      Gnome::Vfs::FileSize used,
                      Gnome::Vfs::FileSize avail)
{
  prog->set_text(Gnome::Vfs::format_file_size_for_display(used) + " / " + Gnome::Vfs::format_file_size_for_display(avail));
  //prog->set_text(Gnome::Vfs::format_file_size_for_display(avail - used));
}
  
} // anonymous namespace

namespace Coaster
{

namespace Data
{

//////////////////////////////////////////////////////////////////////////////////////////
//  Coaster::Data::CDInfo                                                               //
//////////////////////////////////////////////////////////////////////////////////////////

CDInfo::CDInfo()
: Widgets::CDInfo(), m_cd_size(0), m_size_used(0)
{
  using namespace Gtk;
  
  std::vector<Glib::ustring> sizes;
  sizes.push_back(N_("650 MB"));
  sizes.push_back(N_("700 MB"));
  init(sizes);

  change_prog_text(m_pProgress, m_size_used, m_cd_size);
  m_pComboBox->signal_changed().connect(sigc::mem_fun(*this, &CDInfo::on_combo_box_changed));
}

CDInfo::~CDInfo()
{}

bool CDInfo::cd_size_add(Gnome::Vfs::FileSize size)
{
  if(size > (m_cd_size - m_size_used))
  {
    //error
    return false;
  }

  m_size_used += size;
  update_progress_bar();

  return true;
}

void CDInfo::cd_size_remove(Gnome::Vfs::FileSize size)
{
  m_size_used -= size;
  update_progress_bar();
}

CDInfo::type_signal_cd_size_changed& CDInfo::signal_cd_size_changed()
{
  return signal_cd_size_changed_;
}

Gnome::Vfs::FileSize CDInfo::get_size_used() const
{
  return m_size_used;
}

CDSizeType CDInfo::get_cd_size() const
{
  int active_row = m_pComboBox->get_active_row_number();

  switch(active_row)
  {
    case 0:
    default:
      return COASTER_CD_SIZE_650;
    case 1:
      return COASTER_CD_SIZE_700;
  }
}

void CDInfo::set_cd_size(CDSizeType size)
{
  debug(String::ucompose("CDInfo: set_cd_size -> %1", size));
  switch(size)
  {
    case COASTER_CD_SIZE_NONE:
      m_pComboBox->set_active(Util::get_int("layouts/default_size"));
      return;
    case COASTER_CD_SIZE_650:
      m_pComboBox->set_active(0);
      return;
    case COASTER_CD_SIZE_700:
      m_pComboBox->set_active(1);
      return;
  }
}

//////////////////////////////////////////////////////////////////////////////////////////
//  Coaster::CDInfo -- protected                                                   //
//////////////////////////////////////////////////////////////////////////////////////////

void CDInfo::on_combo_box_changed()
{
  switch(m_pComboBox->get_active_row_number())
  {
    case 0:
    default:
      m_cd_size = 650 * 1024 * 1024;
      update_progress_bar();
      signal_cd_size_changed().emit(COASTER_CD_SIZE_650);
      debug("CDInfo: changed to 650 MB");
      break;
    case 1:
      m_cd_size = 700 * 1024 * 1024;
      update_progress_bar();
      signal_cd_size_changed().emit(COASTER_CD_SIZE_700);
      debug("CDInfo: changed to 700 MB");
      break;
  }
}

void CDInfo::update_progress_bar()
{
  m_pProgress->set_fraction(((long double)m_size_used) / ((long double)m_cd_size));
  change_prog_text(m_pProgress, m_size_used, m_cd_size);
}

//////////////////////////////////////////////////////////////////////////////////////////
//  Coaster::CDInfo -- private                                                     //
//////////////////////////////////////////////////////////////////////////////////////////

} // namespace Data

} // namespace Coaster
