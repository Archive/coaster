/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_COMBO_SPEED_H_
#define _COASTER_COMBO_SPEED_H_

#include <gtkmm/combobox.h>
#include <gtkmm/treemodel.h>

namespace Gtk
{
class ListStore;
} // namespace Gtk

namespace Coaster
{

namespace Widgets
{

class ComboSpeed : public Gtk::ComboBox
{
public:
  ComboSpeed();
  ~ComboSpeed();

  int get_selected_real_speed() const;
  void populate_speed(int max_speed);

  class ComboColumns : public Gtk::TreeModel::ColumnRecord
  {
  public:
    ComboColumns()
    {
      add(m_col_speed);
      add(m_col_real_speed);
    }

    Gtk::TreeModelColumn<Glib::ustring> m_col_speed;
    Gtk::TreeModelColumn<int> m_col_real_speed;
  };

protected:
  void set_stored_speed(const Glib::ustring& key);
  
private:
  Glib::RefPtr<Gtk::ListStore> m_refListModel;
};

} // namespace Widgets
  
} // namespace Coaster

#endif // _COASTER_COMBO_SPEED_H_
