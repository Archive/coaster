/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "widgets/view-base.h"

#include "instance-manager.h"
#include "undo-manager.h"

#include <gtkmm/menu.h>
#include <gtkmm/actiongroup.h>

namespace Coaster
{

namespace Widgets
{

View_Base::View_Base()
: instmgr(InstanceMgr::instance()), m_pMenu_Popup(0),
  m_refViewActionGroup(Gtk::ActionGroup::create("CoasterViewSpecificActions"))
{
  undo_manager = UndoManagerPtr(new UndoManager());
  undo_manager->signal_changed().connect(sigc::mem_fun(*this, &View_Base::on_undo_manager_changed));
}

View_Base::~View_Base()
{}

Glib::ustring View_Base::get_ui_location() const
{
  return Glib::ustring();
}

Glib::RefPtr<Gtk::ActionGroup> View_Base::get_ui_action_group() const
{
  return m_refViewActionGroup;
}

void View_Base::set_popup_menu(Gtk::Menu* popup_menu)
{
  m_pMenu_Popup = popup_menu;
}

void View_Base::popup(guint button,
                      guint32 activate_time)
{
  m_pMenu_Popup->popup(button, activate_time);
}

/*void View_Base::set_document(Document* pDocument)
{
  m_pDocument = pDocument;
  if(m_pDocument)
    m_pDocument->signal_forget().connect( sigc::mem_fun(*this, &type_self::on_document_forget) );
}*/

void View_Base::on_burn_clicked()
{}

void View_Base::on_layout_modified(bool modified)
{}

void View_Base::on_layout_selection_changed(int number_selected)
{}

void View_Base::on_undo_manager_changed()
{}

void View_Base::enable_selection_actions(bool enable)
{}

void View_Base::enable_layout_item_actions(bool enable)
{}

} // namespace Widgets

} // namespace Coaster
