/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "data/data-layout.h"

#include "cstr-intl.h"
#include "cstr-debug.h"

#include "instance-manager.h"
#include "edit-ops.h"
#include "xml-io.h"

#include "data/data-row.h"
#include "data/data-file.h"
#include "data/data-utils.h"
#include "data/data-undo-ops.h"

#include "dialogs/dialog-util.h"

#include "widgets/cellrenderer-ellipsize.h"
#include "widgets/cellrenderer-icontext.h"

#include "util.h"

#include <gdk/gdkkeysyms.h>
#include <gtk/gtkdnd.h>

#include <gtkmm/icontheme.h>
#include <gtkmm/radiobutton.h>
#include <gtkmm/entry.h>
#include <gtkmm/treemodel.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/stock.h>

#include <libgnomevfsmm/handle.h>
#include <libgnomevfsmm/directory-handle.h>
#include <libgnomevfsmm/mime-handlers.h>
#include <libgnomevfsmm/utils.h>

#include <bakery/Utilities/BusyCursor.h>

#include <libxml++/document.h>
#include <libxml++/parsers/domparser.h>

#include "ucompose.h"

#include <vector>
namespace // Anonymous namespace
{

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
Coaster::EntityType entity_type_from_mime(Glib::ustring mime_type)
{
  using namespace Coaster;
  EntityType result = COASTER_ENTITY_FILE;

  if(mime_type == "application/ogg" || mime_type == "application/x-ogg" ||
     mime_type == "audio/mpeg" || mime_type == "audio/x-mp3" ||
     mime_type == "application/x-flac" || mime_type == "audio/x-flac" ||
     mime_type == "application/x-wav" || mime_type == "audio/x-wav")
  {
    result = COASTER_ENTITY_AUDIO;
  }
  else
  {
    result = COASTER_ENTITY_FILE;
  }

  return result;
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
Coaster::Data::Layout::type_listUStrings uri_list_extract_uris(const Glib::ustring& uri_list)
{
	/* Note that this is mostly very stolen from nautilus's libnautilus-private/nautilus-icon-dnd.c */

	const gchar *p, *q;

  std::list<Glib::ustring> result;

	//g_return_val_if_fail (uri_list != NULL, NULL);

	p = uri_list.c_str();

	/* We don't actually try to validate the URI according to RFC
	 * 2396, or even check for allowed characters - we just ignore
	 * comments and trim whitespace off the ends.  We also
	 * allow LF delimination as well as the specified CRLF.
	 */
	while (p != NULL) {
		if (*p != '#') {
			while (g_ascii_isspace (*p))
				p++;

			q = p;
			while ((*q != '\0')
			       && (*q != '\n')
			       && (*q != '\r'))
				q++;

			if (q > p) {
				q--;
				while (q > p
				       && g_ascii_isspace (*q))
					q--;
          result.push_back(Glib::ustring(p, q+1));
			}
		}
		p = strchr (p, '\n');
		if (p != NULL)
			p++;
	}

	return result;
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
int sort_name(const Gtk::TreeModel::iterator& a,
              const Gtk::TreeModel::iterator& b)
{
  using namespace Coaster;
  const Data::Columns& model_columns = Data::columns();

  bool a_isdir = Data::is_tree_dir(a);
  bool b_isdir = Data::is_tree_dir(b);

  if(a_isdir && b_isdir)
  {
    const std::string a_str = (*a).get_value(model_columns.m_col_name).collate_key();
    const std::string b_str = (*b).get_value(model_columns.m_col_name).collate_key();
    return a_str.compare(b_str);
  }
  else if(a_isdir && !b_isdir)
  {
    return -1;
  }
  else if(!a_isdir && b_isdir)
  {
    return 1;
  }
  else
  {
    const std::string a_str = (*a).get_value(model_columns.m_col_name).collate_key();
    const std::string b_str = (*b).get_value(model_columns.m_col_name).collate_key();
    return a_str.compare(b_str);
  }
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
int sort_size(const Gtk::TreeModel::iterator& a,
              const Gtk::TreeModel::iterator& b)
{
  using namespace Coaster;
  const Data::Columns& model_columns = Data::columns();

  bool a_isdir = Data::is_tree_dir(a);
  bool b_isdir = Data::is_tree_dir(b);

  if(a_isdir && b_isdir)
  {
    int a_childsize = (*a).children().size();
    int b_childsize = (*b).children().size();

    if(a_childsize > b_childsize)
      return -1;
    if(a_childsize < b_childsize)
      return 1;
    else
      return 0;
  }
  else if(a_isdir && !b_isdir)
  {
    return -1;
  }
  else if(!a_isdir && b_isdir)
  {
    return 1;
  }
  else
  {
    Gnome::Vfs::FileSize a_int = (*a).get_value(model_columns.m_col_size);
    Gnome::Vfs::FileSize b_int = (*b).get_value(model_columns.m_col_size);
    if(a_int > b_int)
      return -1;
    else if( a_int < b_int )
      return 1;
    else
      return 0;
  }
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
int sort_uri(const Gtk::TreeModel::iterator& a,
             const Gtk::TreeModel::iterator& b)
{
  using namespace Coaster;
  const Data::Columns& model_columns = Data::columns();

  bool a_isdir = Data::is_tree_dir(a);
  bool b_isdir = Data::is_tree_dir(b);

  if(a_isdir && b_isdir)
  {
    const std::string a_str = (*a).get_value(model_columns.m_col_uri).collate_key();
    const std::string b_str = (*b).get_value(model_columns.m_col_uri).collate_key();
    return a_str.compare(b_str);
  }
  else if(a_isdir && !b_isdir)
  {
    return -1;
  }
  else if(!a_isdir && b_isdir)
  {
    return 1;
  }
  else
  {
    const std::string a_str = (*a).get_value(model_columns.m_col_uri).collate_key();
    const std::string b_str = (*b).get_value(model_columns.m_col_uri).collate_key();
    return a_str.compare(b_str);
  }
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
Gnome::Vfs::FileSize size_of_children(const Gtk::TreeNodeChildren& children)
{
  using namespace Coaster;
  Gnome::Vfs::FileSize size = 0;

  for(Gtk::TreeIter iter = children.begin() ; iter != children.end() ; ++iter)
  {
    if(Data::is_tree_dir(iter))
    {
      size += size_of_children((*iter).children());
    }
    else
    {
      const Data::Columns& model_columns = Data::columns();
      size += (*iter).get_value(model_columns.m_col_size);
    }
  }

  return size;
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
Gnome::Vfs::FileSize size_of_children(const std::list<Gtk::TreeRow>& rows)
{
  using namespace Coaster;
  Gnome::Vfs::FileSize size = 0;

  for(std::list<Gtk::TreeRow>::const_iterator i = rows.begin() ; i != rows.end() ; ++i)
  {
    if(Data::is_tree_dir((*i)))
    {
      size += size_of_children((*i).children());
    }
    else
    {
      size += (*i).get_value(Data::columns().m_col_size);
    }
  }

  return size;
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void remove_children(const Gtk::TreeRow& row,
                     const sigc::slot<void, const Gnome::Vfs::FileSize&>& slot_cd_remove)
{
  using namespace Coaster;
  
  for( Gtk::TreeIter iter = row.children().begin() ; iter != row.children().end() ; ++iter )
  {
    if(Data::is_tree_dir(iter))
    {
      remove_children(*iter, slot_cd_remove);
    }
    else
    {
      const Data::Columns& model_columns = Data::columns();
      slot_cd_remove((*iter).get_value(model_columns.m_col_size));
    }
  }
}

}

namespace Coaster
{

namespace Data
{

//////////////////////////////////////////////////////////////////////////////////////////
//  Coaster::Data::Layout                                                               //
//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
Layout::Layout(const sigc::slot<void,guint,guint32>& slot_popup,
               const sigc::slot<bool,const Gnome::Vfs::FileSize&>& slot_cd_size_add,
               const sigc::slot<void,const Gnome::Vfs::FileSize&>& slot_cd_size_remove,
               const sigc::slot<void,const UndoOpPtr&>& slot_push_undo)
: type_base(), popup(slot_popup), cd_size_add(slot_cd_size_add),
  cd_size_remove(slot_cd_size_remove), push_undo(slot_push_undo),
  instmgr(InstanceMgr::instance())
{
  init();

  show_all_children();
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
Layout::~Layout()
{
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::add_from_xml(const xmlpp::Element* rootNode,
                              bool respect_ids,
                              bool undo)
{
  if(!respect_ids)
  {
    type_listTP list_paths = m_selection_->get_selected_rows();
    std::list<Gtk::TreeRow> rows;

    if(list_paths.size() > 0)
    {
      Gtk::TreeRow row = *(m_refStore->get_iter(list_paths.back()));
      XmlIO::tree_branch_from_xml(rows, row, rootNode, m_refStore, cd_size_add);
    }
    else
    {
      Gtk::TreeNodeChildren children = m_refStore->children();
      XmlIO::tree_branch_from_xml(rows, children, rootNode, m_refStore, cd_size_add);
    }

    if(undo)
      push_undo(SharedPtr<AddOp>(new AddOp(rows,
                                           _("Paste"),
                                           sigc::mem_fun(*this, &Layout::remove_from_xml),
                                           sigc::mem_fun(*this, &Layout::add_from_xml))));
  }
  else
  {
    XmlIO::add_back_from_xml(rootNode, m_refStore, cd_size_add);
  }
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::add_from_xml_dnd(const xmlpp::Element* rootNode,
                                  const Gtk::TreePath& path)
{
  std::list<Gtk::TreeRow> rows;
  Gtk::TreeNodeChildren children = m_refStore->children();
  XmlIO::tree_branch_from_xml(rows, children, rootNode, m_refStore, cd_size_add);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::remove_from_xml(const xmlpp::Element* rootNode)
{
  std::list<Gtk::TreeRow> rows = XmlIO::remove_from_xml(rootNode, m_refStore);

  erase_rows(rows);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_clear()
{
  debug("Layout: Clear clicked");
  cd_size_remove(size_of_children(m_refStore->children()));
  push_undo(SharedPtr<RemoveOp>(new RemoveOp(m_refStore->children(),
                                             _("Clear"),
                                             sigc::mem_fun(*this, &Layout::add_from_xml),
                                             sigc::mem_fun(*this, &Layout::remove_from_xml))));
  type_base::on_clear();
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_cut()
{
  copy_selected(true);

  using namespace Gtk;

  type_listTP list_paths = m_selection_->get_selected_rows();
  if(list_paths.size() > 0) // only do this if we have rows selected
  {
    std::list<Gtk::TreeRow> rows = remove_duplicate_children(list_paths);
    
    push_undo(SharedPtr<RemoveOp>(new RemoveOp(rows,
                                               _("Cut"),
                                               sigc::mem_fun(*this, &Layout::add_from_xml),
                                               sigc::mem_fun(*this, &Layout::remove_from_xml))));
    erase_rows(rows);

    signal_modified_.emit(true); // alert signal handlers we changed
  }
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_copy()
{
  copy_selected();
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_paste()
{
  Edit::paste_data_rows(sigc::mem_fun(*this, &Layout::add_from_xml));
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_search_clicked()
{
  Glib::ustring search_string = Dialogs::ui_find(Util::get_win(this));

  if(search_string != "")
    search(search_string);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_add_files()
{
  debug("Layout: on add files");
  bool hidden;
  int response;

  Dialogs::type_listUStrings files = Dialogs::offer_add_files(Util::get_win(this), hidden, response);
  add_files_from_list(files, hidden);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_add_folder()
{
  using namespace Gtk;
  
  debug("Layout: on add folder");
  bool hidden, recursive;
  int response;
  
  Glib::ustring folder = Dialogs::offer_add_folder(Util::get_win(this), hidden, recursive, response);

  if(response != RESPONSE_OK)
    return;

  Bakery::BusyCursor cursor(Util::get_win(this));

  // Add files from folder
  Glib::ustring::size_type posLastSlash = folder.find_last_of("/");
  Glib::ustring folder_name;
  if(posLastSlash == Glib::ustring::npos)
    folder_name = "/";
  else
    folder_name = folder.substr(posLastSlash+1);

  TreePath parent_path = get_new_entity_row();

  Data::Row parent_row(Gtk::TreeRowReference(m_refStore, parent_path), folder_name);
  Data::new_row(parent_row, m_refStore);
  expand_to_created_path(parent_path);

  bool changed = false;
  type_listUStrings unable_to_add;

  add_to_folder_from_dir(parent_row.rowref, folder, recursive, hidden, changed, unable_to_add);

  if(changed)
  {
    std::list<Gtk::TreeRow> row;
    row.push_back(*(m_refStore->get_iter(parent_path)));
    push_undo(SharedPtr<AddOp>(new AddOp(row,
                                         _("Add Directory"),
                                         sigc::mem_fun(*this, &Layout::remove_from_xml),
                                         sigc::mem_fun(*this, &Layout::add_from_xml))));
    signal_modified_.emit(true);
  }

  if(!unable_to_add.empty())
  {
    Dialogs::ui_error_add_files(Util::get_win(this), unable_to_add);
  }
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_create_directory()
{
  using namespace Gtk;

  TreePath path = get_new_entity_row();

  Data::Row row(Gtk::TreeRowReference(m_refStore, path), _("untitled folder"));
  Data::new_row(row, m_refStore);

  expand_to_created_path(path);

  std::list<Gtk::TreeRow> rows;
  rows.push_back(*(m_refStore->get_iter(path)));
  push_undo(SharedPtr<AddOp>(new AddOp(rows,
                                       _("Create Directory"),
                                       sigc::mem_fun(*this, &Layout::remove_from_xml),
                                       sigc::mem_fun(*this, &Layout::add_from_xml))));
  signal_modified_.emit(true);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_rename_selected()
{
  type_listTP list_paths = m_selection_->get_selected_rows();
  if(list_paths.size() == 1) // only do this if there is only one row selected
  {
    static_cast<Widgets::CellRendererIconText*>(m_colName->get_first_cell_renderer())->property_editable() = true;
    set_cursor(*(list_paths.begin()), *m_colName, true);
  }
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_remove_selected()
{
  using namespace Gtk;

  int warn_response = Dialogs::ui_warn_remove_files(Util::get_win(this));

  if(warn_response != Gtk::RESPONSE_OK)
    return;

  type_listTP list_paths = m_selection_->get_selected_rows();
  if(list_paths.size() > 0)
  {
    std::list<Gtk::TreeRow> rows = remove_duplicate_children(list_paths);
    
    push_undo(SharedPtr<RemoveOp>(new RemoveOp(rows,
                                               _("Remove"),
                                               sigc::mem_fun(*this, &Layout::add_from_xml),
                                               sigc::mem_fun(*this, &Layout::remove_from_xml))));
    erase_rows(rows);

    signal_modified_.emit(true); // alert signal handlers we changed
  }
}

//////////////////////////////////////////////////////////////////////////////////////////
//  Coaster::Layout -- protected                                               //
//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::init()
{
  using namespace Gtk;

  set_enable_search(false);

  init_columns();
  init_drag_n_drop();

  // Signals
  m_selection_->signal_changed().connect(
      sigc::mem_fun(*this, &Layout::on_tree_selection_changed));
  m_refStore->signal_row_changed().connect(
      sigc::mem_fun(*this, &Layout::on_row_changed));
  m_refStore->signal_row_deleted().connect(
      sigc::mem_fun(*this, &Layout::on_row_deleted));
  m_refStore->signal_row_inserted().connect(
      sigc::mem_fun(*this, &Layout::on_row_inserted));
  m_refStore->signal_rows_reordered().connect(
      sigc::mem_fun(*this, &Layout::on_rows_reordered));

  instmgr->signal_icon_theme_changed().connect(sigc::mem_fun(*this, &Layout::on_icon_theme_changed));
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::init_columns()
{
  using namespace Gtk;

  const Data::Columns& model_columns = Data::columns();

  m_refStore->set_sort_func(model_columns.m_col_name, sigc::ptr_fun(&sort_name));
  m_refStore->set_sort_func(model_columns.m_col_size, sigc::ptr_fun(&sort_size));
  m_refStore->set_sort_func(model_columns.m_col_uri, sigc::ptr_fun(&sort_uri));
  m_refStore->set_sort_column(model_columns.m_col_name, SORT_ASCENDING);

  if(debug())
  {
    Column* colID = new Column(_("ID"));
    append_column(*manage(colID));

    CellRendererText *const cell_text = new CellRendererText();
    cell_text->property_editable() = false;
    colID->pack_start(*manage(cell_text));
    colID->add_attribute(cell_text->property_text(), model_columns.m_col_id);
  }
    
  {
    m_colName = new Column(_("Name"));
    append_column(*manage(m_colName));

    set_expander_column(*m_colName);

    // sorting
    m_colName->set_sort_column(model_columns.m_col_name);

    Widgets::CellRendererIconText *const cell_icontext = new Widgets::CellRendererIconText();

    cell_icontext->property_editable() = false;
    cell_icontext->signal_edited().connect(sigc::mem_fun(*this, &Layout::on_text_edited));
    cell_icontext->signal_editing_canceled().connect(sigc::mem_fun(*this, &Layout::on_text_editing_canceled));

    m_colName->pack_start(*manage(cell_icontext));

    m_colName->add_attribute(cell_icontext->property_text(), model_columns.m_col_name);
    m_colName->set_cell_data_func(*cell_icontext, sigc::mem_fun(*this, &Layout::icon_cell_data_func));
    m_colName->set_resizable(true);
  }

  {
    m_colSize = new Column(_("Size"));
    append_column(*manage(m_colSize));

    // sorting
    m_colSize->set_sort_column(model_columns.m_col_size);

    CellRendererText *const cell_size = new CellRendererText();
    //CellRendererFileSize *const cell_size = new CellRendererFileSize();
    m_colSize->pack_start(*manage(cell_size));
    cell_size->property_xalign() = 1.0;

    m_colSize->add_attribute(cell_size->property_text(), model_columns.m_col_size);
    m_colSize->set_cell_data_func(*cell_size, sigc::mem_fun(*this, &Layout::size_cell_data_func));
    m_colSize->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
    m_colSize->set_resizable(true);
  }

  {
    m_colUri = new Column(_("Location"));
    append_column(*manage(m_colUri));

    // sorting
    m_colUri->set_sort_column(model_columns.m_col_uri);

    Widgets::CellRendererEllipsize *const cell_uri = new Widgets::CellRendererEllipsize();
    m_colUri->pack_start(*manage(cell_uri));

    m_colUri->add_attribute(cell_uri->property_text(), model_columns.m_col_uri);
    m_colUri->set_sizing(Gtk::TREE_VIEW_COLUMN_AUTOSIZE);
  }
  
  set_headers_clickable(true);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::init_drag_n_drop()
{
  // Drag and drop
  std::vector<Gtk::TargetEntry> target_list_source;
  std::vector<Gtk::TargetEntry> target_list_dest;
  
  target_list_source.push_back(Gtk::TargetEntry("application/x-coaster/data-row"));

  target_list_dest.push_back(Gtk::TargetEntry("application/x-coaster/data-row"));
  target_list_dest.push_back(Gtk::TargetEntry("text/uri-list"));

  //enable_model_drag_source();
  //enable_model_drag_dest();
  enable_model_drag_source(target_list_source, Gdk::MODIFIER_MASK, Gdk::ACTION_COPY|Gdk::ACTION_MOVE);
  enable_model_drag_dest(target_list_dest, Gdk::ACTION_COPY|Gdk::ACTION_MOVE);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::search(const Glib::ustring& search_string)
{
  bool match_case = Util::get_bool("search/match_case");

  grab_focus();
  m_selection_->unselect_all();
  
  if(match_case)
    m_refStore->foreach_iter(sigc::bind(sigc::mem_fun(*this, &Layout::on_search_foreach_case), search_string));
  else
    m_refStore->foreach_iter(sigc::bind(sigc::mem_fun(*this, &Layout::on_search_foreach_nocase), search_string));
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
bool Layout::on_search_foreach_case(const Gtk::TreeIter& iter,
                                        const Glib::ustring& search_string)
{
  if(search_string == "")
    return true;

  const Data::Columns& columns = Data::columns();

  if(((*iter).get_value(columns.m_col_name)).find(search_string) != Glib::ustring::npos)
    m_selection_->select(iter);

  return false;
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
bool Layout::on_search_foreach_nocase(const Gtk::TreeIter& iter,
                                          const Glib::ustring& search_string)
{
  if(search_string == "")
    return true;

  const Data::Columns& columns = Data::columns();
  Glib::ustring tmp_cell_name = ((*iter).get_value(columns.m_col_name)).lowercase();

  if(tmp_cell_name.find(search_string.lowercase()) != Glib::ustring::npos)
    m_selection_->select(iter);

  return false;
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
bool Layout::on_button_press_event(GdkEventButton* event)
{
  using namespace Gtk;

  if(event->type == GDK_BUTTON_PRESS)
  {
    TreePath path;
    TreeViewColumn *column;
    int cell_x, cell_y;

    bool over_row = get_path_at_pos(static_cast<int>(event->x), static_cast<int>(event->y),
                                    path, column, cell_x, cell_y);

    if(event->button == 3)
    {
      if(!over_row)
        m_selection_->unselect_all();
      else
      {
        if(m_selection_->count_selected_rows() == 0)
          m_selection_->select(path);
        else if(!m_selection_->is_selected(path))
        {
          m_selection_->unselect_all();
          m_selection_->select(path);
        }
      }

      popup(event->button, event->time);

      return true;
    }
    else if(event->button == 1)
    {
      if(!over_row)
        m_selection_->unselect_all();
      return TreeView::on_button_press_event(event);
    }
  }
  return TreeView::on_button_press_event(event);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
bool Layout::on_key_press_event(GdkEventKey* event)
{
  if(m_selection_->count_selected_rows() == 1)
  {
    if(event->type == GDK_KEY_PRESS)
    {
      if(event->keyval == GDK_Return)
      {
        Gtk::TreePath path = *(m_selection_->get_selected_rows().begin());
        Gtk::TreeRow row = *(m_refStore->get_iter(path));
        if(!row.children().empty())
        {
          if(row_expanded(path))
            collapse_row(path);
          else
            expand_row(path, false);

          return true;
        }
      }
    }
  }
  
  return TreeView::on_key_press_event(event);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_tree_selection_changed()
{
  using namespace Gtk;
  signal_selection_changed().emit(m_selection_->count_selected_rows());
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_row_changed(const Gtk::TreePath& path,
                                const Gtk::TreeIter& iter)
{
  signal_modified_.emit(true);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_row_deleted(const Gtk::TreePath& path)
{
  signal_modified_.emit(true);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_row_inserted(const Gtk::TreePath& path,
                                 const Gtk::TreeIter& iter)
{
  signal_modified_.emit(true);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_rows_reordered(const Gtk::TreePath& path,
                                   const Gtk::TreeIter& iter,
                                   int* new_order)
{
  signal_modified_.emit(true);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_drag_begin(const Glib::RefPtr<Gdk::DragContext>& context)
{
  debug_dnd("Layout::on_drag_begin");
  TreeView::on_drag_begin(context);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_drag_leave(const Glib::RefPtr<Gdk::DragContext>& context,
                               guint time)
{
  debug_dnd("Layout::on_drag_leave");
  TreeView::on_drag_leave(context, time);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
bool Layout::on_drag_motion(const Glib::RefPtr<Gdk::DragContext>& context,
                                int x,
                                int y,
                                guint time)
{
  debug_dnd("Layout::on_drag_motion");

  return TreeView::on_drag_motion(context, x, y, time);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
bool Layout::on_drag_drop(const Glib::RefPtr<Gdk::DragContext>& context,
                              int x,
                              int y,
                              guint time)
{
  debug_dnd("Layout::on_drag_drop");

  // if we're dropping on the same widget the drag is coming from
  if(this == Gtk::Widget::drag_get_source_widget(context))
  {
    Gtk::TreePath drop_path;
    Gtk::TreeViewDropPosition drop_pos;
    
    bool over_row = get_dest_row_at_pos(x, y, drop_path, drop_pos);

    Gtk::TreePath orig_path = *(m_selection_->get_selected_rows().begin());

    if(over_row)
    {
      // if the drop path is the same as the source path
      if(orig_path == drop_path)
      {
        debug_dnd("drop false");
        context->drop_reply(false, time);
        return false;
      }

      Gtk::TreeRow drop_row = *(m_refStore->get_iter(drop_path));
      if(Data::is_tree_dir(drop_row))
      {
        // if we're dropping into the directory
        if(drop_pos == Gtk::TREE_VIEW_DROP_INTO_OR_BEFORE ||
           drop_pos == Gtk::TREE_VIEW_DROP_INTO_OR_AFTER)
        {
          Gtk::TreePath orig_path_parent = orig_path;
          orig_path_parent.up();

          // if we're dropping into the same parent of the source
          if(!orig_path_parent.empty() && (orig_path_parent == drop_path))
          {
            debug_dnd("drop false");
            context->drop_reply(false, time);
            return false;
          }
          
          Gtk::TreePath drop_path_parent = drop_path;
          drop_path_parent.up();

          // if we're dropping into itself
          if(!drop_path_parent.empty() && orig_path == drop_path_parent)
          {
            debug_dnd("drop false");
            context->drop_reply(false, time);
            return false;
          }
        }
        else // if we're dropping before or after the directory
        {
          Gtk::TreePath orig_path_parent = orig_path;
          Gtk::TreePath drop_path_parent = drop_path;
          
          orig_path_parent.up();
          drop_path_parent.up();

          // if we're dropping into the same directory the source is in
          if(orig_path_parent == drop_path_parent)
          {
            debug_dnd("drop false");
            context->drop_reply(false, time);
            return false;
          }
        }
      }
      else // if we're dropping on a file
      {
        if(orig_path.is_ancestor(drop_path))
        {
          debug_dnd("drop false");
          context->drop_reply(false, time);
          return false;
        }
      }
    }
    else // if we're dropping into a blank area
    {
      Gtk::TreePath orig_parent_path = orig_path;
      orig_parent_path.up();

      if(orig_parent_path.empty()) // if the original row is already at the toplevel
      {
        debug_dnd("drop false");
        context->drop_reply(false, time);
        return false;
      }
    }
  }

  debug_dnd("drop true");
  return TreeView::on_drag_drop(context, x, y, time);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_drag_data_get(const Glib::RefPtr<Gdk::DragContext>& context,
                                  Gtk::SelectionData& selection_data,
                                  guint info,
                                  guint time)
{
  debug_dnd("Layout::on_drag_data_get");
  debug_dnd("Get target: ", selection_data.get_target());

  std::list<Gtk::TreeRow> rows = remove_duplicate_children(m_selection_->get_selected_rows());
  xmlpp::Document doc;
  xmlpp::Element* rootNode = doc.create_root_node("coaster-drag-rows");

  XmlIO::rows_to_xml(rows, rootNode, true, true);

  Glib::ustring string = doc.write_to_string();

  selection_data.set(selection_data.get_target(), 8, (const guchar*)string.c_str(), string.size());

  TreeView::on_drag_data_get(context, selection_data, info, time);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_drag_data_received(const Glib::RefPtr<Gdk::DragContext>& context,
                                       int x,
                                       int y,
                                       const Gtk::SelectionData& selection_data,
                                       guint info,
                                       guint time)
{
  debug_dnd("Layout::on_drag_data_received");
  debug_dnd("Recieved target: ", selection_data.get_target());


  Gtk::TreeRow drop_parent;
  
  if(selection_data.get_target() == "application/x-coaster/data-row")
  {
    debug_dnd("Recieved XML: ", selection_data.get_data_as_string());
    Gtk::TreePath drop_path;
    Gtk::TreeViewDropPosition drop_pos;

    bool row_exists = get_dest_row_at_pos(x, y, drop_path, drop_pos);
    Gtk::TreeNodeChildren drop_children = get_drop_children(row_exists, drop_path, drop_pos);

    xmlpp::DomParser parser;
    try
    {
      parser.parse_memory(selection_data.get_data_as_string());
    }
    catch(const xmlpp::exception& e)
    {
      debug("Layout::on_drag_data_recieved: exception: ", e.what());
      return TreeView::on_drag_data_received(context, x, y, selection_data, info, time);
    }
    
    xmlpp::Document* doc = parser.get_document();

    Gdk::DragAction action = context->get_action();

    if(this == Gtk::Widget::drag_get_source_widget(context))
    {
      XmlIO::tree_branch_from_xml(drop_children,
                                  doc->get_root_node(),
                                  m_refStore, cd_size_add, (action == Gdk::ACTION_MOVE));
      context->drag_finish(true, (action == Gdk::ACTION_MOVE), time);
      return;
    }
    else
    {
      XmlIO::tree_branch_from_xml(drop_children, doc->get_root_node(), m_refStore, cd_size_add);
      context->drag_finish(true, (action == Gdk::ACTION_MOVE), time);
      return;
    }
  }
  else if(selection_data.get_target() == "text/uri-list")
  {
    debug_dnd("Recieved URIs: ", selection_data.get_data_as_string());
   
    //std::list<Glib::ustring> uri_list = uri_list_extract_uris(selection_data.get_data_as_string());

    context->drag_finish(false, false, time);
    return;
  }

  TreeView::on_drag_data_received(context, x, y, selection_data, info, time);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_drag_data_delete(const Glib::RefPtr<Gdk::DragContext>& context)
{
  debug_dnd("Layout::on_drag_data_delete");
  TreeView::on_drag_data_delete(context);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_drag_end(const Glib::RefPtr<Gdk::DragContext>& context)
{
  debug_dnd("Layout::on_drag_end");
  TreeView::on_drag_end(context);
}


//////////////////////////////////////////////////////////////////////////////////////////
//  Coaster::Layout -- private                                                 //
//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::icon_cell_data_func(Gtk::CellRenderer* cell,
                                     const Gtk::TreeModel::iterator& iter)
{
  const Data::Columns& columns = Data::columns();
  Widgets::CellRendererIconText& renderer = dynamic_cast<Widgets::CellRendererIconText&>(*cell);

  renderer.property_pixbuf() = (*iter).get_value(columns.m_col_ref_pixbuf);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::size_cell_data_func(Gtk::CellRenderer* cell,
                                     const Gtk::TreeModel::iterator& iter)
{
  using namespace Gtk;

  CellRendererText& renderer = dynamic_cast<CellRendererText&>(*cell);
  if(Data::is_tree_dir(iter))
  {
    int num_children = (*iter).children().size();
    renderer.property_text() = String::ucompose((num_children == 1 ? "%1 item" : "%1 items"), num_children);
  }
  else
  {
    const Data::Columns& model_columns = Data::columns();
    
    Gnome::Vfs::FileSize cell_file_size = (*iter).get_value(model_columns.m_col_size);
    renderer.property_text() = Gnome::Vfs::format_file_size_for_display(cell_file_size);
  } 
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::do_nothing()
{
  debug("This does nothing!");
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_text_edited(const Glib::ustring& path_text,
                                const Glib::ustring& new_text)
{
  using namespace Gtk;
  
  const Data::Columns& model_columns = Data::columns();

  TreeRow row = *(m_refStore->get_iter(path_text));
  Glib::ustring previous_name = row.get_value(model_columns.m_col_name);

  if(row.get_value(model_columns.m_col_name) != new_text)
  {
    debug(String::ucompose("Edited: From %1 to %2", previous_name, new_text));

    push_undo(SharedPtr<RenameOp>(new RenameOp(row, previous_name,
                                               new_text, m_refStore)));
    row[model_columns.m_col_name] = new_text;
    signal_modified_.emit(true);
  }

  static_cast<Widgets::CellRendererIconText*>(m_colName->get_first_cell_renderer())->property_editable() = false;
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_text_editing_canceled()
{
  static_cast<Widgets::CellRendererIconText*>(m_colName->get_first_cell_renderer())->property_editable() = false;
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::add_files_from_list(const type_listUStrings& files,
                                 bool hidden)
{
  using namespace Gnome::Vfs;
  Bakery::BusyCursor cursor(Util::get_win(this));

  type_listUStrings unable_to_add;

  std::list<Gtk::TreeRow> rows;
  bool changed = false;
  for(type_listUStrings::const_iterator iter = files.begin() ; iter != files.end() ; ++iter)
  {
    File data_file(Glib::filename_to_utf8(*iter));

    if(data_file.is_valid())
    {
      Glib::ustring tmpName = data_file.get_name();
      Glib::ustring firstChar = tmpName.substr(0,1);

      if(hidden == false && firstChar == ".")
        continue;

      if(cd_size_add(data_file.get_size()))
      {
        changed = true;

        Gtk::TreePath path = get_new_entity_row();
        rows.push_back(*(m_refStore->get_iter(path)));

        Data::Row row(Gtk::TreeRowReference(m_refStore, path),
                      tmpName, data_file.get_size(),
                      COASTER_ENTITY_FILE, *iter,
                      data_file.get_mime_type());

        debug("TreePath view-layout: ",path.to_string());
        Data::new_row(row, m_refStore);

        expand_to_created_path(path);
      }
      else
      {
        unable_to_add.push_back(*iter);
      }
    }
  }

  if(changed)
  {
    push_undo(SharedPtr<AddOp>(new AddOp(rows,
                                         _("Add Files"),
                                         sigc::mem_fun(*this, &Layout::remove_from_xml),
                                         sigc::mem_fun(*this, &Layout::add_from_xml))));
    signal_modified_.emit(true);
  }

  if(!unable_to_add.empty())
  {
    Dialogs::ui_error_add_files(Util::get_win(this), unable_to_add);
  }
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::add_to_folder_from_dir(const Gtk::TreeRowReference& parent_row,
                                        const Glib::ustring& folder,
                                        bool recurse,
                                        bool hidden,
                                        bool& changed,
                                        type_listUStrings& unable_to_add)
{
  using namespace Gtk;
  bool file_exists = true;
  
  Gnome::Vfs::DirectoryHandle dhandle;
  dhandle.open(Util::create_full_uri(folder), COMMON_INFO_OPTIONS);

  Glib::RefPtr<Gnome::Vfs::FileInfo> info = dhandle.read_next(file_exists);
  while(file_exists)
  {
    Glib::ustring new_row_name = info->get_name();
    if(hidden == false && new_row_name.substr(0,1) == ".")
    {
      info = dhandle.read_next(file_exists);
      continue;
    }

    Glib::ustring mime_type = info->get_mime_type();
    if(mime_type != "x-directory/normal")
    {
      if(cd_size_add(info->get_size()))
      {
        TreePath child_path = get_new_entity_row(parent_row.get_path());

        Data::Row child_row(Gtk::TreeRowReference(m_refStore, child_path),
                            new_row_name, info->get_size(), COASTER_ENTITY_FILE,
                            folder + "/" + new_row_name, mime_type);

        Data::new_row(child_row, m_refStore);
        expand_to_created_path(child_path);

        if(!changed)
          changed = true;
      }
      else
      {
        unable_to_add.push_back(folder + "/" + info->get_name());
      }
    }
    else
    {
      if(recurse)
      {
        if(new_row_name != "." && new_row_name != "..")
        {
          TreePath new_folder_path = get_new_entity_row(parent_row.get_path());

          Data::Row folder_row(Gtk::TreeRowReference(m_refStore, new_folder_path), new_row_name);
          Data::new_row(folder_row, m_refStore);
          if(!changed)
            changed = true;

          add_to_folder_from_dir(folder_row.rowref, folder + "/" + new_row_name, recurse, hidden, changed, unable_to_add);
        }
      }
    }

    info = dhandle.read_next(file_exists);
  }
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::copy_selected(bool cut)
{
  type_listTP list_paths = m_selection_->get_selected_rows();
  
  if(list_paths.size() == 1)
  {
    Edit::copy_data_row(*(m_refStore->get_iter(list_paths.front())), cut);
  }
  else if(list_paths.size() > 0)
  {
    std::list<Gtk::TreeRow> rows = remove_duplicate_children(list_paths);
    
    Edit::copy_data_rows(rows, cut);
  }
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::select_all()
{
  m_selection_->select_all();
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
Gtk::TreePath Layout::get_new_entity_row()
{
  type_listTP list_paths = m_selection_->get_selected_rows();
  Gtk::TreeRow new_row;
 
  if(list_paths.size() > 0) // if a row or more is selected
  {
    Gtk::TreeRow selected_row = *(m_refStore->get_iter(list_paths.back()));
    if(Data::is_tree_dir(selected_row))
      new_row = *(m_refStore->append(selected_row.children()));
    else
      new_row = *(m_refStore->insert_after(selected_row));
  }
  else
  {
    new_row = *(m_refStore->append());
  }

  return m_refStore->get_path(new_row);
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
Gtk::TreePath Layout::get_new_entity_row(const Gtk::TreePath& path)
{
  Gtk::TreeRow new_row;
 
  Gtk::TreeRow selected_row = *(m_refStore->get_iter(path));
  new_row = *(m_refStore->append(selected_row.children()));

  return m_refStore->get_path(new_row);
}

void Layout::expand_to_created_path(const Gtk::TreePath& path)
{
  Gtk::TreePath temp_path = path;
  if(temp_path.up())
  {
    if(!row_expanded(temp_path))
    {
      expand_row(temp_path, true);
    }
  }
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
std::list<Gtk::TreeRow> Layout::remove_duplicate_children(const type_listTP& paths)
{
  std::list<Gtk::TreeRow> rows;
  type_listTP paths_check = paths;

  if(paths.size() > 0)
  {
    for(type_listTP::const_iterator i = paths.begin() ; i != paths.end() ; ++i)
    {
      bool add = true;
      for(type_listTP::iterator j = paths_check.begin() ; j != paths_check.end() ; ++j)
      {
        if((*i) == (*j))
          continue;

        if((*i).is_descendant((*j)))
        {
          add = false;
          break;
        }
      }

      if(add)
        rows.push_back(*(m_refStore->get_iter(*i)));
    }
  }

  return rows;
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::erase_rows(const std::list<Gtk::TreeRow>& rows)
{
  cd_size_remove(size_of_children(rows));

  for(std::list<Gtk::TreeRow>::const_reverse_iterator i = rows.rbegin() ; i != rows.rend() ; ++i)
  {
    m_refStore->erase((*i));
  }
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
Gtk::TreeNodeChildren Layout::get_drop_children(bool exists,
                                                    const Gtk::TreePath& path,
                                                    const Gtk::TreeViewDropPosition& pos)
{
  if(exists)
  {
    Gtk::TreeRow drop_row = *(m_refStore->get_iter(path));
    if(Data::is_tree_dir(drop_row))
    {
      if(pos == Gtk::TREE_VIEW_DROP_INTO_OR_BEFORE ||
         pos == Gtk::TREE_VIEW_DROP_INTO_OR_AFTER)
      {
        return drop_row.children();
      }
      else
      {
        return (*(drop_row.parent())).children();
      }
    }
    else
    {
      return (*(drop_row.parent())).children();
    }
  }

  return m_refStore->children();
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
void Layout::on_icon_theme_changed()
{
  m_refStore->foreach_iter(sigc::mem_fun(*this, &Layout::icon_theme_changed_foreach));
}

//////////////////////////////////////////////
// Function: //
// Purpose: //
// Input: //
// Output: //
//////////////////////////////////////////////
bool Layout::icon_theme_changed_foreach(const Gtk::TreeModel::iterator& iter)
{
  const Data::Columns& columns = Data::columns();
  (*iter)[columns.m_col_ref_pixbuf] = instmgr->lookup_icon((*iter).get_value(columns.m_col_mime_type));
  return false;
}

} // namespace Data

} // namespace Coaster
