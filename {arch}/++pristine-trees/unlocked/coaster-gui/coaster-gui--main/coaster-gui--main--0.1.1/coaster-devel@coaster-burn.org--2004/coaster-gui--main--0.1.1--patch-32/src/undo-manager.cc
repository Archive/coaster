/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "undo-manager.h"

#include "ucompose.h"
#include "util.h"

namespace Coaster
{

//////////////////////////////////////////////////////
//  UndoStack
/////////////////////////////////////////////////////

UndoManager::UndoStack::~UndoStack()
{
  clear();
}

void UndoManager::UndoStack::clear()
{
  impl_stack.clear();
}

// returns number of removed items
UndoManager::UndoStack::size_type UndoManager::UndoStack::truncate(size_type max_size)
{
  if(size() <= max_size)
    return 0;

  size_type num_truncated = size() - max_size;

  impl_stack_t::iterator del_begin = impl_stack.begin();
  impl_stack_t::iterator del_end = del_begin + num_truncated;
  impl_stack.erase(del_begin, del_end);

  return num_truncated;
}

UndoManager::UndoStack::value_t UndoManager::UndoStack::top() const
{
  return impl_stack.back();
}

void UndoManager::UndoStack::pop()
{
  impl_stack.erase(--impl_stack.end());
}

void UndoManager::UndoStack::push(const value_t &value)
{
  impl_stack.push_back(value);
}

bool UndoManager::UndoStack::empty() const
{
  return !impl_stack.size();
}

UndoManager::UndoStack::size_type UndoManager::UndoStack::size() const
{
  return impl_stack.size();
}


/////////////////////////////////////////////////
//    UndoManager
/////////////////////////////////////////////////

UndoManager::UndoManager()
: origin(0), origin_valid(true)
{}

UndoManager::~UndoManager()
{}

void UndoManager::push(const UndoOpPtr& op)
{
  // Redo stack is invalidated
  redo_stack.clear();

  push_undo(op);
  m_signal_changed_.emit();
}

void UndoManager::undo()
{
  if(!undo_stack.size())
    return;
    
  UndoOpPtr top_op = undo_stack.top();
  undo_stack.pop();
    
  top_op->undo();

  push_redo(top_op);
  m_signal_changed_.emit();

  if (origin_valid && origin == undo_stack.size())
    m_signal_origin_reached_.emit();
}

void UndoManager::redo()
{
  if(!redo_stack.size())
    return;

  UndoOpPtr top_op = redo_stack.top();
  redo_stack.pop();

  top_op->redo();

  push_undo(top_op);
  m_signal_changed_.emit();

  if(origin_valid && origin == undo_stack.size())
    m_signal_origin_reached_.emit();
}

UndoManager::UndoStack::size_type UndoManager::undo_size() const
{
  return undo_stack.size();
}

UndoManager::UndoStack::size_type UndoManager::redo_size() const
{
  return redo_stack.size();
}

Glib::ustring UndoManager::undo_label() const
{
  if(undo_size())
    return String::ucompose(" %1", undo_stack.top()->get_label());
  else
    return Glib::ustring();
}

Glib::ustring UndoManager::redo_label() const
{
  if(redo_size())
    return String::ucompose(" %1", redo_stack.top()->get_label());
  else
    return Glib::ustring();
}

void UndoManager::set_origin()
{
  origin = undo_stack.size();
  origin_valid = true;
}

UndoManager::type_signal_changed& UndoManager::signal_changed()
{
  return m_signal_changed_;
}

UndoManager::type_signal_origin_reached& UndoManager::signal_origin_reached()
{
  return m_signal_origin_reached_;
}

void UndoManager::push_undo(const UndoOpPtr& op)
{
  undo_stack.push(op);
  truncate_undo_stack();
}

void UndoManager::push_redo(const UndoOpPtr& op)
{
  redo_stack.push(op);
}

void UndoManager::truncate_undo_stack()
{
  UndoStack::size_type num_truncated = undo_stack.truncate(Util::get_int("undo_actions_limit"));

  if(origin_valid)
    if(num_truncated > origin)
      origin_valid = false;
    else
      origin -= num_truncated;
}

} // namespace Coaster
