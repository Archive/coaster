#ifndef _COASTER_MEDIA_FILE_H_
#define _COASTER_MEDIA_FILE_H_

#include "cstr-common.h"
#include <glibmm.h>
#include <libgnomevfsmm.h>

namespace Coaster
{

class MediaFile
{
public:
  explicit MediaFile(const Glib::ustring& uri);
  virtual ~MediaFile();

  bool is_valid();
  Glib::ustring get_info_name();
  Glib::ustring get_title();
  Glib::ustring get_duration_string();
  double get_duration_secs();

protected:

private:
  Gnome::Vfs::Handle m_handle;
  const Glib::ustring m_uri;
  Glib::ustring m_mime_type;
  Glib::ustring m_info_name;
  MediaType m_MediaType;

  Glib::ustring m_title;
  Glib::ustring m_duration_string;
  double m_duration_secs;
};
  
} // namespace Coaster

#endif // _COASTER_MEDIA_FILE_H_
