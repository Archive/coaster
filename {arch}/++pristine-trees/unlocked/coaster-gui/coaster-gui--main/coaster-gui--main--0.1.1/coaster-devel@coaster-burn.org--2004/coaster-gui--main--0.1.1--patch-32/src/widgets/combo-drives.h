/* Copyright (C) 2002-2004  The Coaster Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _COASTER_COMBO_DRIVES_H_
#define _COASTER_COMBO_DRIVES_H_

#include "sharedptr.h"

#include <gtkmm/combobox.h>
#include <gtkmm/treemodel.h>

namespace burn
{
extern "C"
{
struct burn_drive_info;
struct burn_drive;
}
}

namespace Gtk
{
class ListStore;
} // namespace Gtk

namespace Coaster
{

class InstanceMgr;
  
class ComboDrives : public Gtk::ComboBox
{
public:
  typedef sigc::slot<void, burn::burn_drive*, const Glib::ustring&> type_slot_speed_pop;

  explicit ComboDrives(bool writers_only,
                       const Glib::ustring& drive_key);
  explicit ComboDrives(bool writers_only,
                       const Glib::ustring& drive_key,
                       type_slot_speed_pop& slot);
  ~ComboDrives();

  void init(bool writers_only);

  bool is_selected_erasable() const;
  bool is_selected_burnable() const;
  Glib::ustring get_selected_name() const;
  burn::burn_drive_info* get_selected_drive_info() const;

  class ComboColumns : public Gtk::TreeModel::ColumnRecord
  {
  public:
    ComboColumns()
    {
      add(m_col_name);
      add(m_col_drive_info);
    }

    Gtk::TreeModelColumn<Glib::ustring> m_col_name;
    Gtk::TreeModelColumn<burn::burn_drive_info*> m_col_drive_info;
  };

protected:
  void on_changed();
  void set_active_drive();
  
private:
  Glib::RefPtr<Gtk::ListStore> m_refListModel;
  burn::burn_drive_info* drives;
  unsigned int n_drives;
  burn::burn_drive* current_drive;

  type_slot_speed_pop speed_pop;
  Glib::ustring key;

  SharedPtr<InstanceMgr> instmgr;
};
  
} // namespace Coaster

#endif // _COASTER_COMBO_DRIVES_H_
