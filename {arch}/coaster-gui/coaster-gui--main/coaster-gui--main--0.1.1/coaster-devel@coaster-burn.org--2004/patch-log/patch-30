Revision: coaster-gui--main--0.1.1--patch-30
Archive: coaster-devel@coaster-burn.org--2004
Creator: Bryan Forbes <bryan@reigndropsfall.net>
Date: Tue Nov 16 16:32:52 CST 2004
Standard-date: 2004-11-16 22:32:52 GMT
New-files: src/.arch-ids/undo-manager.cc.id
    src/.arch-ids/undo-manager.h.id
    src/.arch-ids/undo-op.h.id
    src/data/.arch-ids/data-undo-ops.cc.id
    src/data/.arch-ids/data-undo-ops.h.id
    src/data/data-undo-ops.cc src/data/data-undo-ops.h
    src/dialogs/.arch-ids/about.cc.id
    src/dialogs/.arch-ids/about.h.id
    src/dialogs/.arch-ids/remove-warn.cc.id
    src/dialogs/.arch-ids/remove-warn.h.id
    src/dialogs/about.cc src/dialogs/about.h
    src/dialogs/remove-warn.cc src/dialogs/remove-warn.h
    src/undo-manager.cc src/undo-manager.h src/undo-op.h
Modified-files: ChangeLog NEWS configure.in
    data/coaster.schemas.in
    data/glade/coaster-dialogs.glade
    data/glade/coaster-preferences.glade
    data/glade/coaster.glade data/ui/coaster-data-menu.ui
    macros/cxx.m4 po/POTFILES.in po/POTFILES.skip po/nl.po
    src/Makefile.am src/backends/Makefile.am
    src/data/Makefile.am src/data/data-columns.h
    src/data/data-layout.cc src/data/data-layout.h
    src/data/data-utils.cc src/data/data-utils.h
    src/data/data-view.cc src/data/data-view.h
    src/dialogs/Makefile.am src/dialogs/dialog-util.cc
    src/dialogs/dialog-util.h src/dialogs/find.cc
    src/dialogs/find.h src/dialogs/preferences.cc
    src/edit-ops.cc src/edit-ops.h src/instance-manager.cc
    src/instance-manager.h src/interface.cc src/interface.h
    src/main.cc src/util.cc src/util.h
    src/widgets/Makefile.am
    src/widgets/combo-layout-type.cc src/xml-io.cc
    src/xml-io.h
New-patches: bryan@reigndropsfall.net--2004/coaster-gui--bryan--0.1.1--patch-52
    bryan@reigndropsfall.net--2004/coaster-gui--bryan--0.1.1--patch-53
    bryan@reigndropsfall.net--2004/coaster-gui--bryan--0.1.1--patch-54
    coaster-devel@coaster-burn.org--2004/coaster-gui--main--0.1.1--patch-30
Summary: Changes for 0.1.2; added undo/redo!

2004-11-16  Bryan Forbes  <bryan@reigndropsfall.net>

	* NEWS: bumped to version 0.1.2.
	* configure.in: bumped to version 0.1.2; removed dependency on libgnomeuimm;
	added check for hash_map.
	* macros/cxx.m4: added check for hash_map.
	* data/coaster.schemas.in: added key for remove warning.
	* data/glade/coaster-dialogs.glade: changed "Delete" to "Remove" where
	necesary for consistency; added check button for the remove check.
	* data/glade/coaster-preferences.glade: added check button for new gconf
	key.
	* data/glade/coaster.glade: changed status_bar to a GtkStatusBar.
	* data/ui/coaster-data-menu.ui: uncommented undo and redo.
	
	* po/POTFILES.in:
	* po/POTFILES.skip:
	* po/nl.po: updated

	* src/Makefile.am: added undo classes and new hash_map define.
	* src/edit-ops.[h|cc]: changed to pass in if the operation is a cut or not;
	updated add_from_xml.
	* src/instance-manager.[h|cc]: changed from using a std::map to using a
	__gnu_cxx::hash_map if the user has it.
	* src/interface.[h|cc]: changed to use our About dialog; changed labels of
	some actions; changed to use Gtk::StatusBar instead of Gnome::UI::AppBar.
	* src/main.cc: removed libgnomeuimm references.
	* src/util.[h|cc]: removed lookup_icon.
	* src/undo-manager.[h|cc]:
	* src/undo-op.h: added classes to handle undo/redo.
	* src/xml-io.[h|cc]: added functions to handle undo/redo.
	
	* src/data/Makefile.am:
	* src/data/data-undo-ops.[h|cc]: added classes to implement undo.
	* src/data/data-columns.h: added column for id.
	* src/data/data-layout.[h|cc]: implemented undo/redo; remove references to
	libgnomeuimm; added slot to push items onto the undo stack; used new
	add_back_from_xml and remove_from_xml functions; put copy code into
	copy_selected(); implemented undo on rename.
	* src/data/data-utils.[h|cc]: added parameter to new_row to support ids;
	implement incrementing the column row; added set_name function.
	* src/data/data-view.[h|cc]: added UndoManager instance; added undo and redo
	actions.

	* src/dialogs/Makefile.am: added new dialogs to build.
	* src/dialogs/about.[h|cc]: added class to handle About dialog to get rid of
	Gnome::UI::About.
	* src/dialogs/remove-warn.[h|cc]: moved delete-warn.* to here and renamed
	for consitancy.
	* src/dialogs/dialog-util.[h|cc]: added more methods to pop up dialogs
	easily.
	* src/dialogs/find.[h|cc]: changed Gnome::UI::Entry to Gtk::Entry.
	* src/dialogs/preferences.cc: changed gconf key and glade widget for the
	remove warn checkbox.

	* src/widgets/combo-layout-type.[h|cc]: removed references to libgnomeuimm.
