## $Id$
##
## Copyright (c) 2002  Daniel Elstner  <daniel.elstner@gmx.net>
## Changed to fit Coaster by Bryan Forbes  <bryan@reigndropsfall.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License VERSION 2 as
## published by the Free Software Foundation.  You are not allowed to
## use any other version of the license; unless you got the explicit
## permission from the author to do so.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


## COASTER_CXX_HAS_STD_LOCALE()
##
## Check whether the C++ environment supports std::locale.
## If true, #define COASTER_HAVE_STD_LOCALE 1.
##
AC_DEFUN([COASTER_CXX_HAS_STD_LOCALE],
[
AC_CACHE_CHECK(
  [whether the C++ library supports std::locale],
  [coaster_cv_cxx_has_std_locale],
[
  AC_LANG_PUSH([C++])
  AC_LINK_IFELSE(
  [
    AC_LANG_PROGRAM(
    [[
      #include <iostream>
      #include <locale>
    ]],[[
      std::cout.imbue(std::locale(""));
    ]])
  ],
    [coaster_cv_cxx_has_std_locale=yes],
    [coaster_cv_cxx_has_std_locale=no])
  AC_LANG_POP([C++])
])

if test "x$coaster_cv_cxx_has_std_locale" = xyes; then
{
  AC_DEFINE([COASTER_HAVE_STD_LOCALE], [1], [Define to 1 if the C++ library supports std::locale.])
}
fi
])

## COASTER_CXX_FILEBUF_CHECK()
##
## Check whether __gnu_cxx::stdio_filebuf takes a boolean argument.
## If false, #define COASTER_NEW_STDIO_FILEBUF 1.
##
AC_DEFUN([COASTER_CXX_FILEBUF_CHECK],
[
AC_CACHE_CHECK(
  [if libstdc++ accepts a boolean to __gnu_cxx::stdio_filebuf],
  [coaster_cv_cxx_stdio_filebuf],
[
  AC_LANG_PUSH([C++])
  AC_LINK_IFELSE(
  [
    AC_LANG_PROGRAM(
    [[
      #include <iostream>
      #include <ext/stdio_filebuf.h>
    ]],[[
      __gnu_cxx::stdio_filebuf<char> buffer(0, std::ios::out, false, static_cast<size_t>(BUFSIZ));
    ]])
  ],
    [coaster_cv_cxx_stdio_filebuf=yes],
    [coaster_cv_cxx_stdio_filebuf=no])
  AC_LANG_POP([C++])
])

if test "x$coaster_cv_cxx_stdio_filebuf" = xno; then
{
  AC_DEFINE([COASTER_NEW_STDIO_FILEBUF], [1], [Define to 1 if libstdc++ doesn't have a boolean in __gnu_cxx::stdio_filebuf])
}
fi
])

## COASTER_CXX_HAS_HASH_MAP()
##
## Check whether __gnu_cxx::hash_map exists.
## If true, #define COASTER_HAS_HASH_MAP 1.
##
AC_DEFUN([COASTER_CXX_HAS_HASH_MAP],
[
AC_CACHE_CHECK(
  [if libstdc++ defines __gnu_cxx::hash_map],
  [coaster_cv_cxx_has_hash_map],
[
  AC_LANG_PUSH([C++])
  AC_LINK_IFELSE(
  [
    AC_LANG_PROGRAM(
    [[
      #include <iostream>
      #include <ext/hash_map>

      struct eqstr
      {
        bool operator()(const char* s1, const char* s2) const
        {
          return strcmp(s1, s2) == 0;
        }
      };
    ]],[[
      __gnu_cxx::hash_map<const char*, int, __gnu_cxx::hash<const char*>, eqstr> months;
    ]])
  ],
    [coaster_cv_cxx_has_hash_map=yes],
    [coaster_cv_cxx_has_hash_map=no])
  AC_LANG_POP([C++])
])

if test "x$coaster_cv_cxx_has_hash_map" = xyes; then
{
  AC_DEFINE([COASTER_HAS_HASH_MAP], [1], [Define to 1 if libstdc++ defines __gnu_cxx::hash_map])
  coaster_hash_map_define="-DCOASTER_HAS_HASH_MAP"
  AC_SUBST([coaster_hash_map_define])
}
fi
])

## COASTER_ARG_ENABLE_WARNINGS()
##
## Provide the --enable-warnings configure argument, set to 'minimum'
## by default.
##
AC_DEFUN([COASTER_ARG_ENABLE_WARNINGS],
[
AC_REQUIRE([AC_PROG_CXX])

AC_ARG_ENABLE([warnings], AC_HELP_STRING(
  [--enable-warnings=@<:@none|minimum|maximum|hardcore@:>@],
  [Control compiler pickyness. @<:@default=minimum@:>@]),
  [coaster_enable_warnings=$enableval],
  [coaster_enable_warnings=minimum])

AC_MSG_CHECKING([for compiler warning flags to use])

warning_flags=

case $coaster_enable_warnings in
  minimum|yes) warning_flags='-Wall' ;;
  maximum)     warning_flags='-pedantic -W -Wall' ;;
  hardcore)    warning_flags='-pedantic -W -Wall -Werror' ;;
esac

tested_flags=

if test "x$warning_flags" != x; then
{
  AC_LANG_PUSH([C++])
  AC_LANG_CONFTEST([AC_LANG_SOURCE([[int foo() { return 0; }]])])
  conftest_source="conftest.${ac_ext:-cc}"

  for flag in $warning_flags
  do
    # Test whether the compiler accepts the flag.  GCC doesn't bail
    # out when given an unsupported flag but prints a warning, so
    # check the compiler output instead.
    coaster_cxx_out=`$CXX $tested_flags $flag -c $conftest_source 2>&1 || echo failed`
    rm -f "conftest.$OBJEXT"
    test "x$coaster_cxx_out" = x && tested_flags=${tested_flags:+"$tested_flags "}$flag
  done

  rm -f "$conftest_source"
  coaster_cxx_out=
  AC_LANG_POP([C++])
}
fi

if test "x$tested_flags" != x
then
  for flag in $tested_flags
  do
    case " $CXXFLAGS " in
      *" $flag "*) ;; # don't add flags twice
      *)           CXXFLAGS=${CXXFLAGS:+"$CXXFLAGS "}$flag ;;
    esac
  done
else
  tested_flags=none
fi

AC_MSG_RESULT([${tested_flags}])
])

